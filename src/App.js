import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import 'react-notifications/lib/notifications.css';
import { BrowserRouter, Routes, Route, } from "react-router-dom";

import OnboardingRoutes from "./routes/OnboardingRoutes";
import CommonRoutes from "./routes/CommonRoutes";
import StudentRoutes from "./routes/StudentRoutes";
import AdvisorRoutes from "./routes/AdvisorRoutes";
import ParentRoutes from "./routes/ParentRoutes"
// import AdvisorRoutes from "./routes/AdvisorRoutes";

import { Provider } from "react-redux";
import configureStore, { history } from "./redux/store";

export const store = configureStore();
class App extends Component {
    render() {
        return (
            <>
                <Provider store={store}>
                    <BrowserRouter>
                        {/* Onboarding Routes */}
                        <OnboardingRoutes />
                        {/* Common Routes */}
                        <CommonRoutes />
                        {/* Student page Routes */}
                        <StudentRoutes />
                        {/* Advisor page Routes */}
                        <AdvisorRoutes />
                        {/* Parent page Routes */}
                        <ParentRoutes />
                    </BrowserRouter>
                </Provider>
            </>
        );
    }
}

export default App;