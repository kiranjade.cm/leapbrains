import {
	FETCH_ERROR, FETCH_START, FETCH_SUCCESS, HIDE_MESSAGE, SHOW_MESSAGE,
	GET_USER_ROLES, GET_UNIVERSITIES, GET_PROFESSIONS, GET_GENDER, GET_STATUS, USER_ONBOARDED,GET_COUNTRY,
	NEWPASSWORD_RESET,UPDATE_PASSWORD,VERIFY_PASSWORD, VERIFY_PASSWORD_STATUS,UPDATE_PASSWORD_STATUS, CUSTOM_ALERT_SHOW
} from "../constants/CommonTypes";

import axios from 'axios'
import { NotificationManager } from 'react-notifications';
import { getUserData } from '../../utils/AuthUtils'

const BASE_URL = process.env.REACT_APP_BASE_URL;

export const fetchStart = () => {
	return {
		type: FETCH_START
	}
};

export const fetchSuccess = () => {
	return {
		type: FETCH_SUCCESS
	}
};

export const fetchError = (error) => {
	return {
		type: FETCH_ERROR,
		payload: error
	}
};

export const showMessage = (message) => {
	return {
		type: SHOW_MESSAGE,
		payload: message
	}
};

export const hideMessage = () => {
	return {
		type: HIDE_MESSAGE
	}
};

export const getUserRoles = () => {
	return (dispatch) => {
		axios.get(BASE_URL + '/user-service/v1/master/usertypes').then(({ data }) => {
			const { headers: { statusCode, message } } = data
			if (statusCode === "200") {
				dispatch({ type: FETCH_SUCCESS });
				dispatch({ type: GET_USER_ROLES, payload: data.data });
			} else {
				dispatch({ type: FETCH_ERROR, payload: message });
			}
		}).catch(function (error) {
			if (error.response) {
				const { headers: { message } } = error.response.data;
				dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message } });
				NotificationManager.error(message ? message : error.message, 'Error');
			} else {
				dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
				NotificationManager.error(error.message, 'Error');
			}
			console.log("Error****:", error.message);
		});
	}
};

export const getProfessions = () => {
	return (dispatch) => {
		axios.get(BASE_URL + '/config-service/v1/master/professions').then(({ data }) => {
			const { headers: { statusCode, message } } = data
			if (statusCode === "200") {
				console.log(data);
				dispatch({ type: FETCH_SUCCESS });
				dispatch({ type: GET_PROFESSIONS, payload: data.recordInfo });
				console.log(data)
			} else {
				dispatch({ type: FETCH_ERROR, payload: message });
			}
		}).catch(function (error) {
			if (error.response) {
				const { headers: { message } } = error.response.data;
				dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message } });
				NotificationManager.error(message ? message : error.message, 'Error');
			} else {
				dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
				NotificationManager.error(error.message, 'Error');
			}
			console.log("Error****:", error.message);
		});
	}
};
export const getUniversities = () => {
	return (dispatch) => {
		axios.get(BASE_URL + '/config-service/v1/master/universities').then(({ data }) => {
			const { headers: { statusCode, message } } = data
			if (statusCode === "200") {
				dispatch({ type: FETCH_SUCCESS });
				dispatch({ type: GET_UNIVERSITIES, payload: data.recordInfo });
			} else {
				dispatch({ type: FETCH_ERROR, payload: message });
			}
		}).catch(function (error) {
			if (error.response) {
				const { headers: { message } } = error.response.data;
				dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message } });
				NotificationManager.error(message ? message : error.message, 'Error');
			} else {
				dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
				NotificationManager.error(error.message, 'Error');
			}
			console.log("Error****:", error.message);
		});
	}
};

export const getGender = () => {
	return (dispatch) => {
		axios.get(BASE_URL + '/user-service/v1/master/gender').then(({ data }) => {
			const { headers: { statusCode, message } } = data
			if (statusCode === "200") {
				dispatch({ type: FETCH_SUCCESS });
				dispatch({ type: GET_GENDER, payload: data.data });
			} else {
				dispatch({ type: FETCH_ERROR, payload: message });
			}
		}).catch(function (error) {
			if (error.response) {
				const { headers: { message } } = error.response.data;
				dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message } });
				NotificationManager.error(message ? message : error.message, 'Error');
			} else {
				dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
				NotificationManager.error(error.message, 'Error');
			}
			console.log("Error****:", error.message);
		});
	}
};

export const getStatus = () => {
	return (dispatch) => {
		axios.get(BASE_URL + '/config-service/v1/master/studentstatus').then(({ data }) => {
			const { headers: { statusCode, message } } = data
			if (statusCode === "200") {
				dispatch({ type: FETCH_SUCCESS });
				dispatch({ type: GET_STATUS, payload: data.recordInfo });
			} else {
				dispatch({ type: FETCH_ERROR, payload: message });
			}
		}).catch(function (error) {
			if (error.response) {
				const { headers: { message } } = error.response.data;
				dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message } });
				NotificationManager.error(message ? message : error.message, 'Error');
			} else {
				dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
				NotificationManager.error(error.message, 'Error');
			}
			console.log("Error****:", error.message);
		});
	}
};

export const getCountry = () => {
	return (dispatch) => {
		axios.get(BASE_URL + '/config-service/v1/master/countries').then(({ data }) => {
			const { headers: { statusCode, message } } = data
			console.log("cntry",data.recordInfo)
			if (statusCode === "200") {
				dispatch({ type: FETCH_SUCCESS });
				dispatch({ type: GET_COUNTRY, payload: data.recordInfo });
			} else {
				dispatch({ type: FETCH_ERROR, payload: message });
			}
		}).catch(function (error) {
			if (error.response) {
				const { headers: { message } } = error.response.data;
				dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message } });
				NotificationManager.error(message ? message : error.message, 'Error');
			} else {
				dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
				NotificationManager.error(error.message, 'Error');
			}
			console.log("Error****:", error.message);
		});
	}
};

export const setUserOnboard = () => {
	axios.defaults.headers.common['Authorization'] = "Bearer " + getUserData('accessToken');
	return (dispatch) => {
		axios.post(BASE_URL + '/user-service/v1/profile/onboard').then(({ data }) => {
			const { headers: { statusCode, message } } = data
			if (statusCode === "200") {
				dispatch({ type: FETCH_SUCCESS });
				dispatch({ type: USER_ONBOARDED });
			} else {
				dispatch({ type: FETCH_ERROR, payload: message });
				NotificationManager.error(message, 'Error');
			}
		}).catch(function (error) {
			if (error.response) {
				const { headers: { message } } = error.response.data;
				dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message } });
				NotificationManager.error(message ? message : error.message, 'Error');
			} else {
				dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
				NotificationManager.error(error.message, 'Error');
			}
			console.log("Error****:", error.message);
		});
	}
};

export const forgetPassword  = requestParams => {
	return (dispatch) => {
		axios.post(BASE_URL + '/user-service/v1/auth/forgotpassword/initate', requestParams).then(({ data }) => {
		  const { headers: { statusCode, message } } = data
		  console.log("prev", data)
		  if (statusCode === "200") {
			console.log("sucess", data)
			dispatch({ type: FETCH_SUCCESS });
			dispatch({ type: NEWPASSWORD_RESET});
			dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:message, success:true}})
		  } else {
			console.log("error", data)
			dispatch({ type: FETCH_ERROR, payload: message });
			dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:message, success:false}})
			NotificationManager.error(message, 'Error');
		  }
		}).catch(function (error) {
		  if (error.response) {
			console.log('e-data', error.response.data)
			const { headers: { message }, errorlist } = error.response.data;
			dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist } });
			NotificationManager.error(message ? message : error.message, 'Error');
		  } else {
			dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
			NotificationManager.error(error.message, 'Error');
		  }
		  dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:error.message, success:false}})
		  console.log("Error****:", error.message);
		});
	  }
}
export const forgetPasswordstatus = status =>{
	return (dispatch) => {
		  dispatch({ type: NEWPASSWORD_RESET,payload:status });
	}
};
export const updatePassword  = requestParams => {
	return (dispatch) => {
		axios.post(BASE_URL + '/user-service/v1/auth/forgotpassword/updatepassword', requestParams).then(({ data }) => {
		  const { headers: { statusCode, message } } = data
		  console.log("predata",data)
		  if (statusCode === "200") {
			console.log("sucess", data)
			dispatch({ type: FETCH_SUCCESS });
			dispatch({ type: UPDATE_PASSWORD});
			dispatch({ type: UPDATE_PASSWORD_STATUS,payload:true});
			dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:message, success:true}})
		  } else {
			console.log("error", data)
			dispatch({ type: FETCH_ERROR, payload: message });
			dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:message, success:false}})
		  }
		}).catch(function (error) {
		  if (error.response) {
			console.log('e-data', error.response.data)
			const { headers: { message }, errorlist } = error.response.data;
			dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist } });
			NotificationManager.error(message ? message : error.message, 'Error');
		  } else {
			dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
			NotificationManager.error(error.message, 'Error');
		  }
		  console.log("Error****:", error.message);
		  dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:error.message, success:false}})
		});
	  }
}
export const updatePasswordstatus  = requestParams => {
	return (dispatch) => {
		dispatch({ type: UPDATE_PASSWORD_STATUS,payload:requestParams });
  }
}
export const verifyPassword = requestParams => {
	return (dispatch) => {
	  axios.post(BASE_URL + '/user-service/v1/auth/forgotpassword/verify', requestParams).then(({ data }) => {
		const { headers: { statusCode, message } } = data
		if (statusCode === "200") {
		  dispatch({ type: FETCH_SUCCESS });
		  dispatch({ type: VERIFY_PASSWORD });
		  dispatch({ type: VERIFY_PASSWORD_STATUS ,payload:true})
		} else {
		  dispatch({ type: FETCH_ERROR, payload: message });
		  dispatch({ type: VERIFY_PASSWORD_STATUS ,payload:false})
		  dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:message, success:false}})
		}
	  }).catch(function (error) {
		if (error.response) {
		  const { headers: { message, statusCode }, errorlist, } = error.response.data;
		  dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist, statusCode: statusCode } });
		  dispatch({ type: VERIFY_PASSWORD_STATUS ,payload:false})
		} else {
		  dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
		  dispatch({ type: VERIFY_PASSWORD_STATUS ,payload:false})
		}
		console.log("Error****:", error.message);
		dispatch({ type: VERIFY_PASSWORD_STATUS ,payload:false})
		dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:error.message, success:false}})
	  });
	}
  };
  export const verifyPasswordstatus = status =>{
	return (dispatch) => {
		  dispatch({ type: VERIFY_PASSWORD_STATUS,payload:status });
	}
};