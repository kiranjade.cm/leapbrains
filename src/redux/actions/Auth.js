import {
  FETCH_ERROR,
  FETCH_START,
  FETCH_SUCCESS
} from "../constants/CommonTypes";
import {
  INIT_URL,
  SIGNOUT_USER_SUCCESS,
  USER_TOKEN_SET,LOGIN_SUCESS,
  SIGNUP_USER_SUCCESS,
  EMAIL_VERIFICATION_SUCCESS,
  USER_PROFILE_SUCCESS,
  USER_ROLE_SAVED,
  USER_PASSWORD_UPDATE,USER_PROFILE_DATA,
  SET_ISGOAL_SUCCESS_STATUS,
  SET_PROFILE_SAVED_STATUS,EMAIL_VERIFIED,NEW_REGISTER,CUSTOM_ALERT_SHOW
} from "../constants/AuthActionTypes";
import axios from 'axios'
import { setSession, encryptData, getUserData, clearSession, authHeader } from '../../utils/AuthUtils'
import { NotificationManager } from 'react-notifications';

const BASE_URL = process.env.REACT_APP_BASE_URL;

export const setInitUrl = (url) => {
  return {
    type: INIT_URL,
    payload: url
  };
};

export const userSignUp = requestParams => {
  return (dispatch) => {
    axios.post(BASE_URL + '/user-service/v1/auth/register', requestParams).then(({ data }) => {
      const { headers: { statusCode, message } } = data
      if (statusCode === "200") {
        dispatch({ type: FETCH_SUCCESS });
        dispatch({ type: SIGNUP_USER_SUCCESS });
        dispatch({ type: NEW_REGISTER,payload:true });
      } else {
        dispatch({ type: FETCH_ERROR, payload: message });
        dispatch({ type: NEW_REGISTER,payload:false });
         NotificationManager.error(message, 'Error');
      }
    }).catch(function (error) {
      if (error.response) {
        console.log('e-data', error.response.data)
        const { headers: { message }, errorlist } = error.response.data;
        dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist } });
        dispatch({ type: NEW_REGISTER,payload:false });
        NotificationManager.error(message ? message : error.message, 'Error');
      } else {
        dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
        dispatch({ type: NEW_REGISTER,payload:false });
        NotificationManager.error(error.message, 'Error');
      }
      console.log("Error****:", error.message);
    });
  }
};

export const verifyEmail = requestParams => {
  return (dispatch) => {
    axios.post(BASE_URL + '/user-service/v1/auth/email/verify', requestParams).then(({ data }) => {
      const { headers: { statusCode, message } } = data
      if (statusCode === "200") {
        dispatch({ type: FETCH_SUCCESS });
        dispatch({ type: EMAIL_VERIFICATION_SUCCESS });
        dispatch({ type: EMAIL_VERIFIED,payload:true });
        dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:message, success:true}})
      } else {
        dispatch({ type: FETCH_ERROR, payload: message });
        dispatch({ type: EMAIL_VERIFIED,payload:false });
        dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:message, success:false}})
      }
    }).catch(function (error) {
      if (error.response) {
        const { headers: { message, statusCode }, errorlist, } = error.response.data;
        dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist, statusCode: statusCode } });
        dispatch({ type: EMAIL_VERIFIED,payload:false });
      } else {
        dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
        dispatch({ type: EMAIL_VERIFIED,payload:false });
      }
      console.log("Error****:", error.message);
      dispatch({ type: EMAIL_VERIFIED,payload:false });
      dispatch({type:CUSTOM_ALERT_SHOW, payload:{msg:error.message, success:false}})
    });
  }
};
export const Emailverify = status =>{
  return (dispatch) => {
        dispatch({ type: EMAIL_VERIFIED,payload:status });
  }
};

export const userSignIn = requestParams => {
  return (dispatch) => {
    axios.post(BASE_URL + '/user-service/v1/auth/login', requestParams).then(({ data }) => {
      const { headers: { statusCode, message }, accessToken, refreshKey } = data
      if (statusCode === "200") {
        setSession(encryptData(JSON.stringify({ accessToken, refreshKey })));
        axios.defaults.headers.common['Authorization'] = "Bearer " + accessToken;
        dispatch({ type: FETCH_SUCCESS });
        dispatch({type: LOGIN_SUCESS});
        dispatch({ type: USER_TOKEN_SET, payload: true });
        dispatch(getUserProfile());
      } else {
        dispatch({ type: FETCH_ERROR, payload: message });
        NotificationManager.error(message, 'Error');
      }
    }).catch(function (error) {
      if (error.response) {
        console.log('e-data', error.response.data)
        const { headers: { message }, errorlist } = error.response.data;
        dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist } });
        NotificationManager.error(message ? message : error.message, 'Error');
      } else {
        dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
        NotificationManager.error(error.message, 'Error');
      }
      console.log("Error****:", error.message);
    });
  }
};

export const getUserProfile = () => {
  return (dispatch) => {
    axios.get(BASE_URL + '/user-service/v1/profile').then(({ data }) => {
      const { headers: { statusCode, message } } = data
      if (statusCode === "200") {
        dispatch({ type: FETCH_SUCCESS });
        dispatch({ type: USER_PROFILE_SUCCESS, payload: data.profileInfo });
        dispatch({ type: USER_PROFILE_DATA, payload: data.studentProfileData });
      } else {
        dispatch({ type: FETCH_ERROR, payload: message });
      }
    }).catch(function (error) {
      if (error.response) {
        const { headers: { message } } = error.response.data;
        dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message } });
      } else {
        dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
      }
      console.log("Error****:", error.message);
    });
  }
};

export const setUserRole = requestParams => {
  return (dispatch) => {
    axios.post(BASE_URL + '/user-service/v1/profile/roles/update', requestParams).then(({ data }) => {
      const { headers: { statusCode, message } } = data
      if (statusCode === "200") {
        dispatch({ type: FETCH_SUCCESS });
        dispatch({ type: USER_ROLE_SAVED });
        dispatch(getUserProfile());
      } else {
        dispatch({ type: FETCH_ERROR, payload: message });
      }
    }).catch(function (error) {
      if (error.response) {
        const { headers: { message, statusCode }, errorlist, } = error.response.data;
        dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist, statusCode: statusCode } });
      } else {
        dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
      }
      console.log("Error****:", error.message);
    });
  }
};
// GOALS
export const setUserGoals = requestParams => {
  return (dispatch) => {
    axios.post(BASE_URL + '/user-service/v1/student/goals/update', requestParams).then(({ data }) => {
      const { headers: { statusCode, message } } = data
      console.log(data)
      if (statusCode === "200") {
        dispatch({ type: FETCH_SUCCESS });
        dispatch({ type: SET_ISGOAL_SUCCESS_STATUS,payload:true });
        dispatch(getUserProfile());
      } else {
        dispatch({ type: FETCH_ERROR, payload: message });
        NotificationManager.error(message, 'Error');
      }
    }).catch(function (error) {
      if (error.response) {
        const { headers: { message, statusCode }, errorlist, } = error.response.data;
        dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist, statusCode: statusCode } });
        NotificationManager.error(message ? message : error.message, 'Error');
      } else {
        dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
        NotificationManager.error(error.message, 'Error');
      }
      console.log("Error****:", error.message);
    });
  }
};

export const setIsGoalSuccess = status =>{
  return (dispatch) => {
        dispatch({ type: SET_ISGOAL_SUCCESS_STATUS,payload:status });
  }
};
// GOALS
// PROFILES
export const setUserProfile = requestParams => {
  console.log(requestParams);
  return (dispatch) => {
    axios.post(BASE_URL + '/user-service/v1/student/profile/update', requestParams).then(({ data }) => {
      const { headers: { statusCode, message } } = data
      if (statusCode === "200") {
        dispatch({ type: FETCH_SUCCESS });
        dispatch({ type: SET_PROFILE_SAVED_STATUS,payload:true });
        dispatch(getUserProfile());
      } else {
        dispatch({ type: FETCH_ERROR, payload: message });
        NotificationManager.error(message, 'Error');
      }
    }).catch(function (error) {
      if (error.response) {
        const { headers: { message, statusCode }, errorlist, } = error.response.data;
        dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist, statusCode: statusCode } });
        NotificationManager.error(message ? message : error.message, 'Error');
      } else {
        dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
        NotificationManager.error(error.message, 'Error');
      }
      console.log("Error****:", error.message);
    });
  }
};
export const setIsUserProfileSuccess = status =>{
  return (dispatch) => {
        dispatch({ type: SET_PROFILE_SAVED_STATUS,payload:status });
  }
};
// PROFILES
export const userSignOut = requestParams => {
  return (dispatch) => {
    axios.post(BASE_URL + '/user-service/v1/auth/logout', requestParams).then(({ data }) => {
      const { headers: { statusCode, message } } = data
      if (statusCode === "200") {
        localStorage.removeItem("token");
        setSession(null)
        dispatch({ type: SIGNOUT_USER_SUCCESS });
      }
    }).catch(function (error) {
      dispatch({ type: FETCH_ERROR, payload: error.message });
      console.log("Error****:", error.message);
    });
  }
};


export const refreshToken = () => {
  const refreshKey = getUserData('refreshKey')
  return (dispatch) => {
    dispatch({ type: FETCH_START })
    axios.post(BASE_URL + '/user-service/v1/auth/refresh', { refreshKey }).then(({ data }) => {
      const { headers: { statusCode, message }, accessToken, refreshKey } = data
      if (statusCode !== "200") {
        clearSession()
      } else {
        setSession(encryptData(JSON.stringify({ accessToken, refreshKey })));
        axios.defaults.headers.common['Authorization'] = "Bearer " + accessToken;
      }
      dispatch({ type: FETCH_SUCCESS });
    }).catch(function (error) {
      clearSession()
      dispatch({ type: FETCH_ERROR, payload: error.message });
      console.log("Error****:", error.message);
    });
  }
}

export const changePassword = requestParams => {
  axios.defaults.headers.common['Authorization'] = "Bearer " + getUserData('accessToken');
  console.log(requestParams);
  return (dispatch) => {
    axios.post(BASE_URL + '/user-service/v1/profile/changepassword', requestParams).then(({ data }) => {
      const { headers: { statusCode, message } } = data
      if (statusCode === "200") {
        dispatch({ type: FETCH_SUCCESS });
        dispatch({ type: USER_PASSWORD_UPDATE });
        NotificationManager.success(message, 'Success');
      } else {
        dispatch({ type: FETCH_ERROR, payload: message });
        NotificationManager.error(message, 'Error');
      }
    }).catch(function (error) {
      if (error.response) {
        const { headers: { message, statusCode }, errorlist, } = error.response.data;
        dispatch({ type: FETCH_ERROR, payload: { message: message ? message : error.message, errorList: errorlist, statusCode: statusCode } });
        NotificationManager.error(message ? message : error.message, 'Error');
      } else {
        dispatch({ type: FETCH_ERROR, payload: { message: error.message } });
        NotificationManager.error(error.message, 'Error');
      }
      console.log("Error****:", error.message);
    });
  }
};