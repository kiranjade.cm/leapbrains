import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import Auth from "./Auth";
import Common from "./Common";


const reducers = combineReducers({
  routing: routerReducer,
  auth: Auth,
  commonData: Common
});

export default reducers;
