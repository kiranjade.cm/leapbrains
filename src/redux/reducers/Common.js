import {FETCH_ERROR, FETCH_START, FETCH_SUCCESS, HIDE_MESSAGE, 
  SHOW_MESSAGE, SET_PAGINATION, GET_USER_ROLES, GET_UNIVERSITIES,GET_COUNTRY, 
  GET_PROFESSIONS, GET_GENDER, GET_STATUS, USER_ONBOARDED,NEWPASSWORD_RESET,UPDATE_PASSWORD
  ,VERIFY_PASSWORD,FORGET_PASSWORD_STATUS,VERIFY_PASSWORD_STATUS,UPDATE_PASSWORD_STATUS, CUSTOM_ALERT_SHOW, CUSTOM_ALERT_CLOSE} from '../constants/CommonTypes'

const INIT_STATE = {
  error: "",
  loading: false,
  message: '',
  isUserOnboarded: false,
  isNewPassword: false,
  isUpdatePassword: false,
  isverifyPassword:false,
  isverifyPasswordstatus:true,
  isUpdatePasswordstatus:true,
  errorList: {},
  pagination: {
    totalpages: 1,
    currentpage: 1,
    recordlimit: 10,
    totalrecords: 0
  },
  customAlertMsg:"",
  customAlertShow:false,
  customAlertSuccess:true
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case CUSTOM_ALERT_SHOW:{
      return {...state, customAlertMsg: action.payload.msg, customAlertShow: true, customAlertSuccess: action.payload.success};
    }
    case CUSTOM_ALERT_CLOSE:{
      return {...state, customAlertMsg: "", customAlertShow: false, customAlertSuccess: true};
    }
    case FETCH_START: {
      return {...state, error: '', message: '', loading: true};
    }
    case FETCH_SUCCESS: {
      return {...state, error: '', message: '', loading: false};
    }
    case SHOW_MESSAGE: {
      return {...state, error: '', message: action.payload, loading: false};
    }
    case FETCH_ERROR: {
      return {...state, loading: false, error: action.payload.message, message: action.payload.message, errorList: action.payload.errorList, statusCode: action.payload.statusCode};
    }
    case HIDE_MESSAGE: {
      return {...state, loading: false, error: '', message: '', errorList: {}};
    }
    case SET_PAGINATION: {
      return {...state, pagination: action.payload};
    }
    case GET_USER_ROLES: {
      return {...state, userRoles: action.payload};
    }
    case GET_UNIVERSITIES: {
      return {...state, universities: action.payload};
    }
    case GET_PROFESSIONS: {
      return {...state, professions: action.payload};
    }
    case GET_GENDER: {
      return {...state, gender: action.payload};
    }
    case GET_STATUS: {
      return {...state, status: action.payload};
    }
    case USER_ONBOARDED: {
      return {...state, isUserOnboarded: true};
    }
    case GET_COUNTRY:{
      return {...state, country: action.payload};
    }
    case NEWPASSWORD_RESET:{
      return {...state, isNewPassword: true,};
    }
    case FORGET_PASSWORD_STATUS:{
      return {...state, isNewPassword: action.payload,};
    }
    case VERIFY_PASSWORD:{
      return {...state, isverifyPassword: true,};
    }
    case VERIFY_PASSWORD_STATUS:{
      return {...state, isverifyPasswordstatus: action.payload,};
    }
    case UPDATE_PASSWORD:{
      return {...state, isUpdatePassword: true,};
    }
    case UPDATE_PASSWORD_STATUS:{
      return {...state, isUpdatePasswordstatus: action.payload,};
    }
    default:
      return state;
  }
}
