import {
  INIT_URL, SIGNOUT_USER_SUCCESS,
  USER_TOKEN_SET, SIGNUP_USER_SUCCESS, EMAIL_VERIFICATION_SUCCESS, USER_PROFILE_SUCCESS,USER_PROFILE_DATA,
  USER_ROLE_SAVED, USER_PROFILEL_SAVED, USER_PASSWORD_UPDATE,
  SET_ISGOAL_SUCCESS_STATUS,EMAIL_VERIFIED,LOGIN_SUCESS,
  SET_PROFILE_SAVED_STATUS,NEW_REGISTER, CUSTOM_ALERT_SHOW, CUSTOM_ALERT_CLOSE,
} from "../constants/AuthActionTypes";
import { isUserAuthenticated } from '../../utils/AuthUtils'

const INIT_STATE = {
  token: isUserAuthenticated(),
  verificationKey: '',
  initURL: '',
  islogedin: false,
  isRegistered: false,
  isEmailVerified: false,
  isGoalSuccess: false,
  isRoleSuccess: false,
  isProfileSuccess: false,
  userProfile: {},
  userProfileData:{},
  isPasswordUpdate: '',
  isEmailConfirm: true,
  isNewRegister: false,
  customAlertMsg:"",
  customAlertShow:false,
  customAlertSuccess:true
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case CUSTOM_ALERT_SHOW:{
      return {...state, customAlertMsg: action.payload.msg, customAlertShow: true, customAlertSuccess: action.payload.success};
    }
    case CUSTOM_ALERT_CLOSE:{
      return {...state, customAlertMsg: "", customAlertShow: false, customAlertSuccess: true};
    }
    case INIT_URL: {
      return { ...state, initURL: action.payload };
    }
    case SIGNOUT_USER_SUCCESS: {
      return {
        ...state,
        token: null,
        initURL: ''
      }
    }

    case USER_TOKEN_SET: {
      return {
        ...state,
        token: action.payload,
        islogedin: true
      };
    }

    case SIGNUP_USER_SUCCESS: {
      return {
        ...state,
        isRegistered: true,
      };
    }

    case EMAIL_VERIFICATION_SUCCESS: {
      return {
        ...state,
        isEmailVerified: true,
      };
    }
    case NEW_REGISTER:{
      return {
        ...state,
        isNewRegister: action.payload,
      };
    }
    case USER_PROFILE_SUCCESS: {
      return {
        ...state,
        userProfile: action.payload,
      };
    }
    case USER_PROFILE_DATA: {
      return {
        ...state,
        userProfileData: action.payload,
      };
    }
    case USER_ROLE_SAVED: {
      return {
        ...state,
        isRoleSuccess: true,
      };
    }

    case SET_ISGOAL_SUCCESS_STATUS: {
      return {
        ...state,
        isGoalSuccess: action.payload,
      };
    }

    case SET_PROFILE_SAVED_STATUS: {
      return {
        ...state,
        isProfileSuccess: action.payload,
      };
    }
    case USER_PASSWORD_UPDATE: {
      return {
        ...state,
        isPasswordUpdate: true,
      };
    }
    case EMAIL_VERIFIED:{
      return {
        ...state,
        isEmailConfirm: action.payload,
      };
    }

    default:
      return state;
  }
}
