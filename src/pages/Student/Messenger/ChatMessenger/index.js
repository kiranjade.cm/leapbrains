import React, { Component } from "react";
import '../../../../assets/css/Messenger.less';
import { Tab } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import avator from '../../../../assets/images/80.png'
class Index extends Component {
    constructor() {
        super();
        this.state = {
            Messagekey: "messages"
        };
        this.showChatcall = this.showChatcall.bind(this)
    }
    setMessagekey = (m) => {
        this.setState({
            Messagekey: m,
        });
    }
    showChatcall = () => {
        this.props.chatcall(false);
    }
    render() {

        return (
            <>
                <div className="chatbox-messenger-container">
                    <div className="message-chatting">
                        <div className="msg-head">
                            <div className="chat-head-left">
                                <div className="arrow" onClick={this.showChatcall}>
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M20 12C20 11.4477 19.5523 11 19 11H5C4.44771 11 4 11.4477 4 12C4 12.5523 4.44771 13 5 13H19C19.5523 13 20 12.5523 20 12Z" fill="#1B1C1E" />
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.7071 4.29289C12.3166 3.90237 11.6834 3.90237 11.2929 4.29289L4.29289 11.2929C3.90237 11.6834 3.90237 12.3166 4.29289 12.7071L11.2929 19.7071C11.6834 20.0976 12.3166 20.0976 12.7071 19.7071C13.0976 19.3166 13.0976 18.6834 12.7071 18.2929L6.41421 12L12.7071 5.70711C13.0976 5.31658 13.0976 4.68342 12.7071 4.29289Z" fill="#1B1C1E" />
                                    </svg>
                                </div>
                                <div className="messenger-profile">
                                    <div className="msg-img"> <img src={avator} alt='img' />
                                        <div className="online"></div>
                                    </div>
                                </div>
                                <div className="msg-name">
                                    <div className="name">Stevan Sanders</div>
                                    <div className="online-status">Online</div>
                                </div>

                            </div>
                            <div className="chat-head-right">
                                <div className="search-call">
                                    <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <ellipse rx="6.53333" ry="6.53979" transform="matrix(-1 0 0 1 7.53307 7.53979)" stroke="#1C84EE" stroke-width="2" />
                                        <path d="M12.2 13.1445L15 15.9473" stroke="#1C84EE" stroke-width="2" stroke-linecap="round" />
                                    </svg>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.67969 10.9453C6.98438 13.25 9.78125 15.0234 12.0625 15.0234C13.0859 15.0234 13.9844 14.6719 14.7031 13.875C15.1172 13.4062 15.375 12.8672 15.375 12.3281C15.375 11.9141 15.2188 11.5156 14.8203 11.2344L12.4219 9.53125C12.0469 9.26562 11.7266 9.14062 11.4453 9.14062C11.0781 9.14062 10.7578 9.34375 10.3984 9.70312L9.82812 10.2656C9.74219 10.3516 9.63281 10.3828 9.53125 10.3828C9.40625 10.3828 9.28906 10.3359 9.20312 10.2969C8.71875 10.0312 7.875 9.30469 7.09375 8.53125C6.32031 7.75 5.59375 6.91406 5.33594 6.42188C5.28906 6.33594 5.24219 6.21875 5.24219 6.10156C5.24219 6 5.27344 5.89062 5.36719 5.80469L5.92188 5.23438C6.28125 4.86719 6.49219 4.54688 6.49219 4.17969C6.49219 3.89844 6.35938 3.58594 6.09375 3.20312L4.40625 0.828125C4.11719 0.429688 3.71094 0.257812 3.26562 0.257812C2.74219 0.257812 2.21094 0.492188 1.74219 0.9375C0.96875 1.67188 0.625 2.57812 0.625 3.59375C0.625 5.875 2.375 8.64062 4.67969 10.9453Z" fill="#1C84EE" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div className="container-notify row container-messenger">
                            <div className="col-md-12">
                                <div className="contain">
                                    <Tab.Container id="left-tabs-example" activeKey={this.state.Messagekey} onSelect={(m) => this.setMessagekey(m)}>
                                        <div className="row">
                                            <div classname="col-sm-6 col-lg-12 col-xl-12">
                                                <Nav variant="pills" >
                                                    <Nav.Item>
                                                        <Nav.Link eventKey="messages">Messages</Nav.Link>
                                                    </Nav.Item>
                                                    <Nav.Item>
                                                        <Nav.Link eventKey="threads">Threads</Nav.Link>
                                                    </Nav.Item>

                                                </Nav>
                                            </div>
                                            <div classname="col-sm-6 col-lg-12 col-xl-12">
                                                <Tab.Content>
                                                    <Tab.Pane eventKey="messages">

                                                    </Tab.Pane>
                                                    <Tab.Pane eventKey="threads">

                                                    </Tab.Pane>

                                                </Tab.Content>
                                            </div>
                                        </div>
                                    </Tab.Container>
                                </div>
                            </div>
                        </div>
                        <div className="msg-bottom-text">
                            <div className="enter-text">
                                <input />
                            </div>
                            <div className="msg-bottom">
                                <div className="msg-bottom-left">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5 7C5 5.89543 5.89543 5 7 5H17C18.1046 5 19 5.89543 19 7V17C19 18.1046 18.1046 19 17 19H7C5.89543 19 5 18.1046 5 17V7Z" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        <path d="M6 17.0711L9.51472 13L14.0711 19.0711" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M14 12C12.8951 12 12 11.1047 12 10C12 8.8953 12.8951 8 14 8C15.1043 8 16 8.8953 16 10C16 11.1047 15.1043 12 14 12Z" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.5642 14.3612L15.464 10.4614C16.2071 9.71831 16.6822 9.05176 16.884 8.28157C17.1676 7.43582 16.9583 6.55591 16.2642 5.86186C15.014 4.61187 13.2641 4.87259 11.5697 6.56705L7.66987 10.4669C5.86129 12.2754 5.80782 15.2045 7.59951 16.9962C9.29973 18.6964 12.2253 18.6394 14.0338 16.8308L19.9335 10.9311" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.9382 17.7183C10.4102 19.2463 7.90199 19.2155 6.34308 17.6566C4.78098 16.0945 4.75302 13.5898 6.28131 12.0615L8.52617 9.81661" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        <path d="M11.4163 6.92621C12.9443 5.39823 15.4525 5.42906 17.0114 6.98798C18.5735 8.55008 18.6015 11.0548 17.0732 12.5831L14.8283 14.8279" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        <path d="M10.586 13.4141L12.8883 11.1118" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6 11C6 16.0604 12 20 12 20C12 20 18 16.0604 18 11C18 7.81044 15.3429 5 12 5C8.65714 5 6 7.81044 6 11Z" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12 19C8.13296 19 5 15.8664 5 12C5 8.13357 8.13296 5 12 5C15.8652 5 19 8.13357 19 12C19 15.8664 15.8652 19 12 19Z" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        <path d="M15.375 13.125C15 14.25 13.8636 15.375 12 15.375C10.1355 15.375 9 14 8.625 13.125" stroke="#959699" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                </div>
                                <div className="msg-bottom-right">
                                    <div className="msg-count">123/500 messages</div>
                                    <Button variant="primary">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M17.8151 12.0002L6.39844 18.2071L6.39844 5.79687L17.8151 12.0002Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                            <path d="M6.39844 12.0031H10.3984" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        </svg>
                                        Send</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        );
    }

}


export default Index;