import React, { Component } from "react";
import '../../../../assets/css/Messenger.less';
import { Tab } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import Advisor from "./AdvisorInbox";
class Index extends Component {
    constructor() {
        super();
        this.state = {
            key: "advisor",
            isChatDisplayed:false,
        };
        this.handleCallback = this.handleCallback.bind(this)
    }
    setKey = (k) => {
        this.setState({
            key: k,
        });
    }
    handleCallback = (childData) =>{
        this.props.chatcall(childData);
    }


    render() {

        console.log(this.state.isChatDisplayed)


        return (
            <>
                <div className="inbox-messenger-container">
                    <div className="messenger-head">
                        <div className="m-tittle"><h2>Chat</h2></div>
                        <div className="m-add-btn"><button className="btn cta--rounded cta-primary">+</button></div>
                    </div>
                    <div className="inbox-tab-container row ">
                        <div className="col-md-12">
                            <div className="contain-in">
                                <Tab.Container id="left-tabs-example" activeKey={this.state.key} onSelect={(k) => this.setKey(k)}>
                                    <div className="row">
                                        <div classname="col-sm-6 col-lg-12 col-xl-12 inbox-tab">
                                            <Nav variant="pills" >
                                                <Nav.Item>
                                                    <Nav.Link eventKey="advisor">Advisor</Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item>
                                                    <Nav.Link eventKey="friends">Friends</Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item>
                                                    <Nav.Link eventKey="family">Family</Nav.Link>
                                                </Nav.Item>
                                            </Nav>
                                        </div>
                                        <div classname="col-sm-6 col-lg-12 col-xl-12 pl-0">
                                            <Tab.Content>
                                                <Tab.Pane eventKey="advisor">
                                                    <Advisor parentCallback = {this.handleCallback}/>
                                                </Tab.Pane>
                                                <Tab.Pane eventKey="friends">

                                                </Tab.Pane>
                                                <Tab.Pane eventKey="family">

                                                </Tab.Pane>
                                            </Tab.Content>
                                        </div>
                                    </div>
                                </Tab.Container>
                            </div>
                        </div>
                    </div>
                </div>


            </>
        );
    }

}


export default Index;