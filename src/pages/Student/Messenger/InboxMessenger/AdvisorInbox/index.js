import React, { Component } from "react";
import avator from '../../../../../assets/images/80.png';
import "../../../../../assets/css/Messenger.less";



class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isChatDisplayed:false,
            MessengerAdvisorList: [
                {
                    id: 1,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "37 m",
                    getMessage: " Hi, let me know when you go to library."
                },
                {
                    id: 2,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "54 m",
                    getMessage: "When will you start to read the book that i have send today."
                },
                {
                    id: 3,
                    activeStatus: "inactive",
                    getName: 'Stevan Sanders',
                    getMinute: "57 m",
                    getMessage: " Please inform your parents that I have recevied Money."
                },
                {
                    id: 4,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "37 m",
                    getMessage: " Hi, let me know when you go to library."
                },
                {
                    id: 5,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "54 m",
                    getMessage: "When will you start to read the book that i have send today."
                },
                {
                    id: 6,
                    activeStatus: "inactive",
                    getName: 'Stevan Sanders',
                    getMinute: "57 m",
                    getMessage: " Please inform your parents that I have recevied Money."
                },
                {
                    id: 7,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "37 m",
                    getMessage: " Hi, let me know when you go to library."
                },
                {
                    id: 8,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "54 m",
                    getMessage: "When will you start to read the book that i have send today."
                },
                {
                    id: 9,
                    activeStatus: "inactive",
                    getName: 'Stevan Sanders',
                    getMinute: "57 m",
                    getMessage: " Please inform your parents that I have recevied Money."
                },

            ]
        };
        this.showChatcall = this.showChatcall.bind(this)
    }
    componentDidMount = () => {

    }
    showChatcall = () => {
        this.props.parentCallback(true);
    }
    render() {
      
        let MessengerAdvisorDetails = this.state.MessengerAdvisorList.map((element, key) => {
            return (
                <>
                    <div className="col-md-12 msg">
                        <div className="messenger-chat " onClick={this.showChatcall}>
                            <div className='messenger-profile'>
                                <div className="msg-img"> <img src={avator} alt='img' />
                                    <div className="online"></div>
                                </div>
                            </div>
                            <div className='messenger-content'>
                                <div className='top'>
                                    <div className='name'>{element.getName}</div>
                                    <div className='time-min'>{element.getMinute}</div>
                                </div>
                                <div className='message'>
                                    <div className="message-content">{element.getMessage}</div>
                                    <div className="active-status">
                                        <div className="active"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </>
            )

        })

        return (
            <>
                <div className="changecnt">
                    {this.state.MessengerAdvisorList && this.state.MessengerAdvisorList.length > 0 &&
                        <div className="row jj" onClick={this.showchat}>
                            {MessengerAdvisorDetails}
                        </div>
                    }
                </div>
            </>
        );
    }
}

export default Index;