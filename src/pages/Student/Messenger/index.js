import React, { Component } from "react";
import '../../../assets/css/Messenger.less';
import ChatMessenger from "./ChatMessenger";
import InboxMessenger from "./InboxMessenger";
import Emptybox from "./Emptybox";
class Index extends Component {
    constructor() {
        super();
        this.state = {
            isChatDisplayed: false,
        };
        // this.showChat = this.showChat.bind(this)
        this.chatDisplaynow = this.chatDisplaynow.bind(this)
    }

    // showChat = () => {
    //     this.setState({ ...this.state, isChatDisplayed: true })
    // }
    chatDisplaynow = (chat) => {
        this.setState({ ...this.state, isChatDisplayed: chat })
    }

    render() {
        console.log(this.state.isChatDisplayed)
        return (

            <>
                <div className="messenger-container">
                    <div className="messenger-desktop">
                        <div className="messenger fixed" >
                            {/* <button onClick={this.showChat}>y</button> */}
                            <InboxMessenger chatcall={this.chatDisplaynow} />
                            {this.state.isChatDisplayed ? <ChatMessenger chatcall={this.chatDisplaynow}/> : <Emptybox />}
                        </div>
                    </div>
                    <div className="messenger-mobileview">
                    {this.state.isChatDisplayed ? <ChatMessenger chatcall={this.chatDisplaynow}/> : <InboxMessenger chatcall={this.chatDisplaynow} />}
                    </div>
                </div>

            </>
        );
    }

}


export default Index;