import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { Modal } from "react-bootstrap";
import Dropdown from 'react-bootstrap/Dropdown'
import Select from 'react-select';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import TwoUserIcons from "../../../../../components/TwoUserIcons"
import Search from "../../../../../components/Search";
import Avatar1 from "../../../../../assets/images/icons/Avatar1.png"
import Avatar2 from "../../../../../assets/images/icons/Avatar2.png"
import "../../../../../assets/css/work.less"

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            UniversityCheckBox: false,
            ProfessionsCheckBox: false,
            GPACheckBox: false,
            isShowPan: false,
            deleteshow: false,

            Plans: [
                {
                    id: 1,
                    status: "Active",
                    goal: "Get into university",
                    coursename: "Algebra",
                    planname: "Admission to Cambridge",
                    data: "2021-2022",
                    advisorname: true
                },
                {
                    id: 2,
                    status: "Ongoing",
                    goal: "Improve GPA",
                    planname: "Admission to Cambridge",
                    data: "2021-2022",
                    advisorname: true
                },
                {
                    id: 3,
                    status: "Completed",
                    goal: "Profession Enginnering",
                    planname: "Admission to Cambridge",
                    data: "2021-2022",
                    advisorname: true
                },
                {
                    id: 4,
                    status: "Not started",
                    goal: "Improve GPA",
                    planname: "Algebra",
                    data: "2021-2022",
                    advisorname: false,
                },
                {
                    id: 5,
                    status: "Ongoing",
                    goal: "Improve GPA",
                    planname: "Economy",
                    data: "2021-2022",
                    advisorname: true
                },
            ]

        };
    }
    openplansDropDown = (e) => {
        this.setState({
            showDropDown: !this.state.showDropDown
        });

    }
    closeplansDropDown = (e) => {
        this.setState({
            showDropDown: false
        });
    }
    openDropDown = (e) => {
        console.log(e)
        if (e === "Edit") {
            this.setState({ show: true })
        }
        else if (e === "Delete") {
            this.setState({ deleteshow: true })
        }
    }
    handleShow(e) {
        e.preventDefault();
        this.setState({ show: true })
    }

    setShow(e) {
        this.setState({ show: false })
        this.props.showMyPlans(e, false)
    }

    deletehandleShow(e) {
        e.preventDefault();
        this.setState({ deleteshow: true })
    }
    deletehandleClose(e) {
        this.setState({ deleteshow: false })
    }
    confirmdelete(e) {
        this.setState({ deleteshow: false })
        NotificationManager.success('Course Deleted Sucessfully', 'Delete!', 5000, () => {
        });
    }
    handleNextStep(e, step) {
        e.preventDefault();
        let seasonList = document.getElementsByClassName("mywork-popup-steps");
        let sample = ""
        for (let i = 0; i < seasonList.length; i++) {
            if (step == seasonList[i].dataset.step) {
                sample = seasonList[i];
                sample.classList.add("show");
            } else {
                seasonList[i].classList.remove("show");
            }
        }
    }

    handleCheckboxChange(e, buttonName) {
        let { UniversityCheckBox, ProfessionsCheckBox, GPACheckBox } = this.state;
        console.log('value of checkbox : ' + buttonName, e.target.checked);
        if (buttonName === "universities") {
            UniversityCheckBox = e.target.checked
        } else if (buttonName === "profession") {
            ProfessionsCheckBox = e.target.checked
        } else if (buttonName === "GPA") {
            GPACheckBox = e.target.checked
        }
        this.setState({ UniversityCheckBox, ProfessionsCheckBox, GPACheckBox })
    }

    handleShowPlan(e) {
        e.preventDefault()
        this.setState({ isShowPan: true })
    }

    handleClickAddPhase(e) {
        e.preventDefault();
        var count = document.getElementsByClassName("initial_plan_parent").length;
        let menu = document.querySelector('#initial_plan_parent');

        let clonedMenu = menu.cloneNode(true);
        clonedMenu.id = 'menu-mobile';
        var child = clonedMenu.firstChild;
        child.innerHTML = "Phase" + parseInt(parseInt(count) + 1) + ":";
        document.getElementById("initial_plan_clone").appendChild(clonedMenu);
    }

    render() {
        let { show, UniversityCheckBox, ProfessionsCheckBox, GPACheckBox, isShowPan } = this.state;
        const UniversityOptionstions = [
            { value: 'UCLA', label: 'UCLA' },
            { value: 'Stanford University', label: 'Stanford University' },
            { value: 'Stanford University1', label: 'Stanford University' },
            { value: 'Stanford University2', label: 'Stanford University' }
        ];
        const ProfessionsOptionstions = [
            { value: 'Software Engineer', label: 'Software Engineer' },
            { value: 'Software Developer', label: 'Software Developer' }
        ];
        const options = [
            { value: '2021 (1-st semestre)', label: '2021 (1-st semestre)' },
            { value: '2021 (2-st semestre)', label: '2021 (2-st semestre)' },
            { value: '2022 (1-st semestre)', label: '2022 (1-st semestre)' },
            { value: '2022 (2-nd semestre)', label: '2022 (2-nd semestre)' },
            { value: '2023 (1-st semestre)', label: '2023 (1-st semestre)' },
            { value: '2023 (2-nd semestre)', label: '2023 (2-nd semestre)' }
        ]
        const options1 = [
            { value: '2021 (1-st semestre)', label: '2021 (1-st semestre)' },
            { value: '2021 (2-st semestre)', label: '2021 (2-st semestre)' },
            { value: '2022 (1-st semestre)', label: '2022 (1-st semestre)' },
            { value: '2022 (2-nd semestre)', label: '2022 (2-nd semestre)' },
            { value: '2023 (1-st semestre)', label: '2023 (1-st semestre)' },
            { value: '2023 (2-nd semestre)', label: '2023 (2-nd semestre)' }
        ]
        const options2 = [
            { value: 'Academic Regular', label: 'Academic Regular' },
            { value: 'Academic Accelerated', label: 'Academic Accelerated' },
            { value: 'Academic Honors', label: 'Academic Honors' },
            { value: 'Academic AP/College level', label: 'Academic AP/College level' },
            { value: 'Desired Grade', label: 'Desired Grade' }
        ]
        const style1 = {
            control: (base, state) => ({
                ...base,
                border: "0 !important",
                boxShadow: "0 !important",
                "&:hover": {
                    border: "0 !important"
                }
            })
        };

        const AvatarIconData = [
            {
                'AvatarIcon': Avatar1,
                'AvatarIconAlt': 'Avatar1-icon'
            },
            {
                'AvatarIcon': Avatar2,
                'AvatarIconAlt': 'Avatar2-icon'
            },
        ];
        let Plansdetails = this.state.Plans.map((element, key) => {
            return (
                <>
                    <div className="carddes">
                        <div className="cardtops">
                            <div className="row">
                                <div className="col">
                                    <Button className="cardbtn" style={{ 'border': "none", 'backgroundColor': element.status === 'Active' ? '#16DAF1' : element.status === 'Ongoing' ? '#FFCC5A' : element.status === 'Completed' ? '#34C38F' : element.status === 'Not started' ? '#1C84EE' : '#1C84EE' }}>{element.status}</Button>
                                </div>
                                <div className="col cardgrid__dot endcols">
                                    <Dropdown onSelect={this.openDropDown}>
                                        <Dropdown.Toggle id="dropdown-basic">
                                            <svg width="16" height="4" viewBox="0 0 16 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" clipRule="evenodd" d="M1.6 3.2C0.716344 3.2 0 2.48366 0 1.6C0 0.716344 0.716344 0 1.6 0C2.48366 0 3.2 0.716344 3.2 1.6C3.2 2.48366 2.48366 3.2 1.6 3.2ZM7.99844 3.2C7.11478 3.2 6.39844 2.48366 6.39844 1.6C6.39844 0.716344 7.11478 0 7.99844 0C8.88209 0 9.59844 0.716344 9.59844 1.6C9.59844 2.48366 8.88209 3.2 7.99844 3.2ZM12.8 1.6C12.8 2.48366 13.5163 3.2 14.4 3.2C15.2837 3.2 16 2.48366 16 1.6C16 0.716344 15.2837 0 14.4 0C13.5163 0 12.8 0.716344 12.8 1.6Z" fill="#1B1C1E" />
                                            </svg>
                                        </Dropdown.Toggle>
                                        {element.status === "Ongoing" && "Not started" ? (
                                            <Dropdown.Menu>
                                                <Dropdown.Item eventKey="Set as completed">Set as completed</Dropdown.Item>
                                                <Dropdown.Item eventKey="Delete">Delete</Dropdown.Item>
                                            </Dropdown.Menu>
                                        ) :
                                            <Dropdown.Menu>
                                                <Dropdown.Item eventKey="Edit">Edit</Dropdown.Item>
                                                <Dropdown.Item eventKey="Delete">Delete</Dropdown.Item>
                                            </Dropdown.Menu>
                                        }
                                    </Dropdown>
                                </div>
                            </div>
                        </div>
                        <div className="midcard">
                            <div className="cardtag">
                                <p>{element.goal}</p>
                            </div>
                            <div className="cardtitle">
                                <p>{element.planname}</p>
                            </div>
                            <p className="plansubtopic"><span>{element.data}</span></p>
                            {element.advisorname === false ? (
                                <div className="advname">
                                    <p className="plansubtopic">No advisors</p>
                                </div>
                            ) :
                                <div className="advimage">
                                    <TwoUserIcons AvatarIconData={AvatarIconData} />
                                </div>
                            }
                            <div className="nextmilestone">
                                <p className="nextmilestonetext">Current milestone<span><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2.5 8H13.5" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                    <path d="M9 3.5L13.5 8L9 12.5" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                </svg></span></p>
                            </div>
                        </div>
                    </div>
                </>
            );
        })

        return (
            <>

                <div className="plancnt">
                    <div className="plantopsec">
                        <div className="row">
                            <div className="col-sm-4 col-md-6 col-lg-6 col-xl-6 col-xxl-3">
                                <Search placeholder="Search" />
                            </div>
                        </div>
                    </div>
                    <div className="cardcnts col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        {this.state.Plans && this.state.Plans.length > 0 &&
                            <div className="row">
                                {Plansdetails}
                            </div>
                        }
                    </div>
                    <div className="">
                        <Modal
                            size={'lg'}
                            show={show ? show : false}
                            fullscreen={true}
                            onHide={(e) => this.setShow(e)}
                            centered={true}
                            dialogclassName={"mywork-popup"}
                        >
                            <Modal.Header closeButton>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="modal-form">
                                    <div className="full-height">
                                        <div className="vertical-center">
                                            <div className="container">
                                                <div className="Templates_2 text-center role">
                                                    <h1 className="login_title text-center">Edit plan</h1>
                                                </div>
                                                <div data-step="step1" className="mywork-popup-steps show">
                                                    <div className="Templates_2 text-center role">
                                                        <div className="goals__form mt-2 mb-2">
                                                            <div className="row">
                                                                <div className="col-md-12 mb-2 role__btns">
                                                                    <input type="text" className="goals__form__inputs" placeholder="Plan Name" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <p className="login_desc text-center bluetext">Choose as many options as you want.</p>
                                                        <div className="goals_page">
                                                            <div className="mb-2 role__btns">
                                                                <input type="checkbox" defaultChecked={UniversityCheckBox} onClick={(e) => this.handleCheckboxChange(e, 'universities')} className="btn-check" name="options1" id="btncheck1" autoComplete="off" />
                                                                <label className="btn cta--role--btn" htmlFor="btncheck1">Get into specific universities</label>
                                                            </div>
                                                            <div className=" mb-2 role__btns">
                                                                <input type="checkbox" defaultChecked={ProfessionsCheckBox} onClick={(e) => this.handleCheckboxChange(e, 'profession')} className="btn-check" name="options2" id="btncheck2" autoComplete="off" />
                                                                <label className="btn cta--role--btn" htmlFor="btncheck2">Get into a specific profession</label>
                                                            </div>
                                                            <div className=" mb-2 role__btns">
                                                                <input type="checkbox" defaultChecked={GPACheckBox} onClick={(e) => this.handleCheckboxChange(e, 'GPA')} className="btn-check" name="options3" id="btncheck3" autoComplete="off" />
                                                                <label className="btn cta--role--btn" htmlFor="btncheck3">Improve my GPA</label>
                                                            </div>
                                                        </div>

                                                        <div className="goals__form">
                                                            {UniversityCheckBox &&
                                                                <div className="row">
                                                                    <div className="col-md-12 mb-2 role__btns">
                                                                        <Select
                                                                            className="goals__form__select mb-3"
                                                                            closeMenuOnSelect={false}
                                                                            isMulti
                                                                            options={UniversityOptionstions}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            }
                                                            {ProfessionsCheckBox &&
                                                                <div className="row">
                                                                    <div className="col-md-12 mb-2 role__btns">
                                                                        <Select
                                                                            className="goals__form__select mb-3"
                                                                            closeMenuOnSelect={false}
                                                                            isMulti
                                                                            options={ProfessionsOptionstions}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            }
                                                            {GPACheckBox &&
                                                                <div className="row">
                                                                    <div className="col-md-12 mb-2 role__btns">
                                                                        <input type="text" className="goals__form__inputs" placeholder="GPA" />
                                                                    </div>
                                                                </div>
                                                            }

                                                            <div className="row">
                                                                <div className="col-md-12 role__btns">
                                                                    <Select
                                                                        className="goals__form__select mb-3"
                                                                        closeMenuOnSelect={false}
                                                                        options={ProfessionsOptionstions}
                                                                        placeholder="Assign existing advisor"
                                                                    />
                                                                </div>
                                                                <p className="t-right">*Optional</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="footer1" style={{ marginTop: "170px" }}>
                                                        <div className="row m-0">
                                                            <div className="footer__center col-md-8 col-sm-8 col-4 ">
                                                                <p>1 out of 2 steps</p>
                                                            </div>
                                                            <div className="footer__right col-md-4 col-sm-4 col-4">
                                                                <a href="" onClick={(e) => this.handleNextStep(e, "step2")} className="footer__cta">Next step</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-step="step2" className="mywork-popup-steps">
                                                    <div className="initial_plan">
                                                        <div className="row">
                                                            <div className="cols-m-12">
                                                                <div id="initial_plan_clone">
                                                                    <div className="initial_plan_parent mb-3" id="initial_plan_parent">
                                                                        <h5 className="Phase_count">Phase 1:</h5>
                                                                        <div className="row m-0  mb-2">
                                                                            <div className="col-sm-6 col-12">
                                                                                <div className="row">
                                                                                    <div className="col-3 p-0"><p className="initial_plan_label">Starts in:</p></div>
                                                                                    <div className="col-9 p-0">
                                                                                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path d="M15.4688 3.23438H12.5156V2.10938C12.5156 2.03203 12.4523 1.96875 12.375 1.96875H11.3906C11.3133 1.96875 11.25 2.03203 11.25 2.10938V3.23438H6.75V2.10938C6.75 2.03203 6.68672 1.96875 6.60938 1.96875H5.625C5.54766 1.96875 5.48438 2.03203 5.48438 2.10938V3.23438H2.53125C2.22012 3.23438 1.96875 3.48574 1.96875 3.79688V15.4688C1.96875 15.7799 2.22012 16.0312 2.53125 16.0312H15.4688C15.7799 16.0312 16.0312 15.7799 16.0312 15.4688V3.79688C16.0312 3.48574 15.7799 3.23438 15.4688 3.23438ZM14.7656 14.7656H3.23438V8.08594H14.7656V14.7656ZM3.23438 6.89062V4.5H5.48438V5.34375C5.48438 5.42109 5.54766 5.48438 5.625 5.48438H6.60938C6.68672 5.48438 6.75 5.42109 6.75 5.34375V4.5H11.25V5.34375C11.25 5.42109 11.3133 5.48438 11.3906 5.48438H12.375C12.4523 5.48438 12.5156 5.42109 12.5156 5.34375V4.5H14.7656V6.89062H3.23438Z" fill="#BFBFBF" />
                                                                                        </svg>
                                                                                        <div className="react-select-noborder">
                                                                                            <Select placeholder="Choose year" styles={style1} options={options} components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }} />
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="col-sm-6 col-12">
                                                                                <div className="row">
                                                                                    <div className="col-3 p-0"><p className="initial_plan_label">Ends in:</p></div>
                                                                                    <div className="col-9 p-0">
                                                                                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path d="M15.4688 3.23438H12.5156V2.10938C12.5156 2.03203 12.4523 1.96875 12.375 1.96875H11.3906C11.3133 1.96875 11.25 2.03203 11.25 2.10938V3.23438H6.75V2.10938C6.75 2.03203 6.68672 1.96875 6.60938 1.96875H5.625C5.54766 1.96875 5.48438 2.03203 5.48438 2.10938V3.23438H2.53125C2.22012 3.23438 1.96875 3.48574 1.96875 3.79688V15.4688C1.96875 15.7799 2.22012 16.0312 2.53125 16.0312H15.4688C15.7799 16.0312 16.0312 15.7799 16.0312 15.4688V3.79688C16.0312 3.48574 15.7799 3.23438 15.4688 3.23438ZM14.7656 14.7656H3.23438V8.08594H14.7656V14.7656ZM3.23438 6.89062V4.5H5.48438V5.34375C5.48438 5.42109 5.54766 5.48438 5.625 5.48438H6.60938C6.68672 5.48438 6.75 5.42109 6.75 5.34375V4.5H11.25V5.34375C11.25 5.42109 11.3133 5.48438 11.3906 5.48438H12.375C12.4523 5.48438 12.5156 5.42109 12.5156 5.34375V4.5H14.7656V6.89062H3.23438Z" fill="#BFBFBF" />
                                                                                        </svg>
                                                                                        <div className="react-select-noborder">
                                                                                            <Select placeholder="Choose year" styles={style1} options={options} components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }} />
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        {isShowPan &&
                                                                            <div className="row m-0"  >
                                                                                <div className="col-12 p-0 mb-4">
                                                                                    <div className="">
                                                                                        <Select placeholder="Course name" options={options1} />
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-12 p-0 mb-4">
                                                                                    <div className="">
                                                                                        <Select placeholder="Course type" options={options2} />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        }

                                                                        <div className="row m-0">
                                                                            <div className="col-6 p-0">
                                                                                <a href="" className="cta cta--rounded cta--plan m-0" onClick={(e) => this.handleShowPlan(e)} >
                                                                                    <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                        <path d="M9.16658 2.33301C9.16658 2.06779 9.06123 1.81344 8.87369 1.6259C8.68615 1.43836 8.4318 1.33301 8.16658 1.33301C7.90137 1.33301 7.64701 1.43836 7.45948 1.6259C7.27194 1.81344 7.16658 2.06779 7.16658 2.33301V6.66634H2.83325C2.56804 6.66634 2.31368 6.7717 2.12615 6.95923C1.93861 7.14677 1.83325 7.40112 1.83325 7.66634C1.83325 7.93156 1.93861 8.18591 2.12615 8.37345C2.31368 8.56098 2.56804 8.66634 2.83325 8.66634H7.16658V12.9997C7.16658 13.2649 7.27194 13.5192 7.45948 13.7068C7.64701 13.8943 7.90137 13.9997 8.16658 13.9997C8.4318 13.9997 8.68615 13.8943 8.87369 13.7068C9.06123 13.5192 9.16658 13.2649 9.16658 12.9997V8.66634H13.4999C13.7651 8.66634 14.0195 8.56098 14.207 8.37345C14.3946 8.18591 14.4999 7.93156 14.4999 7.66634C14.4999 7.40112 14.3946 7.14677 14.207 6.95923C14.0195 6.7717 13.7651 6.66634 13.4999 6.66634H9.16658V2.33301Z" fill="#1B1C1E" />
                                                                                    </svg>
                                                                                    Add Course
                                                                                </a>
                                                                            </div>
                                                                            <div className="col-6 pe-0">
                                                                                <a href="" className="cta cta--rounded cta--plan m-0" onClick={(e) => this.handleShowPlan(e)} >
                                                                                    <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                        <path d="M9.16658 2.33301C9.16658 2.06779 9.06123 1.81344 8.87369 1.6259C8.68615 1.43836 8.4318 1.33301 8.16658 1.33301C7.90137 1.33301 7.64701 1.43836 7.45948 1.6259C7.27194 1.81344 7.16658 2.06779 7.16658 2.33301V6.66634H2.83325C2.56804 6.66634 2.31368 6.7717 2.12615 6.95923C1.93861 7.14677 1.83325 7.40112 1.83325 7.66634C1.83325 7.93156 1.93861 8.18591 2.12615 8.37345C2.31368 8.56098 2.56804 8.66634 2.83325 8.66634H7.16658V12.9997C7.16658 13.2649 7.27194 13.5192 7.45948 13.7068C7.64701 13.8943 7.90137 13.9997 8.16658 13.9997C8.4318 13.9997 8.68615 13.8943 8.87369 13.7068C9.06123 13.5192 9.16658 13.2649 9.16658 12.9997V8.66634H13.4999C13.7651 8.66634 14.0195 8.56098 14.207 8.37345C14.3946 8.18591 14.4999 7.93156 14.4999 7.66634C14.4999 7.40112 14.3946 7.14677 14.207 6.95923C14.0195 6.7717 13.7651 6.66634 13.4999 6.66634H9.16658V2.33301Z" fill="#1B1C1E" />
                                                                                    </svg>
                                                                                    Add Activity
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row m-0">
                                                            <div className="col-12 p-0 text-center mt-4">
                                                                <a href="" onClick={(e) => this.handleClickAddPhase(e)} className=""><svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M9.16658 2.33301C9.16658 2.06779 9.06123 1.81344 8.87369 1.6259C8.68615 1.43836 8.4318 1.33301 8.16658 1.33301C7.90137 1.33301 7.64701 1.43836 7.45948 1.6259C7.27194 1.81344 7.16658 2.06779 7.16658 2.33301V6.66634H2.83325C2.56804 6.66634 2.31368 6.7717 2.12615 6.95923C1.93861 7.14677 1.83325 7.40112 1.83325 7.66634C1.83325 7.93156 1.93861 8.18591 2.12615 8.37345C2.31368 8.56098 2.56804 8.66634 2.83325 8.66634H7.16658V12.9997C7.16658 13.2649 7.27194 13.5192 7.45948 13.7068C7.64701 13.8943 7.90137 13.9997 8.16658 13.9997C8.4318 13.9997 8.68615 13.8943 8.87369 13.7068C9.06123 13.5192 9.16658 13.2649 9.16658 12.9997V8.66634H13.4999C13.7651 8.66634 14.0195 8.56098 14.207 8.37345C14.3946 8.18591 14.4999 7.93156 14.4999 7.66634C14.4999 7.40112 14.3946 7.14677 14.207 6.95923C14.0195 6.7717 13.7651 6.66634 13.4999 6.66634H9.16658V2.33301Z" fill="#1B1C1E" />
                                                                </svg>Add Phase</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="footer1" style={{ marginTop: "170px" }}>
                                                        <div className="row m-0">
                                                            <div className="footer__left col-md-4 col-sm-4 col-4">
                                                                <p>
                                                                    <span className="pe-3">2 out of 2 steps</span>
                                                                    <a href="" onClick={(e) => this.handleNextStep(e, "step1")} className="footer__left__cta">
                                                                        <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                            <path fillRule="evenodd" clipRule="evenodd" d="M5.80474 0.195262C5.54439 -0.0650874 5.12228 -0.0650874 4.86193 0.195262L0.195262 4.86193C-0.0650873 5.12228 -0.0650873 5.54439 0.195262 5.80474L4.86193 10.4714C5.12228 10.7318 5.54439 10.7318 5.80474 10.4714C6.06509 10.2111 6.06509 9.78894 5.80474 9.52859L1.60948 5.33333L5.80474 1.13807C6.06509 0.877722 6.06509 0.455612 5.80474 0.195262Z" fill="#1B1C1E" />
                                                                        </svg>
                                                                        <span>Go back</span>
                                                                    </a>
                                                                </p>
                                                            </div>
                                                            <div className="footer__center col-md-4 col-sm-4 col-4 text-center">
                                                                <p>Get an advisor to help you create a Plan</p>
                                                            </div>
                                                            <div className="footer__right col-md-4 col-sm-4 col-4 text-center">
                                                                <a href="" onClick={(e) => this.setShow(e)} className="footer__cta">Save Changes</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Modal.Body>
                        </Modal>


                        <Modal show={this.state.deleteshow} onHide={(e) => this.deletehandleClose(e)}>
                            <Modal.Header closeButton>
                                <Modal.Title>Delete Confirmation</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>Are you sure? want to delete the course</Modal.Body>
                            <Modal.Footer>
                                <Button variant="outline-secondary" onClick={(e) => this.deletehandleClose(e)}>
                                    Cancel
                                </Button>
                                <Button variant="outline-danger" onClick={(e) => this.confirmdelete(e)}>
                                    Delete
                                </Button>
                            </Modal.Footer>
                        </Modal>
                        <NotificationContainer />
                    </div>
                </div>

            </>
        )
    }
}

export default Index;