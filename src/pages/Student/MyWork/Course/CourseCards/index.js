import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { Modal } from "react-bootstrap";
import Dropdown from 'react-bootstrap/Dropdown'
import Select from 'react-select';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import TwoUserIcons from "../../../../../components/TwoUserIcons"
import Search from "../../../../../components/Search";
import Avatar1 from "../../../../../assets/images/icons/Avatar1.png"
import Avatar2 from "../../../../../assets/images/icons/Avatar2.png"
import "../../../../../assets/css/work.less"



class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coursedropdown: [
                {
                    data: "Set as completed",
                },
                {
                    data: "Delete"
                }

            ],
            Plans: [
                {
                    id: 1,
                    status: "Active",
                    goal: "Get into university",
                    coursename: "Algebra",
                    planname: "Admission to Cambridge",
                    data: "2021, (1- semestre)",
                    advisorname: true
                },
                {
                    id: 2,
                    status: "Ongoing",
                    goal: "Improve GPA",
                    coursename: "Economics",
                    planname: "Admission to Cambridge",
                    data: "2021, (1- semestre)",
                    advisorname: true
                },
                {
                    id: 3,
                    status: "Completed",
                    goal: "Science",
                    coursename: "History",
                    planname: "Admission to Cambridge",
                    data: "2021, (1- semestre)",
                    advisorname: true
                },
                {
                    id: 4,
                    status: "Not started",
                    goal: "Improve GPA",
                    coursename: "Science",
                    planname: "Algebra",
                    data: "2022, (2- semestre)",
                    advisorname: true,
                },
                {
                    id: 5,
                    status: "Ongoing",
                    goal: "Improve GPA",
                    coursename: "Industry",
                    planname: "Economy",
                    data: "2021, (1- semestre)",
                    advisorname: true
                },
            ]

        };
    }
    openplansDropDown = (e) => {
        this.setState({
            showDropDown: !this.state.showDropDown
        });

    }
    closeplansDropDown = (e) => {
        this.setState({
            showDropDown: false
        });
    }


    render() {
        let Coursedropdowndata = this.state.coursedropdown.map((x) => {
            return (
                <li>
                    {x.data}
                </li>
            )
        })
        const AvatarIconData = [
            {
                'AvatarIcon': Avatar1,
                'AvatarIconAlt': 'Avatar1-icon'
            },
            {
                'AvatarIcon': Avatar2,
                'AvatarIconAlt': 'Avatar2-icon'
            }
        ];
        let Plansdetails = this.state.Plans.map((element, key) => {
            return (
                <>
                    <div className="coursecard">
                        <div className="cardtops">
                            <div className="row">
                                <div className="col">
                                    <Button className="cardbtn" style={{ 'border': "none", 'backgroundColor': element.status === 'Active' ? '#16DAF1' : element.status === 'Ongoing' ? '#FFCC5A' : element.status === 'Completed' ? '#34C38F' : element.status === 'Not started' ? '#1C84EE' : '#1C84EE' }}>{element.status}</Button>
                                </div>
                                <div className="col cardgrid__dot endcols" onClick={(e) => this.openDropDown(e)}>
                                    <Dropdown>
                                        <Dropdown.Toggle id="dropdown-basic">
                                            <svg width="16" height="4" viewBox="0 0 16 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fillRule="evenodd" clipRule="evenodd" d="M1.6 3.2C0.716344 3.2 0 2.48366 0 1.6C0 0.716344 0.716344 0 1.6 0C2.48366 0 3.2 0.716344 3.2 1.6C3.2 2.48366 2.48366 3.2 1.6 3.2ZM7.99844 3.2C7.11478 3.2 6.39844 2.48366 6.39844 1.6C6.39844 0.716344 7.11478 0 7.99844 0C8.88209 0 9.59844 0.716344 9.59844 1.6C9.59844 2.48366 8.88209 3.2 7.99844 3.2ZM12.8 1.6C12.8 2.48366 13.5163 3.2 14.4 3.2C15.2837 3.2 16 2.48366 16 1.6C16 0.716344 15.2837 0 14.4 0C13.5163 0 12.8 0.716344 12.8 1.6Z" fill="#1B1C1E" />
                                            </svg>
                                        </Dropdown.Toggle>
                                        <div className="dropDowns">
                                            <Dropdown.Menu >
                                                <Dropdown.Item >{Coursedropdowndata}</Dropdown.Item>
                                            </Dropdown.Menu>
                                        </div>
                                    </Dropdown>
                                </div>
                                <div className="midcard">
                                    <div className="cardtag">
                                        <p>{element.goal}</p>
                                    </div>
                                    <div className="cardtitle">
                                        <p>{element.planname}</p>
                                    </div>
                                    <p className="plancardtopic">Plan:<span>{element.planname}</span></p>
                                    <p className="plansubtopic">Phase:<span>{element.data}</span></p>
                                    {element.advisorname === false ? (
                                        <div className="advname">
                                            <p className="plansubtopic">No advisors</p>
                                        </div>
                                    ) :
                                        <div className="advimage">
                                            <TwoUserIcons AvatarIconData={AvatarIconData} />
                                        </div>
                                    }
                                    <div className="nextmilestone">
                                        <p className="nextmilestonetext">Current milestone<span><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M2.5 8H13.5" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                            <path d="M9 3.5L13.5 8L9 12.5" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            );
        })

        return (
            <><div className="plancnt">
                <div className="plantopsec">
                    <div className="row">
                        <div className="col-sm-4 col-md-6 col-lg-6 col-xl-6 col-xxl-3">
                            <Search placeholder="Search" />
                        </div>
                    </div>
                </div>
                <div className="cardcnts col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    {this.state.Plans && this.state.Plans.length > 0 &&
                        <div className="row">
                            {Plansdetails}
                        </div>
                    }
                </div>
            </div>
            </>
        )
    }
}

export default Index;