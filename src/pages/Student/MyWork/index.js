import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { Tab, Nav, Modal } from "react-bootstrap";
import Select from 'react-select';

import TwoUserIcons from "../../../components/TwoUserIcons"
import Avatar1 from "../../../assets/images/icons/Avatar1.png"
import Avatar2 from "../../../assets/images/icons/Avatar2.png"
import MyPlans from "./Plans"
import MyCourse from "./Course"
import MyMilestones from "./Milestones"
import "../../../assets/css/work.less"
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nodata: false,
            key: "Plans",
            show: false,
        };
    }
    setKey = (k) => {
        this.setState({
            key: k,
        });
    }
    setdata = () => {
        this.setState({
            nodata: false
        });
    }
    handleShow(e) {
        e.preventDefault();
        this.setState({ show: true })
    }

    setShow() {
        this.setState({ show: false })
    }
    render() {
        const AvatarIconData = [
            {
                'AvatarIcon': Avatar1,
                'AvatarIconAlt': 'Avatar1-icon'
            },
            {
                'AvatarIcon': Avatar2,
                'AvatarIconAlt': 'Avatar2-icon'
            },
            {
                'AvatarIcon': Avatar1,
                'AvatarIconAlt': 'Avatar2-icon'
            }
        ];
        let { show, nodata } = this.state;
        return (
            <>
                <div className="workcnt">

                    {/* NO DATA WORK SECTION STARTS */}
                    {nodata === true ? (
                        <div className="nodataworksection">
                            {/* Topbar Mobile view */}
                            <div className="mobileworktopsec">
                                <div className="workmobiletopbar">
                                    <div className="mobileworktitle">
                                        <p>My work</p>
                                    </div>
                                </div>
                                <br />
                                <div className="workbtncnt">
                                    <div className="mobiletitlebtm col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                                        <div className="worktoplayer">
                                            <div className="endbtn">
                                                <a className="btn planbtn">Create new plan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Topbar Mobile view */}
                            {/* Topbar Desktop view */}
                            <div className="worktopsec">
                                <div className="row">
                                    <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 worktitle">
                                        <h3>My work</h3>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 workbtncnt">
                                        <div className="worktitlebtn endcols">

                                            <Button className="btn planbtn" onClick={(e) => this.handleShow(e)}>
                                                Create new plan
                                            </Button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Topbar Desktop view */}
                            {/* Midbar Section */}
                            <div className="workcentersec">
                                <div className="whitesec">
                                    <a >
                                        <div className="midwhitesec">
                                            <div className="boxadd">
                                                <div className="boxes">
                                                    <svg className="plusicon" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fillRule="evenodd" clipRule="evenodd" d="M50 12.5C50.8288 12.5 51.6237 12.8292 52.2097 13.4153C52.7958 14.0013 53.125 14.7962 53.125 15.625V46.875H84.375C85.2038 46.875 85.9987 47.2042 86.5847 47.7903C87.1708 48.3763 87.5 49.1712 87.5 50C87.5 50.8288 87.1708 51.6237 86.5847 52.2097C85.9987 52.7958 85.2038 53.125 84.375 53.125H53.125V84.375C53.125 85.2038 52.7958 85.9987 52.2097 86.5847C51.6237 87.1708 50.8288 87.5 50 87.5C49.1712 87.5 48.3763 87.1708 47.7903 86.5847C47.2042 85.9987 46.875 85.2038 46.875 84.375V53.125H15.625C14.7962 53.125 14.0013 52.7958 13.4153 52.2097C12.8292 51.6237 12.5 50.8288 12.5 50C12.5 49.1712 12.8292 48.3763 13.4153 47.7903C14.0013 47.2042 14.7962 46.875 15.625 46.875H46.875V15.625C46.875 14.7962 47.2042 14.0013 47.7903 13.4153C48.3763 12.8292 49.1712 12.5 50 12.5Z" fill="#1c84ee" />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="workbottomsec">
                                <div className="bottomtitle">
                                    <p>You do not have any plans yet. Do you want an advisor to create it for you?</p>
                                </div>
                                <div className="bottompic">
                                    <div className="btmseg">
                                        <p>Explore advisors
                                            <span>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" clipRule="evenodd" d="M2.66663 7.99967C2.66663 7.63148 2.9651 7.33301 3.33329 7.33301H12.6666C13.0348 7.33301 13.3333 7.63148 13.3333 7.99967C13.3333 8.36786 13.0348 8.66634 12.6666 8.66634H3.33329C2.9651 8.66634 2.66663 8.36786 2.66663 7.99967Z" fill="#3F51B5" />
                                                    <path fillRule="evenodd" clipRule="evenodd" d="M7.52864 2.86225C7.78899 2.6019 8.2111 2.6019 8.47145 2.86225L13.1381 7.52892C13.3985 7.78927 13.3985 8.21138 13.1381 8.47173L8.47145 13.1384C8.2111 13.3987 7.78899 13.3987 7.52864 13.1384C7.26829 12.878 7.26829 12.4559 7.52864 12.1956L11.7239 8.00033L7.52864 3.80506C7.26829 3.54471 7.26829 3.1226 7.52864 2.86225Z" fill="#3F51B5" />
                                                </svg>
                                            </span>
                                        </p>
                                    </div>
                                    <div className="bottomicon">
                                        <TwoUserIcons AvatarIconData={AvatarIconData} />
                                    </div>
                                </div>
                            </div>
                            {/* Midbar Section */}
                        </div>
                        // NO DATA WORK SECTION ENDS 
                    ) :
                        // WITH DATA WORK SECTION STARTS 

                        <div className="dataworksection">
                            {/* Topbar Mobile view */}
                            <div className="mobileworktopsec">
                                <div className="workmobiletopbar">
                                    <div className="mobileworktitle">
                                        <p>My work</p>
                                    </div>
                                </div>
                                <br />
                                <div className="workbtncnt">
                                    <div className="mobiletitlebtm col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                                        <div className="worktoplayer">
                                            <div className="segment">
                                                <div className="revicon">
                                                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect x="6.66699" y="8.74609" width="18.3795" height="17.0667" rx="1.5" stroke="#1B1C1E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M25.0452 14.4347H6.66571" stroke="#1B1C1E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M11.262 5.33203V7.60759" stroke="#1B1C1E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M20.4515 5.33203V7.60759" stroke="#1B1C1E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                    </svg>
                                                </div>
                                                <div className="revicon">
                                                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10 20H26C26.2652 20 26.5196 19.8946 26.7071 19.7071C26.8946 19.5196 27 19.2652 27 19V6C27 5.73478 26.8946 5.48043 26.7071 5.29289C26.5196 5.10536 26.2652 5 26 5H12C11.7348 5 11.4804 5.10536 11.2929 5.29289C11.1054 5.48043 11 5.73478 11 6V7" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M13 17L10 20L13 23" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M22 12H6C5.73478 12 5.48043 12.1054 5.29289 12.2929C5.10536 12.4804 5 12.7348 5 13V26C5 26.2652 5.10536 26.5196 5.29289 26.7071C5.48043 26.8946 5.73478 27 6 27H20C20.2652 27 20.5196 26.8946 20.7071 26.7071C20.8946 26.5196 21 26.2652 21 26V25" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M19 15L22 12L19 9" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                    </svg>
                                                </div>
                                                <div className="endbtn">
                                                    <a>
                                                        <Button className="btn planbtn">
                                                            Create new plan
                                                        </Button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Topbar Mobile view */}
                            {/* Topbar Desktop view */}
                            <div className="worktopsec">
                                <div className="row">
                                    <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 worktitle">
                                        <h3>My work</h3>
                                    </div>
                                    <div className="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 workbtncnt">
                                        <div className="worktitlebtn endcols">
                                            <div className="segment">
                                                <div className="revicon">
                                                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect x="6.66699" y="8.74609" width="18.3795" height="17.0667" rx="1.5" stroke="#1B1C1E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M25.0452 14.4347H6.66571" stroke="#1B1C1E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M11.262 5.33203V7.60759" stroke="#1B1C1E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M20.4515 5.33203V7.60759" stroke="#1B1C1E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                    </svg>
                                                </div>
                                                <div className="revicon">
                                                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10 20H26C26.2652 20 26.5196 19.8946 26.7071 19.7071C26.8946 19.5196 27 19.2652 27 19V6C27 5.73478 26.8946 5.48043 26.7071 5.29289C26.5196 5.10536 26.2652 5 26 5H12C11.7348 5 11.4804 5.10536 11.2929 5.29289C11.1054 5.48043 11 5.73478 11 6V7" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M13 17L10 20L13 23" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M22 12H6C5.73478 12 5.48043 12.1054 5.29289 12.2929C5.10536 12.4804 5 12.7348 5 13V26C5 26.2652 5.10536 26.5196 5.29289 26.7071C5.48043 26.8946 5.73478 27 6 27H20C20.2652 27 20.5196 26.8946 20.7071 26.7071C20.8946 26.5196 21 26.2652 21 26V25" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        <path d="M19 15L22 12L19 9" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                    </svg>
                                                </div>
                                                <div className="endbtn">
                                                    <a>
                                                        <Button className="btn planbtn">
                                                            Create new plan
                                                        </Button>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Topbar Desktop view */}
                            {/* Midbar Section */}
                            <div className="workcentersec">
                                <Tab.Container id="left-tabs-example" activeKey={this.state.key} onSelect={(k) => this.setKey(k)}>
                                    <div className="row">
                                        <div classname="col-sm-6 col-lg-12 col-xl-12">
                                            <Nav variant="pills" >
                                                <Nav.Item>
                                                    <Nav.Link eventKey="Plans">Plans</Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item>
                                                    <Nav.Link eventKey="Courses">Courses</Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item>
                                                    <Nav.Link eventKey="Milestones">Milestones</Nav.Link>
                                                </Nav.Item>
                                            </Nav>
                                        </div>
                                        <div classname="col-sm-6 col-lg-12 col-xl-12">
                                            <Tab.Content>
                                                <Tab.Pane eventKey="Plans">
                                                    <MyPlans/>
                                                </Tab.Pane>
                                                <Tab.Pane eventKey="Courses">
                                                    <MyCourse />
                                                </Tab.Pane>
                                                <Tab.Pane eventKey="Milestones">
                                                    <MyMilestones />
                                                </Tab.Pane>
                                            </Tab.Content>
                                        </div>
                                    </div>
                                </Tab.Container>
                            </div>

                            {/* Midbar Section */}
                        </div>
                    }
                    {/* WITH DATA WORK SECTION ENDS */}
                </div>
                <Modal size={'lg'} show={show ? show : false} fullscreen={true} onHide={(e) => this.setShow(e)}
                    centered={true} dialogClassName={"mywork-popup"}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <svg onClick={(e) => this.setShow(e)} width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd" d="M26.6665 16.0003C26.6665 15.2639 26.0696 14.667 25.3332 14.667H6.66651C5.93012 14.667 5.33317 15.2639 5.33317 16.0003C5.33317 16.7367 5.93012 17.3337 6.66651 17.3337H25.3332C26.0696 17.3337 26.6665 16.7367 26.6665 16.0003Z" fill="#1B1C1E" />
                                <path fillRule="evenodd" clipRule="evenodd" d="M16.943 5.72353C16.4223 5.20283 15.5781 5.20283 15.0574 5.72353L5.72402 15.0569C5.20332 15.5776 5.20332 16.4218 5.72402 16.9425L15.0574 26.2758C15.5781 26.7965 16.4223 26.7965 16.943 26.2758C17.4637 25.7551 17.4637 24.9109 16.943 24.3902L8.55245 15.9997L16.943 7.60915C17.4637 7.08845 17.4637 6.24423 16.943 5.72353Z" fill="#1B1C1E" />
                            </svg>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                    </Modal.Body>
                </Modal>
            </>
        );
    }
}

export default Index;