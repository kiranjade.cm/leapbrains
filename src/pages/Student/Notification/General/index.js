import React, { Component } from "react";
import "../../../../assets/css/notification.less";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    render() {

        return (
            <>
                <div className="container-general">
                    <div className='row'>
                        <div className="col-xl-10 col-lg-12 col-md-11 col-sm-11 col-11 general-notify-box">
                            <div className='notify-box'><span className='msg'>You do not have any notifications at the moment.</span></div>
                        </div>
                    </div>
                </div>

            </>
        );
    }
}

export default Index;