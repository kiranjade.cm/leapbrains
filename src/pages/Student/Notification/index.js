import React, { Component } from "react";
import '../../../assets/css/notification.less';
import Search from "../../../components/Search";
import General from './General/index';
import Change from './Change/index';
import { Tab } from 'react-bootstrap'
import { Nav } from 'react-bootstrap'

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nodata: true,
            key: "general",
            isActive: false,
            advisorActive: false,
            planActive: false,
            courseActive: false,
            phaseActive: false,
            milestoneActive: false,
        };
        this.advisorClick = this.advisorClick.bind(this);
        this.planClick = this.planClick.bind(this);
        this.courseClick = this.courseClick.bind(this);
        this.phaseClick = this.phaseClick.bind(this);
        this.milestoneClick = this.milestoneClick.bind(this);
    }
    setKey = (k) => {
        this.setState({
            key: k,
        });
    }
    handleClickMobilesearch(e) {
        e.preventDefault();
        const currentState = this.state.isActive;
        this.setState({ isActive: !currentState });

    }
    advisorClick() {
        const advisorState = this.state.advisorActive;
        this.setState({
            advisorActive: !advisorState
        });
    }
    planClick() {
        const planState = this.state.planActive;
        this.setState({
            planActive: !planState
        });
    }
    courseClick() {
        const courseState = this.state.courseActive;
        this.setState({
            courseActive: !courseState
        });
    }
    phaseClick() {
        const phaseState = this.state.phaseActive;
        this.setState({
            phaseActive: !phaseState
        });
    }
    milestoneClick() {
        const milestoneState = this.state.milestoneActive;
        this.setState({
            milestoneActive: !milestoneState
        });
    }
    render() {
        let { isActive } = this.state;

        return (
            <>
                <div className="Centercnt notification-center">
                    <div className="dashtopsec">
                        <div className="row notify-head">
                            <div className=" col-xl-3 col-lg-4 col-md-5 col-sm-11 col-10 notify-title">
                                <h1>Notifications</h1>
                            </div>
                            <div className="col-xl-3 col-lg-4 col-md-5 col-sm-1 col-1 Search-content notify-search">
                                <Search placeholder="Search" />
                                <div className="mobile-search">
                                    <div class="mobile-search-content">
                                        <a href="javascript:void(0)" onClick={(e) => this.handleClickMobilesearch(e)} class="mobile-search-button">
                                            <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M14.5046 3.22982C11.3749 0.183809 6.28112 0.183809 3.15144 3.22982C0.0224364 6.2765 0.0224364 11.2335 3.15144 14.2801C5.93851 16.992 10.2792 17.2826 13.4069 15.1649C13.4727 15.468 13.6233 15.7572 13.8655 15.993L18.4233 20.4289C19.0875 21.0741 20.1608 21.0741 20.8216 20.4289C21.4852 19.7832 21.4852 18.7385 20.8216 18.0947L16.2638 13.6575C16.023 13.4237 15.7251 13.2765 15.4137 13.2124C17.5909 10.1677 17.2923 5.9437 14.5046 3.22982ZM13.0656 12.8796C10.729 15.1537 6.92633 15.1537 4.59043 12.8796C2.25521 10.6055 2.25521 6.90511 4.59043 4.631C6.92633 2.35755 10.729 2.35755 13.0656 4.631C15.4022 6.90511 15.4022 10.6055 13.0656 12.8796Z" fill="#1C84EE" />
                                            </svg>
                                        </a>
                                        {isActive && <input type="text" class="mobile-search-input" placeholder="Search here..." />}
                                    </div>
                                </div>
                            </div>
                            {/* <div className="col-xl-6 col-lg-2 col-md-2 text-end">
                                <a href="javascript:void(0)" className="cta--underline">
                                    Mark all as read
                                </a>
                            </div> */}
                        </div>
                    </div>
                    <div className="container-notify row">
                        <div className="col-md-8 col-lg-7 col-xl-6">
                            <div className="contain">
                                <Tab.Container id="left-tabs-example" activeKey={this.state.key} onSelect={(k) => this.setKey(k)}>
                                    <div className="row">
                                        <div classname="col-sm-6 col-lg-12 col-md-6 col-xl-12">
                                            <Nav variant="pills" >
                                                <Nav.Item>
                                                    <Nav.Link eventKey="general">General</Nav.Link>
                                                </Nav.Item>
                                                <Nav.Item>
                                                    <Nav.Link eventKey="change">Changes</Nav.Link>
                                                </Nav.Item>
                                            </Nav>
                                        </div>
                                        <div classname="col-sm-6 col-md-6 col-lg-12 col-xl-12">
                                            <Tab.Content>
                                                <Tab.Pane eventKey="general">
                                                    <General />
                                                </Tab.Pane>
                                                <Tab.Pane eventKey="change">
                                                    <Change />
                                                </Tab.Pane>
                                            </Tab.Content>
                                        </div>
                                    </div>
                                </Tab.Container>
                            </div>
                        </div>
                        <div className="col-md-4 notify-filter-box">
                            <div className="notify">
                                <div className="notify notification-filter">
                                    <div className="filter-main">
                                        <div className="filter-row">
                                            <div className="filter-name">Filter</div>
                                            {/* <a href="javascript:void(0)" className="fliter-link">Clear Filter</a> */}
                                            <a href="javascript:void(0)" className="cta--underline">
                                                Clear Filter
                                            </a>
                                        </div></div>
                                    <div className="filter-main">
                                        <div className="filter-row">
                                            <div className="filter-element-name">Advisors</div>
                                            <div onClick={this.advisorClick} className={this.state.advisorActive ? "Up" : "Down"}></div>
                                        </div>
                                        {this.state.advisorActive &&
                                            <ul className="p-0 mb-0">
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" />
                                                        <label htmlFor="styled-checkbox-1">
                                                            <span>Stevan Sanders</span>
                                                        </label>
                                                    </p>
                                                </li>
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" />
                                                        <label htmlFor="styled-checkbox-2">
                                                            <span>Monika Loopas</span>
                                                        </label>
                                                    </p>
                                                </li>
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value3" />
                                                        <label htmlFor="styled-checkbox-3">
                                                            <span>Bill Wilias</span>
                                                        </label>
                                                    </p>
                                                </li>
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value4" />
                                                        <label htmlFor="styled-checkbox-4">
                                                            <span>Alisa Dagth</span>
                                                        </label>
                                                    </p>
                                                </li>
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-5" type="checkbox" value="value5" />
                                                        <label htmlFor="styled-checkbox-5">
                                                            <span>Sid Biligan</span>
                                                        </label>
                                                    </p>
                                                </li>
                                            </ul>
                                        }
                                    </div>
                                    <div className="filter-main">
                                        <div className="filter-row">
                                            <div className="filter-element-name">Plan</div>
                                            <div onClick={this.planClick} className={this.state.planActive ? "Up" : "Down"}></div>
                                        </div>
                                        {this.state.planActive &&
                                            <ul className="p-0 mb-0">
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" />
                                                        <label htmlFor="styled-checkbox-1">
                                                            <span>Algebra</span>
                                                        </label>
                                                    </p>
                                                </li>
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" />
                                                        <label htmlFor="styled-checkbox-2">
                                                            <span>Admission To Cambridge</span>
                                                        </label>
                                                    </p>
                                                </li>
                                            </ul>
                                        }
                                    </div>
                                    <div className="filter-main">
                                        <div className="filter-row">
                                            <div className="filter-element-name">Course</div>
                                            <div onClick={this.courseClick} className={this.state.courseActive ? "Up" : "Down"}></div>
                                        </div>
                                        {this.state.courseActive &&
                                            <ul className="p-0 mb-0">
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" />
                                                        <label htmlFor="styled-checkbox-1">
                                                            <span>Option 1</span>
                                                        </label>
                                                    </p>
                                                </li>
                                            </ul>
                                        }
                                    </div>
                                    <div className="filter-main">
                                        <div className="filter-row">
                                            <div className="filter-element-name">Phase</div>
                                            <div onClick={this.phaseClick} className={this.state.phaseActive ? "Up" : "Down"}></div>
                                        </div>
                                        {this.state.phaseActive &&
                                            <ul className="p-0 mb-0">
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" />
                                                        <label htmlFor="styled-checkbox-1">
                                                            <span>Option 1</span>
                                                        </label>
                                                    </p>
                                                </li>
                                            </ul>
                                        }
                                    </div>
                                    <div className="filter-main">
                                        <div className="filter-row">
                                            <div className="filter-element-name">Milestone</div>
                                            <div onClick={this.milestoneClick} className={this.state.milestoneActive ? "Up" : "Down"}></div>
                                        </div>
                                        {this.state.milestoneActive &&
                                            <ul className="p-0 mb-0">
                                                <li className="d-block">
                                                    <p className="styled-checkbox-cont">
                                                        <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" />
                                                        <label htmlFor="styled-checkbox-1">
                                                            <span>Option 1</span>
                                                        </label>
                                                    </p>
                                                </li>
                                            </ul>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Index;