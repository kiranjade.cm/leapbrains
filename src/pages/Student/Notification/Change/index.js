import React, { Component } from "react";
import avator from '../../../../assets/images/icons/Avatar1.png'
import "../../../../assets/css/notification.less";



class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            NotifyList: [
                {
                    id: 1,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "37 m",
                    getMessage: " Hi, let me know when you go to library."
                },
                {
                    id: 2,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "54 m",
                    getMessage: "When will you start to read the book that i have send today."
                },
                {
                    id: 3,
                    activeStatus: "inactive",
                    getName: 'Stevan Sanders',
                    getMinute: "57 m",
                    getMessage: " Please inform your parents that I have recevied Money."
                },
                {
                    id: 4,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "37 m",
                    getMessage: " Hi, let me know when you go to library."
                },
                {
                    id: 5,
                    activeStatus: "active",
                    getName: 'Stevan Sanders',
                    getMinute: "54 m",
                    getMessage: "When will you start to read the book that i have send today."
                },
                {
                    id: 6,
                    activeStatus: "inactive",
                    getName: 'Stevan Sanders',
                    getMinute: "57 m",
                    getMessage: " Please inform your parents that I have recevied Money."
                },
            ]
        };
    }
    componentDidMount = () => {

    }
    render() {
        let NotificationDetails = this.state.NotifyList.map((element, key) => {
            return (
                <>
                    <div className="col-lg-12 col-md-12 col-sm-11 ">
                        <div className="container-changes ">
                            <div className={element.activeStatus}></div>
                            <div className='profile'>
                                <img src={avator} alt='img' />
                            </div>
                            <div className='change-box'>
                                <div className='top'>
                                    <div className='name'>{element.getName}</div>
                                    <div className='time-min'>{element.getMinute}</div>
                                </div>
                                <div className='message'>
                                    {element.getMessage}
                                </div>

                            </div>
                        </div>
                    </div>
                </>
            )

        })

        return (
            <>
                <div className="changecnt">
                    {this.state.NotifyList && this.state.NotifyList.length > 0 &&
                        <div className="row">
                            {NotificationDetails}
                        </div>
                    }
                </div>
            </>
        );
    }
}

export default Index;