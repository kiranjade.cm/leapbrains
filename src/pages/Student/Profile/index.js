import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import "../../../assets/css/studentprofile.less"
import Select, { components } from "react-select"
import { changePassword } from "../../../redux/actions/Auth"
import { connect } from "react-redux";
import { withRouter } from "../../../redux/store/navigate";
import { Link } from "react-router-dom";
import { setSession } from "../../../utils/AuthUtils"
import { getGender, getStatus } from "../../../redux/actions/Common";
import { setUserProfile, getUserProfile } from "../../../redux/actions/Auth";
import NotificationContainer from "react-notifications/lib/NotificationContainer";
import NotificationManager from "react-notifications/lib/NotificationManager";
const IDENTIFIER = process.env.REACT_APP_IDENTIFIER;

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            passwordfields: {},
            errors: {},
            loading: false,
            fields: {},
            errors: {},
            selectedGenderOption: null,
            selectedStatusOption: null,
            studentsCollabCheckBox: false,
            counselingAllowedCheckBox: false,
            subCouncelingServicesCheckBox: false,
            profile: null,
        };
        this.passwordhandleValidation = this.passwordhandleValidation.bind(this)
    }

    // componentDidUpdate() {
    //     if (this.props.isPasswordUpdate) {
    //         localStorage.removeItem("token");
    //         setSession(null)
    //         window.location.replace('/')
    //     }
    // }
    PasswordhandleChange(field, e) {
        let { errors } = this.state;
        let passwordfields = this.state.passwordfields;
        passwordfields[field] = e.target.value;
        if (e.target.value.length >= 0) {
            errors[field] = "";
        }
        this.setState({ passwordfields, errors: errors });
    }
    handleChange(field, e) {
        let { errors } = this.state;
        let fields = this.state.fields;
        fields[field] = e.target.value;
        if (e.target.value.length >= 0) {
            errors[field] = "";
        }
        this.setState({ fields, errors: errors });
    }
    passwordhandleValidation() {
        let passwordfields = this.state.passwordfields;
        let errors = {};
        let formIsValid = true;

        if (!passwordfields["password"]) {
            formIsValid = false;
            errors["password"] = "Password cannot be empty";
        }

        if (!passwordfields["newPassword"]) {
            formIsValid = false;
            errors["newPassword"] = "Password cannot be empty";
        }

        if (passwordfields["confirmPassword"] != passwordfields["newPassword"]) {
            formIsValid = false;
            errors["confirmPassword"] = "Mismatch in confirm password with new password";
        }

        if (!passwordfields["confirmPassword"]) {
            formIsValid = false;
            errors["confirmPassword"] = "Confirm password cannot be empty";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }
    passwordhandleSubmit(e) {
        e.preventDefault();
        let passwordfields = this.state.passwordfields;
        if (this.passwordhandleValidation()) {
            let values = {
                password: passwordfields["password"],
                newPassword: passwordfields["newPassword"],
                confirmPassword: passwordfields["confirmPassword"],
            }
            this.props.changePassword(values)
            if (this.props.isPasswordUpdate) {
                NotificationManager.success('Password Updated Sucessfully', 'Sucess!', 5000, () => {
                });
            }
        } else {
            e.preventDefault();
        }
    }

    handleCheckboxChange(e, buttonName) {
        let { studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox } = this.state;
        if (buttonName === "studentsCollab") {
            studentsCollabCheckBox = e.target.checked
        } else if (buttonName === "counselingAllowed") {
            counselingAllowedCheckBox = e.target.checked
        } else if (buttonName === "subCouncelingServices") {
            subCouncelingServicesCheckBox = e.target.checked
        }
        this.setState({ studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox })
    }

    handleSelectChange(options, name) {
        let { selectedGenderOption, selectedStatusOption, errors } = this.state;
        switch (name) {
            case "gender":
                selectedGenderOption = options;
                errors["gender"] = "";
                break;
            case "status":
                selectedStatusOption = options;
                errors["status"] = "";
                break;
        }
        this.setState({ selectedGenderOption, selectedStatusOption, errors: errors });
    }

    handleValidation() {
        let { selectedGenderOption, selectedStatusOption } = this.state;
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        // console.log(fields)
        if (!fields["firstName"]) {
            formIsValid = false;
            errors["firstName"] = "First name cannot be empty";
        }

        if (!fields["lastName"]) {
            formIsValid = false;
            errors["lastName"] = "Last Name cannot be empty";
        }

        if (!fields["country"]) {
            formIsValid = false;
            errors["country"] = "Country cannot be empty";
        }

        if (!fields["zipCode"]) {
            formIsValid = false;
            errors["zipCode"] = "Zip code cannot be empty";
        }

        if (selectedGenderOption === null) {
            formIsValid = false;
            errors["gender"] = "Gender cannot be empty";
        }

        if (selectedStatusOption === null) {
            formIsValid = false;
            errors["status"] = "Status cannot be empty";
        }

        if (!fields["graduationYear"]) {
            formIsValid = false;
            errors["graduationYear"] = "Graduation year cannot be empty";
        }

        if (!fields["currentGPA"]) {
            formIsValid = false;
            errors["currentGPA"] = "Current GPA cannot be empty";
        }

        if (!fields["highSchoolName"]) {
            formIsValid = false;
            errors["highSchoolName"] = "High school name cannot be empty";
        }
        // console.log(errors)
        this.setState({ errors: errors });
        return formIsValid;
    }

    handleNext() {
        // e.preventDefault();
        if (this.handleValidation()) {
            // console.log(fields)
            let fields = this.state.fields;
            console.log(fields)
            let { studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox, selectedGenderOption, selectedStatusOption } = this.state;
            let values = {
                firstName: fields["firstName"],
                lastName: fields["lastName"],
                graduationYear: fields["graduationYear"],
                zipCode: fields["zipCode"],
                // country: fields["country"],
                country: "US",
                highSchoolName: fields["highSchoolName"],
                currentGpa: fields["currentGPA"],
                studentsCollab: studentsCollabCheckBox,
                counselingAllowed: counselingAllowedCheckBox,
                subCouncelingServices: subCouncelingServicesCheckBox,
                gender: selectedGenderOption.label,
                status: selectedStatusOption.label
            }
            console.log(values)
            this.props.setUserProfile(values);
            if (this.props.isProfileSuccess) {
                NotificationManager.success('Profile Updated Sucessfully', 'Sucess!', 5000, () => {
                });
            }
            // e.preventDefault();
        }
    };

    componentDidMount() {
        this.props.getGender();
        this.props.getStatus();
        this.props.getUserProfile();
    }

    componentDidUpdate() {
        // this.props.getUserProfile();
    }

    render() {
        console.log(this.props.userProfile);
        const { gender, status } = this.props;

        let genderOptions = [];
        let statusOptions = [];
        gender !== undefined && gender.length > 0 && gender.map((x, key) => {
            var temp = { label: x, value: key }
            genderOptions.push(temp);
        });
        status !== undefined && status.length > 0 && status.map((x, key) => {
            var temp = { label: x.status, value: x.id }
            statusOptions.push(temp);
        });

        const { ValueContainer, Placeholder } = components;
        const CustomValueContainer = ({ children, ...props }) => {
            return (
                <ValueContainer {...props}>
                    <Placeholder {...props} isFocused={props.isFocused}>
                        {props.selectProps.placeholder}
                    </Placeholder>
                    {React.Children.map(children, child =>
                        child && child.type !== Placeholder ? child : null
                    )}
                </ValueContainer>
            );
        };
        const styles = {
            placeholder: (base, state) => ({
                ...base,
                display:
                    state.isFocused || state.isSelected || state.selectProps.inputValue
                        ? 'none'
                        : 'block',
            }),
        }
        let { roles } = this.state;
        return (
            <>

                <div className="Studentprofilecnt">
                    <div className="toplayer">
                        <div className="topic">
                            <h2>My profile</h2>
                        </div>
                    </div>
                    <div className="toplayermid">
                        <div className="midcnt">
                            <div className="row">
                                <div className="col-6">
                                    <div className="row">
                                        <div className="col-2">
                                            <img src="https://images.unsplash.com/photo-1544723795-3fb6469f5b39?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NjR8fHByb2ZpbGV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60"
                                                alt="profileimage" className="profileimage" />
                                        </div>
                                        <div className="col-4">
                                            <div className="editphotocnt">
                                                <div className="editphotobtn">
                                                    <div className="photobtn">
                                                        <p>
                                                            <span>
                                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fillRule="evenodd" clipRule="evenodd" d="M8.25003 13C8.25003 10.9289 9.92897 9.25 12 9.25C14.0711 9.25 15.75 10.9289 15.75 13C15.75 15.0711 14.0711 16.75 12 16.75C9.92897 16.75 8.25003 15.0711 8.25003 13ZM12 10.75C10.7574 10.75 9.75003 11.7574 9.75003 13C9.75003 14.2426 10.7574 15.25 12 15.25C13.2427 15.25 14.25 14.2426 14.25 13C14.25 11.7574 13.2427 10.75 12 10.75Z" fill="black" />
                                                                    <path fillRule="evenodd" clipRule="evenodd" d="M10.6162 5.75C9.571 5.75 8.72372 6.59728 8.72372 7.64245C8.72372 8.30841 8.2125 8.86283 7.54872 8.91674L5.31912 9.09782C4.88135 9.13337 4.52132 9.45709 4.43961 9.88864C3.99087 12.2586 3.95741 14.6886 4.34072 17.0701L4.43797 17.6743C4.52982 18.2449 4.99945 18.679 5.57551 18.7258L7.51783 18.8835C10.5011 19.1258 13.499 19.1258 16.4822 18.8835L18.4246 18.7258C19.0006 18.679 19.4702 18.2449 19.5621 17.6743L19.6593 17.0701C20.0427 14.6886 20.0092 12.2586 19.5605 9.88864C19.4787 9.45709 19.1187 9.13337 18.6809 9.09782L16.4513 8.91674C15.7876 8.86284 15.2763 8.30841 15.2763 7.64245C15.2763 6.59728 14.4291 5.75 13.3839 5.75H10.6162ZM7.2298 7.4377C7.33565 5.65944 8.81131 4.25 10.6162 4.25H13.3839C15.1888 4.25 16.6644 5.65944 16.7703 7.4377L18.8024 7.60274C19.9133 7.69296 20.8269 8.51446 21.0343 9.60958C21.5153 12.1503 21.5512 14.7554 21.1403 17.3084L21.043 17.9126C20.8414 19.1652 19.8105 20.1182 18.546 20.2209L16.6037 20.3786C13.5396 20.6274 10.4605 20.6274 7.39641 20.3786L5.45409 20.2209C4.18956 20.1182 3.15865 19.1652 2.95703 17.9126L2.85978 17.3084C2.44885 14.7554 2.48472 12.1503 2.9658 9.60958C3.17315 8.51446 4.08678 7.69296 5.1977 7.60274L7.2298 7.4377Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            Edit photo
                                                        </p>
                                                    </div>
                                                    <div className="smalltext"><p>Max size 100 Mb</p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className="editrightcnt">
                                        <div className="toptext">
                                            <p>You have connected with <span className="subhead">Margo Doe </span>
                                                <span>
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M8.46943 8.46753C8.76232 8.17463 9.23719 8.17463 9.53009 8.46753L11.9998 10.9372L14.4694 8.46753C14.7623 8.17464 15.2372 8.17464 15.5301 8.46753C15.823 8.76043 15.823 9.2353 15.5301 9.52819L13.0604 11.9979L15.5301 14.4675C15.823 14.7604 15.823 15.2353 15.5301 15.5282C15.2372 15.8211 14.7623 15.8211 14.4694 15.5282L11.9998 13.0585L9.53009 15.5282C9.2372 15.8211 8.76233 15.8211 8.46943 15.5282C8.17654 15.2353 8.17654 14.7604 8.46943 14.4675L10.9391 11.9979L8.46943 9.52818C8.17653 9.23529 8.17653 8.76042 8.46943 8.46753Z" fill="#5C5C5C" />
                                                        <path fillRule="evenodd" clipRule="evenodd" d="M7.31649 3.76662C10.404 3.42154 13.5954 3.42154 16.683 3.76662C18.5094 3.97075 19.9843 5.40939 20.1991 7.24635C20.5683 10.4033 20.5683 13.5925 20.1991 16.7494C19.9843 18.5863 18.5094 20.025 16.683 20.2291C13.5954 20.5742 10.404 20.5742 7.31649 20.2291C5.49011 20.025 4.01521 18.5863 3.80036 16.7494C3.43113 13.5925 3.43113 10.4033 3.80036 7.24635C4.01521 5.40939 5.49011 3.97075 7.31649 3.76662ZM16.5164 5.25734C13.5396 4.92464 10.4599 4.92464 7.4831 5.25734C6.33867 5.38525 5.42262 6.28849 5.29021 7.4206C4.93451 10.4618 4.93451 13.534 5.29021 16.5751C5.42262 17.7072 6.33867 18.6105 7.4831 18.7384C10.4599 19.0711 13.5396 19.0711 16.5164 18.7384C17.6608 18.6105 18.5769 17.7072 18.7093 16.5751C19.065 13.534 19.065 10.4618 18.7093 7.4206C18.5769 6.28849 17.6608 5.38525 16.5164 5.25734Z" fill="#5C5C5C" />
                                                    </svg>

                                                </span>
                                            </p>
                                        </div>
                                        <div className="rightbtncnt">
                                            <Button className="btn cta--rounded cta-primary">Connect another parent</Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="midlayercnt">
                        <div className="row">
                            <div className="col-6">
                                <div className="miniprofileform">
                                    <p className="profile__form__sub-title">General</p>
                                    <div className="formsection">
                                        <div className="row">
                                            <div className="col-md-5 col-sm-5 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input
                                                        refs="firstName"
                                                        type="text"
                                                        className={"textbox--primary textbox--rounded input"}
                                                        name="firstName"
                                                        placeholder="First Name"
                                                        onChange={this.handleChange.bind(this, "firstName")}
                                                        defaultValue={this.props.userProfile.firstName}
                                                    />
                                                    <label>First Name</label>
                                                    {this.state.errors["firstName"] && <span className="error-text">{this.state.errors["firstName"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input
                                                        refs="lastName"
                                                        type="text"
                                                        className={"textbox--primary textbox--rounded input"}
                                                        name="lastName"
                                                        placeholder="Last Name"
                                                        onChange={this.handleChange.bind(this, "lastName")}
                                                        defaultValue={this.props.userProfile.lastName}
                                                    />
                                                    <label>Last Name</label>
                                                    {this.state.errors["lastName"] && <span className="error-text">{this.state.errors["lastName"]}</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 col-sm-5 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input
                                                        refs="country"
                                                        type="text"
                                                        className={"textbox--primary textbox--rounded input"}
                                                        name="country"
                                                        placeholder="Country"
                                                        onChange={this.handleChange.bind(this, "country")}
                                                        defaultValue={this.props.userProfile.country}
                                                    />
                                                    <label>Country</label>
                                                    {this.state.errors["country"] && <span className="error-text">{this.state.errors["country"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-5 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input
                                                        refs="zipCode"
                                                        type="text"
                                                        className={"textbox--primary textbox--rounded input"}
                                                        name="zipCode"
                                                        placeholder="Zip code"
                                                        onChange={this.handleChange.bind(this, "zipCode")}
                                                        maxLength={9}
                                                        onKeyPress={(event) => { if (!/[0-9-]/.test(event.key)) { event.preventDefault(); } }}
                                                        defaultValue={this.props.userProfile.zipCode}
                                                    />
                                                    <label>Zip code</label>
                                                    {this.state.errors["zipCode"] && <span className="error-text">{this.state.errors["zipCode"]}</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-10 col-sm-10 col-12 p-0">
                                                <div className="input-floating-label">
                                                    <Select
                                                        className="goals__form__select mb-3"
                                                        classNamePrefix="mySelect"
                                                        name="gender"
                                                        defaultValue={{ label: this.props.userProfile.gender }}
                                                        options={genderOptions}
                                                        closeMenuOnSelect={true}
                                                        isClearable={false}
                                                        onChange={(values) => this.handleSelectChange(values, 'gender')}
                                                        components={{
                                                            ValueContainer: CustomValueContainer, IndicatorSeparator: () => null
                                                        }}
                                                        placeholder="Select Gender"
                                                        styles={{
                                                            container: (provided, state) => ({
                                                                ...provided,
                                                                height: '48px',
                                                                overflow: "visible"
                                                            }),
                                                            valueContainer: (provided, state) => ({
                                                                ...provided,
                                                                overflow: "visible",
                                                                height: '100%',
                                                                minHeight: '48px',
                                                            }),
                                                            placeholder: (provided, state) => ({
                                                                ...provided,
                                                                position: "absolute",
                                                                top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                                fontSize: (state.hasValue || state.selectProps.inputValue) && 13,
                                                                background: '#fff',
                                                                paddingLeft: 10,
                                                                paddingRight: 10,
                                                                display: state.isFocused || state.isSelected || state.selectProps.inputValue
                                                                    ? 'none'
                                                                    : 'block',
                                                            })
                                                        }}
                                                    />
                                                </div>
                                                {this.state.errors["gender"] && <span className="error-text">{this.state.errors["gender"]}</span>}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-10 col-sm-10 col-12 p-0">
                                                <div className="input-floating-label">
                                                    <Select
                                                        className="goals__form__select mb-3"
                                                        classNamePrefix="mySelect"
                                                        name="status"
                                                        options={statusOptions}
                                                        closeMenuOnSelect={true}
                                                        isClearable={false}
                                                        onChange={(value) => this.handleSelectChange(value, 'status')}
                                                        components={{
                                                            ValueContainer: CustomValueContainer, IndicatorSeparator: () => null
                                                        }}
                                                        placeholder="Select Status"
                                                        styles={{
                                                            container: (provided, state) => ({
                                                                ...provided,
                                                                height: '48px',
                                                                overflow: "visible"
                                                            }),
                                                            valueContainer: (provided, state) => ({
                                                                ...provided,
                                                                overflow: "visible",
                                                                height: '100%',
                                                                minHeight: '48px',
                                                            }),
                                                            placeholder: (provided, state) => ({
                                                                ...provided,
                                                                position: "absolute",
                                                                top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                                fontSize: (state.hasValue || state.selectProps.inputValue) && 13,
                                                                background: '#fff',
                                                                paddingLeft: 10,
                                                                paddingRight: 10
                                                            })
                                                        }}
                                                    />
                                                    {this.state.errors["status"] && <span className="error-text">{this.state.errors["status"]}</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-10 col-sm-10 col-12 p-0">
                                                <div className="input-floating-label">
                                                    <input
                                                        refs="highSchoolName"
                                                        type="text"
                                                        className={"textbox--primary textbox--rounded input"}
                                                        name="highSchoolName"
                                                        placeholder="High School Name"
                                                        onChange={this.handleChange.bind(this, "highSchoolName")}
                                                    />
                                                    <label>High School Name</label>
                                                    {this.state.errors["highSchoolName"] && <span className="error-text">{this.state.errors["highSchoolName"]}</span>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 col-sm-5 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input
                                                        refs="graduationYear"
                                                        type="text"
                                                        className={"textbox--primary textbox--rounded input"}
                                                        name="graduationYear"
                                                        placeholder="Graduation Year"
                                                        onChange={this.handleChange.bind(this, "graduationYear")}
                                                        maxLength={4}
                                                        onKeyPress={(event) => { if (!/[0-9]/.test(event.key)) { event.preventDefault(); } }}
                                                    />
                                                    <label>Graduation Year</label>
                                                    {this.state.errors["graduationYear"] && <span className="error-text">{this.state.errors["graduationYear"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-5 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input
                                                        refs="currentGPA"
                                                        type="text"
                                                        className={"textbox--primary textbox--rounded input"}
                                                        name="currentGPA"
                                                        placeholder="Current GPA"
                                                        maxLength={5}
                                                        onChange={this.handleChange.bind(this, "currentGPA")}
                                                        onKeyPress={(event) => { if (!/[0-9.]/.test(event.key)) { event.preventDefault(); } }}
                                                    />
                                                    <label>Current GPA</label>
                                                    {this.state.errors["currentGPA"] && <span className="error-text">{this.state.errors["currentGPA"]}</span>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="formsection">
                                    <p className="profile__form__sub-title">Interests</p>
                                    <p className="">
                                        <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value2" onClick={(e) => this.handleCheckboxChange(e, 'studentsCollab')} />
                                        <label htmlFor="styled-checkbox-1">
                                            <span>Collaborating with students with similar goals</span>
                                        </label>
                                    </p>
                                    <p className="">
                                        <input className="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" onClick={(e) => this.handleCheckboxChange(e, 'counselingAllowed')} />
                                        <label htmlFor="styled-checkbox-2">
                                            <span>Advice and counseling from senior students</span>
                                        </label>
                                    </p>
                                    <p className="">
                                        <input className="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value2" onClick={(e) => this.handleCheckboxChange(e, 'subCouncelingServices')} />
                                        <label htmlFor="styled-checkbox-3">
                                            <span>Professional counseling services</span>
                                        </label>
                                    </p>
                                </div>
                                <div className="changepasswordsection">
                                    <p className="profile__form__sub-title">Password</p>
                                    <Form className="login_card_form mb-5" autoComplete="off" onSubmit={this.passwordhandleSubmit.bind(this)} >
                                        <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                            <div className="input-floating-label">
                                                <input
                                                    refs="password"
                                                    type="password"
                                                    className={this.state.errors["password"] ? "textbox--primary textbox--rounded input w-100 error-input" : "textbox--primary textbox--rounded input"}
                                                    name="password"
                                                    placeholder="Current password"
                                                    onChange={this.PasswordhandleChange.bind(this, "password")}
                                                    value={this.state.passwordfields["password"]}
                                                />
                                                <label>Current password</label>
                                                {this.state.errors["password"] && <span className="error-text">{this.state.errors["password"]}</span>}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                            <div className="input-floating-label">
                                                <input
                                                    refs="newPassword"
                                                    type="password"
                                                    className={this.state.errors["newPassword"] ? "textbox--primary textbox--rounded input w-100 error-input" : "textbox--primary textbox--rounded input"}
                                                    name="newPassword"
                                                    placeholder="Create new password"
                                                    onChange={this.PasswordhandleChange.bind(this, "newPassword")}
                                                    value={this.state.passwordfields["newPassword"]}
                                                />
                                                <label>Create new password</label>
                                                <p className="cta--text mb-1">Must contain 1 symbols and at least 1 number</p>
                                                {this.state.errors["password"] && <span className="error-text">{this.state.errors["password"]}</span>}
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                            <div className="input-floating-label">
                                                <input
                                                    refs="confirmPassword"
                                                    type="password"
                                                    className={this.state.errors["confirmPassword"] ? "textbox--primary textbox--rounded input error-input" : "textbox--primary textbox--rounded input"}
                                                    name="confirmPassword"
                                                    placeholder="Confirm password"
                                                    onChange={this.PasswordhandleChange.bind(this, "confirmPassword")}
                                                    value={this.state.passwordfields["confirmPassword"]}
                                                />
                                                <label>Confirm password</label>
                                                {this.state.errors["confirmPassword"] && <span className="error-text">{this.state.errors["confirmPassword"]}</span>}
                                            </div>
                                        </div>
                                        <div style={{ display: "flex", justifyContent: "center" }}>
                                            <Button type="submit" className="btn cta--rounded cta-primary" >
                                                <span>Reset password</span>
                                            </Button>
                                        </div>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="endlayercnt">
                        <div className="billingcnt">
                            <p className="profile__form__sub-title">Billing</p>
                        </div>
                        <div className="billcardsection">
                            <div className="billcard">
                                <div className="billbox">
                                    <div className="cnt">
                                        <div style={{ display: "flex", justifyContent: "center" }}>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.9535 0H13.0465C13.2326 0 13.3256 0.0888889 13.3256 0.266667V23.7333C13.3256 23.9111 13.2326 24 13.0465 24H10.9535C10.7674 24 10.6744 23.9111 10.6744 23.7333V0.266667C10.6744 0.0888889 10.7674 0 10.9535 0Z" fill="#12323E" />
                                                <path d="M0.27907 10.7333H23.7209C23.907 10.7333 24 10.8222 24 11V13C24 13.1778 23.907 13.2667 23.7209 13.2667H0.27907C0.0930233 13.2667 0 13.1778 0 13V11C0 10.8222 0.0930233 10.7333 0.27907 10.7333Z" fill="#12323E" />
                                            </svg>
                                        </div>
                                        <div style={{ display: "flex", justifyContent: "center" }}>
                                            <p>Add new card</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footlayer">
                        <div className="row">
                            <div className="col-6">
                                <div className="foottoplayer">
                                    <p><span><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M13.5443 10.4584C12.7252 9.63964 11.6144 9.17969 10.4563 9.17969C9.29815 9.17969 8.18741 9.63964 7.3683 10.4584L4.2793 13.5464C3.46018 14.3655 3 15.4765 3 16.6349C3 17.7933 3.46018 18.9043 4.2793 19.7234C5.09842 20.5425 6.20939 21.0027 7.3678 21.0027C8.52621 21.0027 9.63718 20.5425 10.4563 19.7234L12.0003 18.1794" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                        <path d="M10.457 13.5443C11.2761 14.3631 12.3869 14.823 13.545 14.823C14.7032 14.823 15.8139 14.3631 16.633 13.5443L19.722 10.4563C20.5412 9.63718 21.0013 8.52621 21.0013 7.3678C21.0013 6.20939 20.5412 5.09842 19.722 4.2793C18.9029 3.46018 17.7919 3 16.6335 3C15.4751 3 14.3642 3.46018 13.545 4.2793L12.001 5.8233" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                    </span>Copy invite link for friend<span><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7.99935 7.16797C8.27549 7.16797 8.49935 7.39183 8.49935 7.66797V11.0013C8.49935 11.2774 8.27549 11.5013 7.99935 11.5013C7.72321 11.5013 7.49935 11.2774 7.49935 11.0013V7.66797C7.49935 7.39183 7.72321 7.16797 7.99935 7.16797Z" fill="#919293" />
                                        <path d="M7.99935 6.0013C8.36754 6.0013 8.66602 5.70283 8.66602 5.33464C8.66602 4.96645 8.36754 4.66797 7.99935 4.66797C7.63116 4.66797 7.33268 4.96645 7.33268 5.33464C7.33268 5.70283 7.63116 6.0013 7.99935 6.0013Z" fill="#919293" />
                                        <path fillRule="evenodd" clipRule="evenodd" d="M2.16602 8.0013C2.16602 4.77964 4.77769 2.16797 7.99935 2.16797C11.221 2.16797 13.8327 4.77964 13.8327 8.0013C13.8327 11.223 11.221 13.8346 7.99935 13.8346C4.77769 13.8346 2.16602 11.223 2.16602 8.0013ZM7.99935 3.16797C5.32997 3.16797 3.16602 5.33193 3.16602 8.0013C3.16602 10.6707 5.32997 12.8346 7.99935 12.8346C10.6687 12.8346 12.8327 10.6707 12.8327 8.0013C12.8327 5.33193 10.6687 3.16797 7.99935 3.16797Z" fill="#919293" />
                                    </svg>
                                        </span></p>
                                </div>
                            </div>
                            <div className="col-6">
                                <div style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end" }}>
                                    <div className="deleteprfcnt">
                                        <div className="deleprfbtn" >
                                            <p><span><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10 2.25C9.58579 2.25 9.25 2.58579 9.25 3V3.75H5C4.58579 3.75 4.25 4.08579 4.25 4.5C4.25 4.91421 4.58579 5.25 5 5.25H19C19.4142 5.25 19.75 4.91421 19.75 4.5C19.75 4.08579 19.4142 3.75 19 3.75H14.75V3C14.75 2.58579 14.4142 2.25 14 2.25H10Z" fill="black" />
                                                <path d="M13.0607 15L14.5303 16.4697C14.8232 16.7626 14.8232 17.2374 14.5303 17.5303C14.2374 17.8232 13.7626 17.8232 13.4697 17.5303L12 16.0607L10.5303 17.5303C10.2374 17.8232 9.76257 17.8232 9.46968 17.5303C9.17678 17.2374 9.17678 16.7626 9.46968 16.4697L10.9393 15L9.46967 13.5303C9.17678 13.2374 9.17678 12.7626 9.46967 12.4697C9.76256 12.1768 10.2374 12.1768 10.5303 12.4697L12 13.9393L13.4697 12.4697C13.7626 12.1768 14.2374 12.1768 14.5303 12.4697C14.8232 12.7626 14.8232 13.2374 14.5303 13.5303L13.0607 15Z" fill="black" />
                                                <path fillRule="evenodd" clipRule="evenodd" d="M5.99142 7.91718C6.03363 7.53735 6.35468 7.25 6.73684 7.25H17.2632C17.6453 7.25 17.9664 7.53735 18.0086 7.91718L18.2087 9.71852C18.5715 12.9838 18.5715 16.2793 18.2087 19.5446L18.189 19.722C18.045 21.0181 17.0404 22.0517 15.7489 22.2325C13.2618 22.5807 10.7382 22.5807 8.25108 22.2325C6.95954 22.0517 5.955 21.0181 5.81098 19.722L5.79128 19.5446C5.42846 16.2793 5.42846 12.9838 5.79128 9.71852L5.99142 7.91718ZM7.40812 8.75L7.2821 9.88417C6.93152 13.0394 6.93152 16.2238 7.2821 19.379L7.3018 19.5563C7.37011 20.171 7.84652 20.6612 8.45905 20.747C10.8082 21.0758 13.1918 21.0758 15.5409 20.747C16.1535 20.6612 16.6299 20.171 16.6982 19.5563L16.7179 19.379C17.0685 16.2238 17.0685 13.0394 16.7179 9.88417L16.5919 8.75H7.40812Z" fill="black" />
                                            </svg>
                                            </span>Delete profile</p>
                                        </div>
                                    </div>
                                    <div className="profilesavecnt">
                                        <Button onClick={e => this.handleNext(e)} className="btn cta--rounded cta-primary profilesavebtn">Save</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <NotificationContainer /> */}
                </div>
            </>
        );
    }
}

const mapStateToProps = ({ auth, commonData }) => {
    const { message, errorList, status, gender } = commonData
    const { isPasswordUpdate } = auth;
    const { isProfileSuccess } = auth;
    const { userProfile } = auth;
    return { isPasswordUpdate, message, errorList, status, gender, isProfileSuccess, userProfile }
};

export default connect(mapStateToProps, { changePassword, getGender, getStatus, setUserProfile, getUserProfile })(Index);