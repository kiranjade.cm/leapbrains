import React, { Component } from "react";
import FullCalendar from '@fullcalendar/react' // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid' // a plugin!
import interactionPlugin from "@fullcalendar/interaction" // needed for dayClick
import timeGridPlugin from "@fullcalendar/timegrid";
import '../../../assets/css/schedule.less';
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Search from '../../../components/Search';
import Select, { components } from "react-select";
var calendar = new FullCalendar();

class Index extends Component {
    constructor() {
        super();
        this.state = {
            isActive: false,
            show: false,
            title: '',
            advisor: '',
            date: '',
            description: ''
        };
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleSelect = this.handleSelect.bind(this)
    }

    handleSubmit(event) {

        event.preventDefault();
        console.log(this.state.title)
        console.log(this.state.advisor)
        console.log(this.state.description)
    }
    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
        console.log(this.state.title)

    }

    handleDateClick = () => {
        this.setState({ ...this.state, show: true })


    }
    handleClose = () => {
        this.setState({ ...this.state, show: false })
    }

    handleSelect(valObj, action) {
        if (action === "clear") {
            return;
        } else {
            calendar.current.getApi().changeView(valObj.value);
        }
    }
    handleClickMobilesearch(e) {
        e.preventDefault();
        const currentState = this.state.isActive;
        this.setState({ isActive: !currentState });
    }

    render() {
        let { isActive } = this.state;
        const events = [

            {
                title: 'Accelerated English',
                start: '2022-07-06T12:30:00',
            },
            {
                title: 'Accelerated Algebra Chapter 6',
                start: '2022-07-08T04:05:00'
            },
        ];

        const CalendarViews = [
            { value: 'dayGridMonth', label: 'Month' },
            { value: 'timeGridWeek', label: 'Week' },
            { value: 'timeGridDay', label: 'Day' }
        ];

        const advisor = [
            { value: 'advisor1', label: 'advisor1' },
            { value: 'advisor2', label: 'advisor2' },
            { value: 'advisor3', label: 'advisor3' },
            { value: 'advisor4', label: 'advisor4' },
        ]

        const { ValueContainer, Placeholder } = components;
        const CustomValueContainer = ({ children, ...props }) => {
            return (
                <ValueContainer {...props}>
                    <Placeholder {...props} isFocused={props.isFocused}>
                        {props.selectProps.placeholder}
                    </Placeholder>
                    {React.Children.map(children, child =>
                        child && child.type !== Placeholder ? child : null
                    )}
                </ValueContainer>
            );
        };
        const styles = {
            placeholder: (base, state) => ({
                ...base,
                display:
                    state.isFocused || state.isSelected || state.selectProps.inputValue
                        ? 'none'
                        : 'block',
            }),
        }

        return (
            <>
                <div className="schedule-mobile-top">
                    <div className="dashboardtop">
                        <div className="row">
                            <div className="col-md-8 col-sm-11 col-10">
                                <div className="dashboard-top-text">
                                    <h2>Schedule</h2>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-1 col-1 Search-content">
                                <Search placeholder="Search" />
                                <div className="mobile-search">
                                    <div class="mobile-search-content">
                                        <a href="javascript:void(0)" onClick={(e) => this.handleClickMobilesearch(e)} class="mobile-search-button">
                                            <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M14.5046 3.22982C11.3749 0.183809 6.28112 0.183809 3.15144 3.22982C0.0224364 6.2765 0.0224364 11.2335 3.15144 14.2801C5.93851 16.992 10.2792 17.2826 13.4069 15.1649C13.4727 15.468 13.6233 15.7572 13.8655 15.993L18.4233 20.4289C19.0875 21.0741 20.1608 21.0741 20.8216 20.4289C21.4852 19.7832 21.4852 18.7385 20.8216 18.0947L16.2638 13.6575C16.023 13.4237 15.7251 13.2765 15.4137 13.2124C17.5909 10.1677 17.2923 5.9437 14.5046 3.22982ZM13.0656 12.8796C10.729 15.1537 6.92633 15.1537 4.59043 12.8796C2.25521 10.6055 2.25521 6.90511 4.59043 4.631C6.92633 2.35755 10.729 2.35755 13.0656 4.631C15.4022 6.90511 15.4022 10.6055 13.0656 12.8796Z" fill="#1C84EE" />
                                            </svg>
                                        </a>
                                        {isActive && <input type="text" class="mobile-search-input" placeholder="Search here..." />}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="schedule">
                    <div className="calendar-outer">
                        <div className="calendar-box">
                            <div className="schedule-head-box">
                                <div className="right">
                                    <div className="cal-select-month">
                                        <Select
                                            closeMenuOnSelect={true}
                                            options={CalendarViews}
                                            placeholder={"Month"}
                                            onChange={(value, { action }, name) => this.handleSelect(value, action)}
                                        />
                                    </div>
                                </div>
                                <div className="left"><div className="top-cal">
                                    <Search />
                                    <svg width="39" height="39" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M17 13C17 12.4477 17.4477 12 18 12H19C19.5523 12 20 12.4477 20 13V14H27C27.5523 14 28 14.4477 28 15C28 15.5523 27.5523 16 27 16H20V17C20 17.5523 19.5523 18 19 18H18C17.4477 18 17 17.5523 17 17V16H13C12.4477 16 12 15.5523 12 15C12 14.4477 12.4477 14 13 14H17V13ZM13 24C12.4477 24 12 24.4477 12 25C12 25.5523 12.4477 26 13 26H22V27C22 27.5523 22.4477 28 23 28H24C24.5523 28 25 27.5523 25 27V26H27C27.5523 26 28 25.5523 28 25C28 24.4477 27.5523 24 27 24H25V23C25 22.4477 24.5523 22 24 22H23C22.4477 22 22 22.4477 22 23V24H13Z" fill="#1B1C1E" /></svg>
                                    <div className="cal-add-btn"><button className="btn cta--rounded cta-primary" onClick={this.handleDateClick}>+</button></div>
                                </div></div>
                            </div>
                            <div className="cal-para"><p>Here all your planned events. You will find information for each event as well as you can plan new one.</p></div>
                            <div className="small-cal">
                                <div className="left">
                                    <div className="cal-select-month">
                                        <Select
                                            closeMenuOnSelect={true}
                                            options={CalendarViews}
                                            placeholder={"Month"}
                                            onChange={(value, { action }, name) => this.handleSelect(value, action)}
                                        />
                                    </div>
                                </div>
                                <div className="right">
                                    <svg width="39" height="39" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M17 13C17 12.4477 17.4477 12 18 12H19C19.5523 12 20 12.4477 20 13V14H27C27.5523 14 28 14.4477 28 15C28 15.5523 27.5523 16 27 16H20V17C20 17.5523 19.5523 18 19 18H18C17.4477 18 17 17.5523 17 17V16H13C12.4477 16 12 15.5523 12 15C12 14.4477 12.4477 14 13 14H17V13ZM13 24C12.4477 24 12 24.4477 12 25C12 25.5523 12.4477 26 13 26H22V27C22 27.5523 22.4477 28 23 28H24C24.5523 28 25 27.5523 25 27V26H27C27.5523 26 28 25.5523 28 25C28 24.4477 27.5523 24 27 24H25V23C25 22.4477 24.5523 22 24 22H23C22.4477 22 22 22.4477 22 23V24H13Z" fill="#1B1C1E" /></svg>
                                    <div className="cal-add-btn"><button className="btn cta--rounded cta-primary" onClick={this.handleDateClick}>+</button></div>
                                </div>
                            </div>
                            <FullCalendar
                                ref={calendar}
                                customButtons={{
                                    myCustomButton: { icon: 'calendar', }

                                }}
                                plugins={[dayGridPlugin, interactionPlugin, timeGridPlugin]}
                                events={events}
                                datesSet={this.handleDatesSet}
                                themeSystem='bootstrapFontAwesome'
                                height='930px'
                                editable='true'
                                selectable='true'
                                titleFormat={{ year: 'numeric', month: 'short' }}
                                headerToolbar={{ left: "title,", center: "prev,myCustomButton,next", right: "" }}

                            />

                            <div className="schedule-model">
                                <Modal show={this.state.show} onHide={this.handleClose} className="schedule-model">
                                    <Modal.Header closeButton>
                                        <Modal.Title>Book session?</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body  >
                                        <div className="schedule-form">
                                            <div className="row m-0">
                                                <div className="col-12">
                                                    <div class="input-floating-label">
                                                        <input type="text" className="textbox--primary textbox--rounded input" name="title" placeholder="Title" />
                                                        <label >Title</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row mb-2 pl-1 ">
                                                <div className="col">
                                                    <label>Plan:</label><select name="plan" id="form-sc-model-select" className="form-select-space">
                                                        <option value=''>Going to Cambridge</option>
                                                        <option value=''>Going to Cambridge </option></select>
                                                </div>
                                            </div>
                                            <div className="row mb-2 pl-1">
                                                <div className="col">
                                                    <label>Course:</label><select name="course" id="form-sc-model-select" className="form-select-space">
                                                        <option value=''>Algebra</option>
                                                        <option value=''>Algebra</option></select>
                                                </div>
                                            </div>
                                            <div className="row mb-4 pl-1">
                                                <div className="col">
                                                    <label>Milestone:</label><select name="milestone" id="form-sc-model-select" className="form-select-space">
                                                        <option value=''>Milestone</option>
                                                        <option value=''>Milestone</option></select>
                                                </div>
                                            </div>
                                            <div className="row m-0">
                                                <div className="col-1 icon-p">
                                                    <label> <svg width="18" height="20" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M12.8972 13.0563C13.4644 12.7516 14.1136 12.5781 14.805 12.5781H14.8074C14.8777 12.5781 14.9105 12.4938 14.8589 12.4469C14.1399 11.8016 13.3185 11.2804 12.4285 10.9047C12.4191 10.9 12.4097 10.8977 12.4004 10.893C13.8558 9.83594 14.8027 8.11797 14.8027 6.17969C14.8027 2.96875 12.2058 0.367188 9.00191 0.367188C5.79801 0.367188 3.20348 2.96875 3.20348 6.17969C3.20348 8.11797 4.15035 9.83594 5.60816 10.893C5.59879 10.8977 5.58941 10.9 5.58004 10.9047C4.53238 11.3477 3.59254 11.9828 2.78395 12.7938C1.98002 13.5962 1.33999 14.5475 0.89957 15.5945C0.466242 16.6198 0.232379 17.7184 0.210508 18.8312C0.209882 18.8563 0.21427 18.8812 0.223412 18.9044C0.232554 18.9277 0.246266 18.949 0.26374 18.9669C0.281214 18.9848 0.302096 18.999 0.325155 19.0087C0.348215 19.0184 0.372986 19.0234 0.398008 19.0234H1.80191C1.9027 19.0234 1.98707 18.9414 1.98941 18.8406C2.03629 17.0312 2.76051 15.3367 4.04254 14.0523C5.36676 12.7234 7.12926 11.9922 9.00426 11.9922C10.3332 11.9922 11.6082 12.3602 12.7074 13.0492C12.7356 13.067 12.768 13.077 12.8014 13.0782C12.8347 13.0794 12.8678 13.0719 12.8972 13.0563ZM9.00426 10.2109C7.93082 10.2109 6.92066 9.79141 6.15895 9.02969C5.78417 8.65588 5.48706 8.21161 5.28473 7.72248C5.0824 7.23335 4.97884 6.70901 4.98004 6.17969C4.98004 5.10391 5.39957 4.09141 6.15895 3.32969C6.91832 2.56797 7.92848 2.14844 9.00426 2.14844C10.08 2.14844 11.0879 2.56797 11.8496 3.32969C12.2243 3.70349 12.5215 4.14776 12.7238 4.63689C12.9261 5.12603 13.0297 5.65036 13.0285 6.17969C13.0285 7.25547 12.6089 8.26797 11.8496 9.02969C11.0879 9.79141 10.0777 10.2109 9.00426 10.2109ZM17.6246 15.7891H15.6558V13.8203C15.6558 13.7172 15.5714 13.6328 15.4683 13.6328H14.1558C14.0527 13.6328 13.9683 13.7172 13.9683 13.8203V15.7891H11.9996C11.8964 15.7891 11.8121 15.8734 11.8121 15.9766V17.2891C11.8121 17.3922 11.8964 17.4766 11.9996 17.4766H13.9683V19.4453C13.9683 19.5484 14.0527 19.6328 14.1558 19.6328H15.4683C15.5714 19.6328 15.6558 19.5484 15.6558 19.4453V17.4766H17.6246C17.7277 17.4766 17.8121 17.3922 17.8121 17.2891V15.9766C17.8121 15.8734 17.7277 15.7891 17.6246 15.7891Z" fill="#919293" />
                                                    </svg></label>
                                                </div>
                                                <div className="col-11 mp-2">
                                                    <div class="input-floating-label">
                                                        <Select
                                                            className="goals__form__select mb-3"
                                                            classNamePrefix="mySelect"
                                                            options={advisor}
                                                            closeMenuOnSelect={true}
                                                            isClearable={false}
                                                            components={{
                                                                ValueContainer: CustomValueContainer, IndicatorSeparator: () => null
                                                            }}
                                                            placeholder="Select advisor"
                                                            styles={{
                                                                container: (provided, state) => ({
                                                                    ...provided,
                                                                    height: '48px',
                                                                    overflow: "visible"
                                                                }),
                                                                valueContainer: (provided, state) => ({
                                                                    ...provided,
                                                                    overflow: "visible",
                                                                    height: '100%',
                                                                    minHeight: '48px',
                                                                }),
                                                                placeholder: (provided, state) => ({
                                                                    ...provided,
                                                                    position: "absolute",
                                                                    top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13,
                                                                    background: '#fff',
                                                                    paddingLeft: 10,
                                                                    paddingRight: 10,
                                                                    display: state.isFocused || state.isSelected || state.selectProps.inputValue
                                                                        ? 'none'
                                                                        : 'block',
                                                                })
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row m-0">
                                                <div className="col-sm-6 col-12">
                                                    <div className="row m-0">
                                                        <div className="col-sm-2 col-1 date-p">
                                                            <label> <svg width="22" height="22" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="1" y="3.55859" width="13.7846" height="12.8" rx="1.5" stroke="#919293" stroke-width="2" stroke-linecap="round" strokeLinejoin="round" /><path d="M14.7839 7.82599H0.999283" stroke="#919293" stroke-width="2" stroke-linecap="round" strokeLinejoin="round" /><path d="M4.44626 1V2.70667" stroke="#919293" stroke-width="2" stroke-linecap="round" strokeLinejoin="round" /><path d="M11.3383 1V2.70667" stroke="#919293" stroke-width="2" stroke-linecap="round" strokeLinejoin="round" /></svg></label>
                                                        </div>
                                                        <div className="col-10 tpl-0 mp-1">
                                                            <div class="input-floating-label">
                                                                <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Date" placeholder="Select date" onChange={this.handleChange} onFocus={(e) => (e.target.type = "date")} onBlur={(e) => (e.target.type = "text")} />
                                                                <label >Select date</label>
                                                            </div></div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-12">
                                                    <div className="row m-0">
                                                        <div className="col-sm-2 col-1 time-p">
                                                            <label><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M12 1.5C6.20156 1.5 1.5 6.20156 1.5 12C1.5 17.7984 6.20156 22.5 12 22.5C17.7984 22.5 22.5 17.7984 22.5 12C22.5 6.20156 17.7984 1.5 12 1.5ZM12 20.7188C7.18594 20.7188 3.28125 16.8141 3.28125 12C3.28125 7.18594 7.18594 3.28125 12 3.28125C16.8141 3.28125 20.7188 7.18594 20.7188 12C20.7188 16.8141 16.8141 20.7188 12 20.7188Z" fill="#919293" />
                                                                <path d="M16.0945 14.9672L12.7523 12.5508V6.75C12.7523 6.64687 12.668 6.5625 12.5648 6.5625H11.4375C11.3344 6.5625 11.25 6.64687 11.25 6.75V13.2047C11.25 13.2656 11.2781 13.3219 11.3273 13.357L15.2039 16.1836C15.2883 16.2445 15.4055 16.2258 15.4664 16.1437L16.1367 15.2297C16.1976 15.1429 16.1789 15.0258 16.0945 14.9672Z" fill="#919293" />
                                                            </svg></label>
                                                        </div>
                                                        <div className="col-10 tpr-0 mp-1">
                                                            <div class="input-floating-label">
                                                                <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder="Select time" onChange={this.handleChange} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (e.target.type = "text")} />
                                                                <label >Select time</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-1 zoom-icon">
                                                    <label><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M4.585 13.6063L4.315 13.5943H1.886L5.122 10.3573L5.109 10.0873C5.10417 9.87775 5.01875 9.67808 4.87051 9.52984C4.72226 9.38159 4.52259 9.29618 4.313 9.29135L4.043 9.27835H0L0.014 9.54835C0.048 9.98635 0.367 10.3183 0.808 10.3443L1.078 10.3573H3.508L0.268 13.5943L0.282 13.8633C0.297 14.2963 0.644 14.6433 1.077 14.6593L1.347 14.6723H5.393L5.379 14.4023C5.343 13.9593 5.029 13.6353 4.584 13.6073L4.585 13.6063ZM7.823 9.27835H7.819C7.10411 9.27874 6.41864 9.56317 5.91334 10.0691C5.40805 10.575 5.1243 11.2609 5.1245 11.9761C5.1247 12.6913 5.40883 13.3771 5.91441 13.8827C6.41998 14.3883 7.10561 14.6723 7.8205 14.6723C8.53539 14.6723 9.22102 14.3883 9.72659 13.8827C10.2322 13.3771 10.5163 12.6913 10.5165 11.9761C10.5167 11.2609 10.233 10.575 9.72766 10.0691C9.22236 9.56317 8.53689 9.27874 7.822 9.27835H7.823ZM8.964 13.1193C8.81555 13.2774 8.63685 13.4041 8.43851 13.4918C8.24018 13.5794 8.02625 13.6264 7.80942 13.6298C7.59259 13.6332 7.37728 13.5931 7.17627 13.5117C6.97527 13.4303 6.79266 13.3094 6.63928 13.1561C6.48591 13.0028 6.3649 12.8202 6.28343 12.6192C6.20196 12.4183 6.1617 12.203 6.16503 11.9862C6.16836 11.7693 6.21522 11.5554 6.30282 11.357C6.39043 11.1586 6.51699 10.9799 6.675 10.8313C6.98151 10.5432 7.38811 10.3858 7.80871 10.3922C8.22931 10.3987 8.63088 10.5686 8.92839 10.866C9.2259 11.1633 9.39599 11.5648 9.40264 11.9854C9.40928 12.406 9.25197 12.8127 8.964 13.1193ZM21.84 9.27935C21.5345 9.27976 21.2325 9.34506 20.9541 9.4709C20.6757 9.59675 20.4272 9.78027 20.225 10.0093C20.0228 9.77915 19.7737 9.59478 19.4945 9.46855C19.2153 9.34231 18.9124 9.27713 18.606 9.27735C18.1753 9.27679 17.7545 9.4057 17.398 9.64735C17.188 9.41435 16.718 9.27735 16.449 9.27735V14.6723L16.719 14.6593C17.169 14.6293 17.497 14.3103 17.515 13.8633L17.528 13.5933V11.7043L17.542 11.4343C17.552 11.2323 17.582 11.0523 17.674 10.8943C17.8173 10.6467 18.053 10.4662 18.3292 10.3925C18.6054 10.3188 18.8996 10.358 19.147 10.5013C19.3099 10.5955 19.4454 10.7306 19.54 10.8933C19.633 11.0533 19.66 11.2333 19.672 11.4333L19.686 11.7043V13.5933L19.699 13.8623C19.7074 14.0706 19.7938 14.2681 19.9411 14.4156C20.0884 14.5631 20.2857 14.6497 20.494 14.6583L20.764 14.6713V11.7043L20.776 11.4343C20.786 11.2343 20.816 11.0503 20.91 10.8913C21.21 10.3773 21.87 10.2013 22.383 10.5013C22.5461 10.5957 22.6816 10.7312 22.776 10.8943C22.868 11.0543 22.896 11.2373 22.906 11.4343L22.921 11.7043V13.5933L22.934 13.8623C22.962 14.3053 23.284 14.6323 23.73 14.6583L24 14.6713V11.4343C23.9997 11.151 23.9437 10.8704 23.835 10.6087C23.7263 10.3469 23.5671 10.1092 23.3665 9.90899C23.1659 9.70879 22.9279 9.55005 22.666 9.44185C22.4041 9.33364 22.1234 9.27808 21.84 9.27835V9.27935ZM11.577 10.0673C11.3155 10.3152 11.1063 10.6129 10.9617 10.9429C10.8172 11.2729 10.7401 11.6286 10.7352 11.9889C10.7302 12.3492 10.7975 12.7068 10.9329 13.0407C11.0684 13.3745 11.2694 13.6779 11.524 13.9328C11.7785 14.1877 12.0816 14.389 12.4152 14.5248C12.7488 14.6607 13.1062 14.7283 13.4664 14.7237C13.8266 14.7191 14.1822 14.6423 14.5122 14.498C14.8422 14.3537 15.14 14.1447 15.388 13.8833C15.8719 13.3734 16.1377 12.6945 16.1287 11.9914C16.1197 11.2883 15.8367 10.6165 15.3399 10.119C14.8431 9.62158 14.1718 9.33781 13.4689 9.32816C12.7661 9.3185 12.0873 9.58373 11.577 10.0673ZM14.627 13.1193C14.4772 13.2721 14.2986 13.3936 14.1015 13.4769C13.9044 13.5602 13.6928 13.6036 13.4788 13.6046C13.2649 13.6056 13.0528 13.5642 12.855 13.4828C12.6571 13.4014 12.4774 13.2815 12.3261 13.1302C12.1749 12.9789 12.0551 12.7991 11.9738 12.6012C11.8924 12.4033 11.8511 12.1913 11.8522 11.9773C11.8533 11.7633 11.8968 11.5517 11.9802 11.3547C12.0636 11.1577 12.1852 10.9791 12.338 10.8293C12.6427 10.5307 13.0529 10.3645 13.4795 10.3667C13.9061 10.3689 14.3146 10.5394 14.6162 10.8411C14.9178 11.1428 15.0882 11.5514 15.0902 11.978C15.0922 12.4046 14.9257 12.8148 14.627 13.1193Z" fill="#303132" />
                                                    </svg></label>
                                                </div>
                                                <div className="col-10 mb-10 pl-1 mp-2">
                                                    <Button variant="primary" >Going with Zoom  </Button>
                                                </div>
                                            </div>
                                            <div className="row m-0">
                                                <div className="col-1 icon-p">
                                                    <label><svg width="20" height="14" viewBox="0 0 20 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.2999 0.400391C1.06121 0.400391 0.832289 0.495212 0.663506 0.663994C0.494723 0.832777 0.399902 1.0617 0.399902 1.30039C0.399902 1.53909 0.494723 1.768 0.663506 1.93679C0.832289 2.10557 1.06121 2.20039 1.2999 2.20039H18.6999C18.9386 2.20039 19.1675 2.10557 19.3363 1.93679C19.5051 1.768 19.5999 1.53909 19.5999 1.30039C19.5999 1.0617 19.5051 0.832777 19.3363 0.663994C19.1675 0.495212 18.9386 0.400391 18.6999 0.400391H1.2999Z" fill="#919293" /><path d="M1.2999 4C1.06121 4 0.832289 4.09482 0.663506 4.2636C0.494723 4.43239 0.399902 4.66131 0.399902 4.9C0.399902 5.13869 0.494723 5.36761 0.663506 5.5364C0.832289 5.70518 1.06121 5.8 1.2999 5.8H18.6999C18.9386 5.8 19.1675 5.70518 19.3363 5.5364C19.5051 5.36761 19.5999 5.13869 19.5999 4.9C19.5999 4.66131 19.5051 4.43239 19.3363 4.2636C19.1675 4.09482 18.9386 4 18.6999 4H1.2999Z" fill="#919293" /><path d="M0.399902 8.49961C0.399902 8.26091 0.494723 8.032 0.663506 7.86321C0.832289 7.69443 1.06121 7.59961 1.2999 7.59961H18.6999C18.9386 7.59961 19.1675 7.69443 19.3363 7.86321C19.5051 8.032 19.5999 8.26091 19.5999 8.49961C19.5999 8.7383 19.5051 8.96722 19.3363 9.13601C19.1675 9.30479 18.9386 9.39961 18.6999 9.39961H1.2999C1.06121 9.39961 0.832289 9.30479 0.663506 9.13601C0.494723 8.96722 0.399902 8.7383 0.399902 8.49961Z" fill="#919293" /><path d="M1.2999 11.2012C1.06121 11.2012 0.832289 11.296 0.663506 11.4648C0.494723 11.6336 0.399902 11.8625 0.399902 12.1012C0.399902 12.3399 0.494723 12.5688 0.663506 12.7376C0.832289 12.9064 1.06121 13.0012 1.2999 13.0012H12.6999C12.9386 13.0012 13.1675 12.9064 13.3363 12.7376C13.5051 12.5688 13.5999 12.3399 13.5999 12.1012C13.5999 11.8625 13.5051 11.6336 13.3363 11.4648C13.1675 11.296 12.9386 11.2012 12.6999 11.2012H1.2999Z" fill="#919293" /></svg></label>
                                                </div>
                                                <div className="col-11 mp-2">
                                                    <div class="input-floating-label">
                                                        <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Description" placeholder="Add Description" />
                                                        <label >Add Description</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row m-0">
                                                <div className="col-1 icon-p">
                                                    <label> <svg width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14.2639 2.60898C12.0561 0.401172 8.46076 0.401172 6.25529 2.60898L0.138108 8.72148C0.098264 8.76133 0.0771701 8.81523 0.0771701 8.87148C0.0771701 8.92773 0.098264 8.98164 0.138108 9.02148L1.00295 9.88633C1.04248 9.92569 1.096 9.94778 1.15178 9.94778C1.20756 9.94778 1.26108 9.92569 1.30061 9.88633L7.4178 3.77383C8.17717 3.01445 9.18733 2.59727 10.2608 2.59727C11.3342 2.59727 12.3444 3.01445 13.1014 3.77383C13.8608 4.5332 14.278 5.54336 14.278 6.61445C14.278 7.68789 13.8608 8.6957 13.1014 9.45508L6.86701 15.6871L5.85686 16.6973C4.91233 17.6418 3.37717 17.6418 2.43264 16.6973C1.97561 16.2402 1.72483 15.6332 1.72483 14.9863C1.72483 14.3395 1.97561 13.7324 2.43264 13.2754L8.6178 7.09258C8.77483 6.93789 8.98108 6.85117 9.20139 6.85117H9.20373C9.42405 6.85117 9.62795 6.93789 9.78264 7.09258C9.93967 7.24961 10.024 7.45586 10.024 7.67617C10.024 7.89414 9.93733 8.10039 9.78264 8.25508L4.72717 13.3059C4.68733 13.3457 4.66623 13.3996 4.66623 13.4559C4.66623 13.5121 4.68733 13.566 4.72717 13.6059L5.59201 14.4707C5.63155 14.5101 5.68506 14.5322 5.74084 14.5322C5.79663 14.5322 5.85014 14.5101 5.88967 14.4707L10.9428 9.41758C11.4092 8.95117 11.6647 8.33242 11.6647 7.67383C11.6647 7.01523 11.4069 6.39414 10.9428 5.93008C9.97951 4.9668 8.41389 4.96914 7.45061 5.93008L6.85061 6.53242L1.2678 12.1129C0.888883 12.4896 0.588526 12.9377 0.384144 13.4314C0.179761 13.9251 0.0754195 14.4544 0.0771701 14.9887C0.0771701 16.0738 0.501389 17.0934 1.2678 17.8598C2.06233 18.652 3.10295 19.048 4.14358 19.048C5.1842 19.048 6.22483 18.652 7.01701 17.8598L14.2639 10.6176C15.3303 9.54883 15.9209 8.12617 15.9209 6.61445C15.9233 5.10039 15.3326 3.67773 14.2639 2.60898Z" fill="#919293" /></svg></label>
                                                </div>
                                                <div className="col-11 mp-2">
                                                    <div class="input-floating-label">
                                                        <input refs="" type="text" className="textbox--primary textbox--rounded input" name="file" placeholder="Add File" />
                                                        <label >Add File</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="primary" onClick={this.handleClose} className="Save-btn-form">Save</Button>
                                    </Modal.Footer>
                                </Modal>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        );
    }

}


export default Index;