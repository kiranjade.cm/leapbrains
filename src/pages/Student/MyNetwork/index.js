import React, { Component } from "react";
import Tab from "react-bootstrap/Tab";
import { Col, Row } from "react-bootstrap";
import Nav from 'react-bootstrap/Nav'
import "../../../assets/css/network.less"
import Advisor from "./Advisor";
import Friend from "./Friends";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: "Advisors",
            isShowNetwork: true,
        };
    }
    setKey = (k) => {
        this.setState({
            key: k,

        });
    }
    showMyNetwork(e, isvalue) {
        e.preventDefault()
        this.setState({ isShowNetwork: isvalue })
    }

    render() {
        let { isShowNetwork } = this.state;
        return (
            <>
                <div className="networktab">
                    {isShowNetwork &&
                        <div className="tab-heading">
                            <div className="row">
                                <div className="topHeader-text">
                                    <h3>My network</h3>
                                    {/* <p class="p-0 m-0"><span>It’s good to see you again.</span></p> */}
                                </div>
                            </div>
                        </div>
                    }
                    <div className="wrap-space">
                        <div className="tab-container">
                            <Tab.Container id="left-tabs-example" activeKey={this.state.key}
                                onSelect={(k) => this.setKey(k)}>
                                <Row>
                                    {isShowNetwork && <Col sm={12}>
                                        <Nav variant="pills" className="mb-3 pg-top " >
                                            <Nav.Item className="">
                                                <Nav.Link eventKey="Advisors" className="cursorshow" >Advisors</Nav.Link>
                                            </Nav.Item>
                                            <Nav.Item className="">
                                                <Nav.Link eventKey="Friends" className="cursorshow" >Friends</Nav.Link>
                                            </Nav.Item>
                                        </Nav>
                                    </Col>}
                                    <Col sm={12}>
                                        <Tab.Content>
                                            <Tab.Pane eventKey="Advisors">
                                                <Advisor
                                                    showMyNetwork={(e, isvalue) => this.showMyNetwork(e, isvalue)}

                                                />
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="Friends">
                                                <Friend
                                                    showFriendNetwork={(e, isvalue) => this.showMyNetwork(e, isvalue)} />
                                            </Tab.Pane>
                                        </Tab.Content>
                                    </Col>
                                </Row>
                            </Tab.Container>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Index;