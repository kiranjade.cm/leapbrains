import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import { Form, Row, Col } from "react-bootstrap";
import SuccessPopup from "../SuccessPopup";
import gpay from "../../../../assets/images/icons/gpay.png";
import applepay from "../../../../assets/images/icons/applepay.png";
import paypal from "../../../../assets/images/icons/paypal.png";

import ErrorPopup from "../SuccessPopup";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      isBook: this.props.show,
      button: true,
      pay: false,
      isPack: this.props.package,
      button: true,
      button1: true,
      button2: true,
      button3: true,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClick1 = this.handleClick1.bind(this);
    this.handleClick2 = this.handleClick2.bind(this);
    this.handleClick3 = this.handleClick3.bind(this);
  }
  handleClick(e) {
    this.setState({
      button: false,
      button1: true,
      button2: true,
      button3: true,
    });
  }
  handleClick1(e) {
    this.setState({
      button1: false,
      button2: true,
      button3: true,
      button: true,
    });
  }
  handleClick2(e) {
    this.setState({
      button2: false,
      button3: true,
      button: true,
      button1: true,
    });
  }
  handleClick3(e) {
    this.setState({
      button3: false,
      button1: true,
      button2: true,
      button: true,
    });
  }
  handleShow(e) {
    e.preventDefault();
    this.setState({ show: true, isBook: false });
  }
  setShow(isshow) {
    this.setState({ show: isshow });
    this.props.onsetShow(false);
  }
  handlePay(e) {
    console.log("pay")
    this.setState({ pay: true, isPack: false });
    this.props.closePackagePopup();
    e.preventDefault();
   
   
  }
  setPay(ispay) {
    this.setState({ pay: ispay });
    this.props.onsetPackage(false);
  }

  render() {
    console.log(this.state.isPack);
    console.log(this.state.pay);

    let { show, isBook, isPack, pay } = this.state;
    let { SessionPopupData } = this.props;
    const popupData = [
      {
        bodyTitle: "Session booked successfully",
        footerTitle: "Return to Profile",
        footerButton: "Go to Calendar",
      },
    ];
    const paymentData = [
      {
        bodyTitle: "Payment was successful",
        footerTitle: "Return to Profile",
        footerButton: "Go to Calendar",
      },
    ];
    const errorData = [
      {
        bodyTitle: "Error",

        footerButton: "Return to Profile",
      },
    ];

    return (
      <>
        {isBook && (
          <div className="SessionModal">
            <Modal show={this.props.show} onHide={this.props.onHide}>
              <Modal.Header closeButton>
                <Modal.Title> {SessionPopupData[0].title}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form onSubmit={this.handleSubmit}>
                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Control type="text" placeholder="Title" autoFocus />
                  </Form.Group>
                  <Form.Group as={Row} className="mb-1" controlId="formPlan">
                    <Form.Label column xs={2}>
                      Plan:
                    </Form.Label>

                    <Col xs={6}>
                      <Form.Select
                        className="form-select-space"
                        aria-label="Default select example"
                        controlId="selectPlan"
                      >
                        <option value="">Going to Cambridge</option>
                      </Form.Select>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} className="mb-1" controlId="formCourse">
                    <Form.Label column xs={2}>
                      Course:
                    </Form.Label>

                    <Col xs={4}>
                      <Form.Select
                        className="form-select-space"
                        aria-label="Default select example"
                        controlId="selectCourse"
                      >
                        <option value="">Algebra</option>
                      </Form.Select>
                    </Col>
                  </Form.Group>
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="formMilestone"
                  >
                    <Form.Label column xs={2}>
                      Milestone:
                    </Form.Label>

                    <Col xs={4}>
                      <Form.Select
                        className="form-select-space"
                        aria-label="Default select example"
                        controlId="selectMilestone"
                      >
                        <option value="">Milestone</option>
                      </Form.Select>
                    </Col>
                  </Form.Group>

                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="SelectAdvisor"
                  >
                    <Form.Label column xs={1}>
                      <svg
                        width="18"
                        height="20"
                        viewBox="0 0 18 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M12.8972 13.0563C13.4644 12.7516 14.1136 12.5781 14.805 12.5781H14.8074C14.8777 12.5781 14.9105 12.4938 14.8589 12.4469C14.1399 11.8016 13.3185 11.2804 12.4285 10.9047C12.4191 10.9 12.4097 10.8977 12.4004 10.893C13.8558 9.83594 14.8027 8.11797 14.8027 6.17969C14.8027 2.96875 12.2058 0.367188 9.00191 0.367188C5.79801 0.367188 3.20348 2.96875 3.20348 6.17969C3.20348 8.11797 4.15035 9.83594 5.60816 10.893C5.59879 10.8977 5.58941 10.9 5.58004 10.9047C4.53238 11.3477 3.59254 11.9828 2.78395 12.7938C1.98002 13.5962 1.33999 14.5475 0.89957 15.5945C0.466242 16.6198 0.232379 17.7184 0.210508 18.8312C0.209882 18.8563 0.21427 18.8812 0.223412 18.9044C0.232554 18.9277 0.246266 18.949 0.26374 18.9669C0.281214 18.9848 0.302096 18.999 0.325155 19.0087C0.348215 19.0184 0.372986 19.0234 0.398008 19.0234H1.80191C1.9027 19.0234 1.98707 18.9414 1.98941 18.8406C2.03629 17.0312 2.76051 15.3367 4.04254 14.0523C5.36676 12.7234 7.12926 11.9922 9.00426 11.9922C10.3332 11.9922 11.6082 12.3602 12.7074 13.0492C12.7356 13.067 12.768 13.077 12.8014 13.0782C12.8347 13.0794 12.8678 13.0719 12.8972 13.0563ZM9.00426 10.2109C7.93082 10.2109 6.92066 9.79141 6.15895 9.02969C5.78417 8.65588 5.48706 8.21161 5.28473 7.72248C5.0824 7.23335 4.97884 6.70901 4.98004 6.17969C4.98004 5.10391 5.39957 4.09141 6.15895 3.32969C6.91832 2.56797 7.92848 2.14844 9.00426 2.14844C10.08 2.14844 11.0879 2.56797 11.8496 3.32969C12.2243 3.70349 12.5215 4.14776 12.7238 4.63689C12.9261 5.12603 13.0297 5.65036 13.0285 6.17969C13.0285 7.25547 12.6089 8.26797 11.8496 9.02969C11.0879 9.79141 10.0777 10.2109 9.00426 10.2109ZM17.6246 15.7891H15.6558V13.8203C15.6558 13.7172 15.5714 13.6328 15.4683 13.6328H14.1558C14.0527 13.6328 13.9683 13.7172 13.9683 13.8203V15.7891H11.9996C11.8964 15.7891 11.8121 15.8734 11.8121 15.9766V17.2891C11.8121 17.3922 11.8964 17.4766 11.9996 17.4766H13.9683V19.4453C13.9683 19.5484 14.0527 19.6328 14.1558 19.6328H15.4683C15.5714 19.6328 15.6558 19.5484 15.6558 19.4453V17.4766H17.6246C17.7277 17.4766 17.8121 17.3922 17.8121 17.2891V15.9766C17.8121 15.8734 17.7277 15.7891 17.6246 15.7891Z"
                          fill="#919293"
                        />
                      </svg>
                    </Form.Label>
                    <Col xs={11} className="form-select-xs">
                      <Form.Group
                        className="mb-3"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Control
                          type="text"
                          defaultValue={"Nikolay Foster"}
                          autoFocus
                        />
                      </Form.Group>
                    </Col>
                  </Form.Group>
                  <Row className="h-5">
                    <Form.Group
                      as={Col}
                      className="mb-3"
                      controlId="formHorizontalEmail"
                    >
                      <Row className="mb-3">
                        <Form.Label column xs={2}>
                          <svg
                            width="23"
                            height="24"
                            viewBox="0 0 16 18"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <rect
                              x="1"
                              y="3.55859"
                              width="13.7846"
                              height="12.8"
                              rx="1.5"
                              stroke="#919293"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                            <path
                              d="M14.7839 7.82599H0.999283"
                              stroke="#919293"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                            <path
                              d="M4.44626 1V2.70667"
                              stroke="#919293"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                            <path
                              d="M11.3383 1V2.70667"
                              stroke="#919293"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                          </svg>
                        </Form.Label>
                        <Col xs={10}>
                          <Form.Control type="date" placeholder="Select date" />
                        </Col>
                      </Row>
                    </Form.Group>

                    <Form.Group
                      as={Col}
                      className="mb-3"
                      controlId="formHorizontalEmail"
                    >
                      <Row>
                        <Form.Label column xs={2}>
                          <svg
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M12 1.5C6.20156 1.5 1.5 6.20156 1.5 12C1.5 17.7984 6.20156 22.5 12 22.5C17.7984 22.5 22.5 17.7984 22.5 12C22.5 6.20156 17.7984 1.5 12 1.5ZM12 20.7188C7.18594 20.7188 3.28125 16.8141 3.28125 12C3.28125 7.18594 7.18594 3.28125 12 3.28125C16.8141 3.28125 20.7188 7.18594 20.7188 12C20.7188 16.8141 16.8141 20.7188 12 20.7188Z"
                              fill="#919293"
                            />
                            <path
                              d="M16.0945 14.9672L12.7523 12.5508V6.75C12.7523 6.64687 12.668 6.5625 12.5648 6.5625H11.4375C11.3344 6.5625 11.25 6.64687 11.25 6.75V13.2047C11.25 13.2656 11.2781 13.3219 11.3273 13.357L15.2039 16.1836C15.2883 16.2445 15.4055 16.2258 15.4664 16.1437L16.1367 15.2297C16.1976 15.1429 16.1789 15.0258 16.0945 14.9672Z"
                              fill="#919293"
                            />
                          </svg>
                        </Form.Label>
                        <Col xs={10}>
                          <Form.Control type="time" placeholder="Slect time" />
                        </Col>
                      </Row>
                    </Form.Group>
                  </Row>
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="SelectAdvisor"
                  >
                    <Form.Label column xs={1}>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M4.585 13.6063L4.315 13.5943H1.886L5.122 10.3573L5.109 10.0873C5.10417 9.87775 5.01875 9.67808 4.87051 9.52984C4.72226 9.38159 4.52259 9.29618 4.313 9.29135L4.043 9.27835H0L0.014 9.54835C0.048 9.98635 0.367 10.3183 0.808 10.3443L1.078 10.3573H3.508L0.268 13.5943L0.282 13.8633C0.297 14.2963 0.644 14.6433 1.077 14.6593L1.347 14.6723H5.393L5.379 14.4023C5.343 13.9593 5.029 13.6353 4.584 13.6073L4.585 13.6063ZM7.823 9.27835H7.819C7.10411 9.27874 6.41864 9.56317 5.91334 10.0691C5.40805 10.575 5.1243 11.2609 5.1245 11.9761C5.1247 12.6913 5.40883 13.3771 5.91441 13.8827C6.41998 14.3883 7.10561 14.6723 7.8205 14.6723C8.53539 14.6723 9.22102 14.3883 9.72659 13.8827C10.2322 13.3771 10.5163 12.6913 10.5165 11.9761C10.5167 11.2609 10.233 10.575 9.72766 10.0691C9.22236 9.56317 8.53689 9.27874 7.822 9.27835H7.823ZM8.964 13.1193C8.81555 13.2774 8.63685 13.4041 8.43851 13.4918C8.24018 13.5794 8.02625 13.6264 7.80942 13.6298C7.59259 13.6332 7.37728 13.5931 7.17627 13.5117C6.97527 13.4303 6.79266 13.3094 6.63928 13.1561C6.48591 13.0028 6.3649 12.8202 6.28343 12.6192C6.20196 12.4183 6.1617 12.203 6.16503 11.9862C6.16836 11.7693 6.21522 11.5554 6.30282 11.357C6.39043 11.1586 6.51699 10.9799 6.675 10.8313C6.98151 10.5432 7.38811 10.3858 7.80871 10.3922C8.22931 10.3987 8.63088 10.5686 8.92839 10.866C9.2259 11.1633 9.39599 11.5648 9.40264 11.9854C9.40928 12.406 9.25197 12.8127 8.964 13.1193ZM21.84 9.27935C21.5345 9.27976 21.2325 9.34506 20.9541 9.4709C20.6757 9.59675 20.4272 9.78027 20.225 10.0093C20.0228 9.77915 19.7737 9.59478 19.4945 9.46855C19.2153 9.34231 18.9124 9.27713 18.606 9.27735C18.1753 9.27679 17.7545 9.4057 17.398 9.64735C17.188 9.41435 16.718 9.27735 16.449 9.27735V14.6723L16.719 14.6593C17.169 14.6293 17.497 14.3103 17.515 13.8633L17.528 13.5933V11.7043L17.542 11.4343C17.552 11.2323 17.582 11.0523 17.674 10.8943C17.8173 10.6467 18.053 10.4662 18.3292 10.3925C18.6054 10.3188 18.8996 10.358 19.147 10.5013C19.3099 10.5955 19.4454 10.7306 19.54 10.8933C19.633 11.0533 19.66 11.2333 19.672 11.4333L19.686 11.7043V13.5933L19.699 13.8623C19.7074 14.0706 19.7938 14.2681 19.9411 14.4156C20.0884 14.5631 20.2857 14.6497 20.494 14.6583L20.764 14.6713V11.7043L20.776 11.4343C20.786 11.2343 20.816 11.0503 20.91 10.8913C21.21 10.3773 21.87 10.2013 22.383 10.5013C22.5461 10.5957 22.6816 10.7312 22.776 10.8943C22.868 11.0543 22.896 11.2373 22.906 11.4343L22.921 11.7043V13.5933L22.934 13.8623C22.962 14.3053 23.284 14.6323 23.73 14.6583L24 14.6713V11.4343C23.9997 11.151 23.9437 10.8704 23.835 10.6087C23.7263 10.3469 23.5671 10.1092 23.3665 9.90899C23.1659 9.70879 22.9279 9.55005 22.666 9.44185C22.4041 9.33364 22.1234 9.27808 21.84 9.27835V9.27935ZM11.577 10.0673C11.3155 10.3152 11.1063 10.6129 10.9617 10.9429C10.8172 11.2729 10.7401 11.6286 10.7352 11.9889C10.7302 12.3492 10.7975 12.7068 10.9329 13.0407C11.0684 13.3745 11.2694 13.6779 11.524 13.9328C11.7785 14.1877 12.0816 14.389 12.4152 14.5248C12.7488 14.6607 13.1062 14.7283 13.4664 14.7237C13.8266 14.7191 14.1822 14.6423 14.5122 14.498C14.8422 14.3537 15.14 14.1447 15.388 13.8833C15.8719 13.3734 16.1377 12.6945 16.1287 11.9914C16.1197 11.2883 15.8367 10.6165 15.3399 10.119C14.8431 9.62158 14.1718 9.33781 13.4689 9.32816C12.7661 9.3185 12.0873 9.58373 11.577 10.0673ZM14.627 13.1193C14.4772 13.2721 14.2986 13.3936 14.1015 13.4769C13.9044 13.5602 13.6928 13.6036 13.4788 13.6046C13.2649 13.6056 13.0528 13.5642 12.855 13.4828C12.6571 13.4014 12.4774 13.2815 12.3261 13.1302C12.1749 12.9789 12.0551 12.7991 11.9738 12.6012C11.8924 12.4033 11.8511 12.1913 11.8522 11.9773C11.8533 11.7633 11.8968 11.5517 11.9802 11.3547C12.0636 11.1577 12.1852 10.9791 12.338 10.8293C12.6427 10.5307 13.0529 10.3645 13.4795 10.3667C13.9061 10.3689 14.3146 10.5394 14.6162 10.8411C14.9178 11.1428 15.0882 11.5514 15.0902 11.978C15.0922 12.4046 14.9257 12.8148 14.627 13.1193Z"
                          fill="#303132"
                        />
                      </svg>
                    </Form.Label>
                    <Col sm={11} xs={10}>
                      <a
                        href=""
                        class="btn model-content__cta cta--zoombtn"
                      >
                        Going with Zoom
                      </a>
                    </Col>
                  </Form.Group>
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="SelectAdvisor"
                  >
                    <Form.Label column xs={1}>
                      <svg
                        width="20"
                        height="14"
                        viewBox="0 0 20 14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M1.2999 0.400391C1.06121 0.400391 0.832289 0.495212 0.663506 0.663994C0.494723 0.832777 0.399902 1.0617 0.399902 1.30039C0.399902 1.53909 0.494723 1.768 0.663506 1.93679C0.832289 2.10557 1.06121 2.20039 1.2999 2.20039H18.6999C18.9386 2.20039 19.1675 2.10557 19.3363 1.93679C19.5051 1.768 19.5999 1.53909 19.5999 1.30039C19.5999 1.0617 19.5051 0.832777 19.3363 0.663994C19.1675 0.495212 18.9386 0.400391 18.6999 0.400391H1.2999Z"
                          fill="#919293"
                        />
                        <path
                          d="M1.2999 4C1.06121 4 0.832289 4.09482 0.663506 4.2636C0.494723 4.43239 0.399902 4.66131 0.399902 4.9C0.399902 5.13869 0.494723 5.36761 0.663506 5.5364C0.832289 5.70518 1.06121 5.8 1.2999 5.8H18.6999C18.9386 5.8 19.1675 5.70518 19.3363 5.5364C19.5051 5.36761 19.5999 5.13869 19.5999 4.9C19.5999 4.66131 19.5051 4.43239 19.3363 4.2636C19.1675 4.09482 18.9386 4 18.6999 4H1.2999Z"
                          fill="#919293"
                        />
                        <path
                          d="M0.399902 8.49961C0.399902 8.26091 0.494723 8.032 0.663506 7.86321C0.832289 7.69443 1.06121 7.59961 1.2999 7.59961H18.6999C18.9386 7.59961 19.1675 7.69443 19.3363 7.86321C19.5051 8.032 19.5999 8.26091 19.5999 8.49961C19.5999 8.7383 19.5051 8.96722 19.3363 9.13601C19.1675 9.30479 18.9386 9.39961 18.6999 9.39961H1.2999C1.06121 9.39961 0.832289 9.30479 0.663506 9.13601C0.494723 8.96722 0.399902 8.7383 0.399902 8.49961Z"
                          fill="#919293"
                        />
                        <path
                          d="M1.2999 11.2012C1.06121 11.2012 0.832289 11.296 0.663506 11.4648C0.494723 11.6336 0.399902 11.8625 0.399902 12.1012C0.399902 12.3399 0.494723 12.5688 0.663506 12.7376C0.832289 12.9064 1.06121 13.0012 1.2999 13.0012H12.6999C12.9386 13.0012 13.1675 12.9064 13.3363 12.7376C13.5051 12.5688 13.5999 12.3399 13.5999 12.1012C13.5999 11.8625 13.5051 11.6336 13.3363 11.4648C13.1675 11.296 12.9386 11.2012 12.6999 11.2012H1.2999Z"
                          fill="#919293"
                        />
                      </svg>
                    </Form.Label>
                    <Col xs={11} className="form-select-xs">
                      <Form.Control
                        type="text"
                        placeholder="Add descripition"
                        autoFocus
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="SelectAdvisor"
                  >
                    <Form.Label column xs={1}>
                      <svg
                        width="16"
                        height="20"
                        viewBox="0 0 16 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M14.2639 2.60898C12.0561 0.401172 8.46076 0.401172 6.25529 2.60898L0.138108 8.72148C0.098264 8.76133 0.0771701 8.81523 0.0771701 8.87148C0.0771701 8.92773 0.098264 8.98164 0.138108 9.02148L1.00295 9.88633C1.04248 9.92569 1.096 9.94778 1.15178 9.94778C1.20756 9.94778 1.26108 9.92569 1.30061 9.88633L7.4178 3.77383C8.17717 3.01445 9.18733 2.59727 10.2608 2.59727C11.3342 2.59727 12.3444 3.01445 13.1014 3.77383C13.8608 4.5332 14.278 5.54336 14.278 6.61445C14.278 7.68789 13.8608 8.6957 13.1014 9.45508L6.86701 15.6871L5.85686 16.6973C4.91233 17.6418 3.37717 17.6418 2.43264 16.6973C1.97561 16.2402 1.72483 15.6332 1.72483 14.9863C1.72483 14.3395 1.97561 13.7324 2.43264 13.2754L8.6178 7.09258C8.77483 6.93789 8.98108 6.85117 9.20139 6.85117H9.20373C9.42405 6.85117 9.62795 6.93789 9.78264 7.09258C9.93967 7.24961 10.024 7.45586 10.024 7.67617C10.024 7.89414 9.93733 8.10039 9.78264 8.25508L4.72717 13.3059C4.68733 13.3457 4.66623 13.3996 4.66623 13.4559C4.66623 13.5121 4.68733 13.566 4.72717 13.6059L5.59201 14.4707C5.63155 14.5101 5.68506 14.5322 5.74084 14.5322C5.79663 14.5322 5.85014 14.5101 5.88967 14.4707L10.9428 9.41758C11.4092 8.95117 11.6647 8.33242 11.6647 7.67383C11.6647 7.01523 11.4069 6.39414 10.9428 5.93008C9.97951 4.9668 8.41389 4.96914 7.45061 5.93008L6.85061 6.53242L1.2678 12.1129C0.888883 12.4896 0.588526 12.9377 0.384144 13.4314C0.179761 13.9251 0.0754195 14.4544 0.0771701 14.9887C0.0771701 16.0738 0.501389 17.0934 1.2678 17.8598C2.06233 18.652 3.10295 19.048 4.14358 19.048C5.1842 19.048 6.22483 18.652 7.01701 17.8598L14.2639 10.6176C15.3303 9.54883 15.9209 8.12617 15.9209 6.61445C15.9233 5.10039 15.3326 3.67773 14.2639 2.60898Z"
                          fill="#919293"
                        />
                      </svg>
                    </Form.Label>
                    <Col xs={11} className="form-select-xs">
                      <Form.Control
                        type="text"
                        placeholder="Add file"
                        autoFocus
                      />
                    </Col>
                  </Form.Group>
                </Form>
              </Modal.Body>
              <Modal.Footer>
                <a
                  href=""
                  onClick={(e) => this.handleShow(e)}
                  className="btn model-content__cta cta--button w-25 clrbutton"
                >
                  Save
                </a>
              </Modal.Footer>
            </Modal>
          </div>
        )}
        {show && (
          <SuccessPopup
            size="lg"
            dialogClassName=""
            centered={false}
            show={this.state.show}
            onHide={() => this.setShow(false)}
            popupData={popupData}
            onsetShow={(isshow) => this.setShow(isshow)}
          />
        )}
        {isPack && (
          <div className="package">
            <Modal show={this.props.package} onHide={this.props.onHide}>
              <Modal.Header closeButton>
                <Modal.Title>
                  <div className="package__top text-black">
                    Standard Package
                  </div>
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="row">
                  <div className=" col-sm-3 col-md-3 col-6">
                    <div
                      className={
                        this.state.button
                          ? "package__tags"
                          : "package__change-color"
                      }
                      onClick={(e) => this.handleClick(e)}
                    >
                      <span>1 month</span>
                    </div>
                  </div>
                  <div className=" col-sm-3 col-md-3 col-6">
                    <div
                      className={
                        this.state.button1
                          ? "package__tags"
                          : "package__change-color"
                      }
                      onClick={(e) => this.handleClick1(e)}
                    >
                      <span>3 months</span>
                    </div>
                  </div>
                  <div className=" col-sm-3 col-md-3 col-6">
                    <div
                      className={
                        this.state.button2
                          ? "package__tags"
                          : "package__change-color"
                      }
                      onClick={(e) => this.handleClick2(e)}
                    >
                      <span>6 months</span>
                    </div>
                  </div>
                  <div className=" col-sm-3 col-md-3 col-6">
                    <div
                      className={
                        this.state.button3
                          ? "package__tags"
                          : "package__change-color"
                      }
                      onClick={(e) => this.handleClick3(e)}
                    >
                      <span>12 months</span>
                    </div>
                  </div>
                </div>
                <div className="row mt-2 mb-2">
                  <h3 class="package__profile-sub">
                    Price : <span class="package__sub-heading">35$</span>
                  </h3>
                </div>
                <div className="row">
                  <div className="col-1 col-sm-1 col-md-1 col-xl-1 col-xxl-1">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M19.1843 4.91249L12.0006 4.75L4.81688 4.91249C3.48572 4.9426 2.35862 5.90322 2.11793 7.21278C1.53621 10.3779 1.53621 13.6225 2.11793 16.7876C2.35862 18.0972 3.48572 19.0578 4.81688 19.0879L12.0006 19.2504L19.1843 19.0879C20.5155 19.0578 21.6426 18.0972 21.8833 16.7876C22.465 13.6225 22.465 10.3779 21.8833 7.21278C21.6426 5.90322 20.5155 4.9426 19.1843 4.91249ZM4.8508 6.41211L12.0006 6.25038L19.1504 6.41211C19.7707 6.42613 20.2958 6.87374 20.408 7.48393C20.5613 8.31792 20.6717 9.15776 20.7394 10.0002H3.26182C3.32947 9.15776 3.43994 8.31792 3.59322 7.48393C3.70537 6.87374 4.23054 6.42613 4.8508 6.41211ZM3.18164 12.0002C3.18164 13.5119 3.31883 15.0235 3.59322 16.5165C3.70537 17.1266 4.23054 17.5742 4.8508 17.5883L12.0006 17.75L19.1504 17.5883C19.7707 17.5742 20.2958 17.1266 20.408 16.5165C20.6824 15.0235 20.8196 13.5119 20.8196 12.0002H3.18164Z"
                        fill="#919293"
                      />
                    </svg>
                  </div>
                  <div className="col-10 col-sm-10 col-md-11 col-xl-11 col-xxl-11">
                    <div class="input-floating-label">
                      <input
                        refs=""
                        type="text"
                        className="textbox--primary textbox--rounded input"
                        name="card-number"
                        placeholder="0000 0000 0000 0000"
                      />
                      <label>0000 0000 0000 0000</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
                    <div className="row">
                      <div className="col-1 col-sm-1 col-lg-1 col-md-1 col-xl-1 col-xxl-1">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <rect
                            x="5"
                            y="6.55859"
                            width="13.7846"
                            height="12.8"
                            rx="1.5"
                            stroke="#919293"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M18.7837 10.826H4.99904"
                            stroke="#919293"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M8.44552 4V5.70667"
                            stroke="#919293"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <path
                            d="M15.3381 4V5.70667"
                            stroke="#919293"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                      </div>
                      <div className="col-5 col-sm-5 col-lg-5 col-md-5 col-xl-5 col-xxl-5 ms-3 package__margin-zero">
                        <div class="input-floating-label">
                          <input
                            refs=""
                            type="text"
                            className="textbox--primary textbox--rounded input"
                            name="double-number"
                            placeholder="00"
                          />
                          <label>00</label>
                        </div>
                      </div>
                      <div className="col-5 col-sm-5 col-lg-5 col-md-5 col-xl-5 col-xxl-5 ">
                        <div class="input-floating-label">
                          <input
                            refs=""
                            type="text"
                            className="textbox--primary textbox--rounded input"
                            name="two-number"
                            placeholder="00"
                          />
                          <label>00</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
                    <div className="row">
                      <div className="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xxl-1  ">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M10.4993 16.0001C10.4993 15.1717 11.1709 14.5001 11.9993 14.5001C12.8278 14.5001 13.4993 15.1717 13.4993 16.0001C13.4993 16.8285 12.8278 17.5001 11.9993 17.5001C11.1709 17.5001 10.4993 16.8285 10.4993 16.0001Z"
                            fill="#919293"
                          />
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M7.62098 10.5973L7.30554 7.75828C7.2651 7.3943 7.2651 7.02696 7.30554 6.66298L7.32831 6.45809C7.56979 4.2847 9.2784 2.56504 11.4502 2.30953C11.815 2.26661 12.1836 2.26661 12.5484 2.30953C14.7202 2.56504 16.4288 4.2847 16.6703 6.45809L16.6931 6.66298C16.7335 7.02696 16.7335 7.3943 16.6931 7.75828L16.3776 10.5972L17.0642 10.6521C18.147 10.7385 19.0311 11.5524 19.2066 12.6243C19.5727 14.8599 19.5727 17.1403 19.2066 19.3759C19.0311 20.4478 18.147 21.2617 17.0642 21.3482L15.5681 21.4676C13.1927 21.6572 10.806 21.6572 8.43061 21.4676L6.93448 21.3482C5.85175 21.2617 4.96765 20.4478 4.79213 19.3759C4.42606 17.1403 4.42606 14.8599 4.79213 12.6243C4.96765 11.5524 5.85175 10.7385 6.93448 10.6521L7.62098 10.5973ZM11.6254 3.79926C11.8738 3.77004 12.1248 3.77004 12.3732 3.79926C13.8518 3.97322 15.0151 5.14402 15.1795 6.62373L15.2022 6.82863C15.2305 7.08252 15.2305 7.33874 15.2022 7.59263L14.8811 10.4831C12.9619 10.3595 11.0367 10.3595 9.11753 10.4831L8.79636 7.59263C8.76816 7.33875 8.76816 7.08252 8.79636 6.82863L8.81913 6.62373C8.98354 5.14402 10.1468 3.97322 11.6254 3.79926ZM15.4487 12.0279C13.1528 11.8446 10.8459 11.8446 8.54998 12.0279L7.05385 12.1473C6.65892 12.1788 6.33644 12.4757 6.27242 12.8667C5.93263 14.9418 5.93263 17.0584 6.27242 19.1335C6.33644 19.5245 6.65892 19.8214 7.05385 19.8529L8.54998 19.9723C10.8459 20.1556 13.1528 20.1556 15.4487 19.9723L16.9449 19.8529C17.3398 19.8214 17.6623 19.5245 17.7263 19.1335C18.0661 17.0584 18.0661 14.9418 17.7263 12.8667C17.6623 12.4757 17.3398 12.1788 16.9449 12.1473L15.4487 12.0279Z"
                            fill="#919293"
                          />
                        </svg>
                      </div>
                      <div className="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xxl-10 ms-3 package__margin-zero">
                      <div class="input-floating-label w-right-icon">
                            <input
                                type="text"
                                className= {"textbox--primary textbox--rounded input"}
                                name="cvv"
                                placeholder="CVV"
                            />
                            <label >CVV</label>
                            <span className="textbox-icon">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.00008 7.16699C8.27622 7.16699 8.50008 7.39085 8.50008 7.66699V11.0003C8.50008 11.2765 8.27622 11.5003 8.00008 11.5003C7.72394 11.5003 7.50008 11.2765 7.50008 11.0003V7.66699C7.50008 7.39085 7.72394 7.16699 8.00008 7.16699Z" fill="#919293"/>
                                <path d="M8.00008 6.00033C8.36827 6.00033 8.66675 5.70185 8.66675 5.33366C8.66675 4.96547 8.36827 4.66699 8.00008 4.66699C7.63189 4.66699 7.33341 4.96547 7.33341 5.33366C7.33341 5.70185 7.63189 6.00033 8.00008 6.00033Z" fill="#919293"/>
                                <path fillRule="evenodd" clipRule="evenodd" d="M2.16675 8.00033C2.16675 4.77866 4.77842 2.16699 8.00008 2.16699C11.2217 2.16699 13.8334 4.77866 13.8334 8.00033C13.8334 11.222 11.2217 13.8337 8.00008 13.8337C4.77842 13.8337 2.16675 11.222 2.16675 8.00033ZM8.00008 3.16699C5.3307 3.16699 3.16675 5.33095 3.16675 8.00033C3.16675 10.6697 5.3307 12.8337 8.00008 12.8337C10.6695 12.8337 12.8334 10.6697 12.8334 8.00033C12.8334 5.33095 10.6695 3.16699 8.00008 3.16699Z" fill="#919293"/>
                                </svg>
                            </span>
                        </div>
</div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-1 col-sm-1 col-md-1 col-xl-1 col-xxl-1">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M15.8977 15.0563C16.4649 14.7516 17.1141 14.5781 17.8055 14.5781H17.8079C17.8782 14.5781 17.911 14.4938 17.8594 14.4469C17.1404 13.8016 16.319 13.2804 15.429 12.9047C15.4196 12.9 15.4102 12.8977 15.4008 12.893C16.8563 11.8359 17.8032 10.118 17.8032 8.17969C17.8032 4.96875 15.2063 2.36719 12.0024 2.36719C8.7985 2.36719 6.20397 4.96875 6.20397 8.17969C6.20397 10.118 7.15084 11.8359 8.60865 12.893C8.59928 12.8977 8.5899 12.9 8.58053 12.9047C7.53287 13.3477 6.59303 13.9828 5.78443 14.7938C4.98051 15.5962 4.34048 16.5475 3.90006 17.5945C3.46673 18.6198 3.23287 19.7184 3.211 20.8312C3.21037 20.8563 3.21476 20.8812 3.2239 20.9044C3.23304 20.9277 3.24675 20.949 3.26423 20.9669C3.2817 20.9848 3.30258 20.999 3.32564 21.0087C3.3487 21.0184 3.37347 21.0234 3.3985 21.0234H4.8024C4.90318 21.0234 4.98756 20.9414 4.9899 20.8406C5.03678 19.0312 5.761 17.3367 7.04303 16.0523C8.36725 14.7234 10.1297 13.9922 12.0047 13.9922C13.3337 13.9922 14.6087 14.3602 15.7079 15.0492C15.7361 15.067 15.7685 15.077 15.8019 15.0782C15.8352 15.0794 15.8682 15.0719 15.8977 15.0563ZM12.0047 12.2109C10.9313 12.2109 9.92115 11.7914 9.15943 11.0297C8.78466 10.6559 8.48755 10.2116 8.28521 9.72248C8.08288 9.23335 7.97933 8.70901 7.98053 8.17969C7.98053 7.10391 8.40006 6.09141 9.15943 5.32969C9.91881 4.56797 10.929 4.14844 12.0047 4.14844C13.0805 4.14844 14.0883 4.56797 14.8501 5.32969C15.2248 5.70349 15.5219 6.14776 15.7243 6.63689C15.9266 7.12603 16.0302 7.65036 16.029 8.17969C16.029 9.25547 15.6094 10.268 14.8501 11.0297C14.0883 11.7914 13.0782 12.2109 12.0047 12.2109ZM20.6251 17.7891H18.6563V15.8203C18.6563 15.7172 18.5719 15.6328 18.4688 15.6328H17.1563C17.0532 15.6328 16.9688 15.7172 16.9688 15.8203V17.7891H15.0001C14.8969 17.7891 14.8126 17.8734 14.8126 17.9766V19.2891C14.8126 19.3922 14.8969 19.4766 15.0001 19.4766H16.9688V21.4453C16.9688 21.5484 17.0532 21.6328 17.1563 21.6328H18.4688C18.5719 21.6328 18.6563 21.5484 18.6563 21.4453V19.4766H20.6251C20.7282 19.4766 20.8126 19.3922 20.8126 19.2891V17.9766C20.8126 17.8734 20.7282 17.7891 20.6251 17.7891Z"
                        fill="#919293"
                      />
                    </svg>
                  </div>
                  <div className="col-10 col-sm-10 col-md-11 col-xl-11 col-xxl-11">
                    <div class="input-floating-label">
                      <input
                        refs=""
                        type="text"
                        className="textbox--primary textbox--rounded input"
                        name="owner-name"
                        placeholder="card owner name"
                      />
                      <label>card owner name</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-1 col-sm-1 col-md-1 col-xl-1 col-xxl-1">
                    <Form>
                      <Form.Check
                        className="text-black"
                        type="switch"
                        id="custom-switch"
                      />
                    </Form>
                  </div>
                  <div className="col-10 col-sm-10 col-md-11 col-xl-11 col-xxl-11">
                    <Form>
                      <Form.Group
                        className="mb-3 ms-2"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label >Automatic payments</Form.Label>
                      </Form.Group>
                    </Form>
                  </div>
                </div>
                <div className="payment-method">
                  <div className="row">
                    <div className="col-sm-4  col-md-4 col-4">
                      <div className="border-rad">
                        <img src={gpay} className="" />
                      </div>
                    </div>
                    <div className="col-sm-4 col-md-4 col-4 ">
                      <div className="border-rad">
                        <img src={applepay} className="" />
                      </div>
                    </div>
                    <div className="col-sm-4 col-md-4 col-4 ">
                      <div className="border-rad">
                        <img src={paypal} className="" />
                      </div>
                    </div>
                  </div>
                </div>

                <div>
                  <p className="payment-method__popup-text mt-4 ps-1 pe-1">
                    By clicking forward to make a payment, you acknowledge that
                    you have read and understand this{" "}
                    <span className="text-black">agreement</span> and you agree
                    to be bound by the{" "}
                    <span className="text-black">terms and conditions</span> set
                    forth above and any future amendments to this agreement
                    which may be made from time to time
                  </p>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <a
                  href=""
                  onClick={(e) => this.handlePay(e)}
                  className="footer__cta d-inline-block  "
                >
                  Pay
                </a>
              </Modal.Footer>
            </Modal>
          </div>
        )}

        {this.state.pay && (
          <ErrorPopup
            size="lg"
            dialogClassName=""
            centered={false}
            error={this.state.pay}
            onClose={() => this.setPay(false)}
            errorData={errorData}
            onsetPackage={(ispay) => this.setPay(ispay)}
          />
        )}
      </>
    );
  }
}
export default Index;
