import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 0,
      show: false,
    };
  }
  changeRating(newRating) {
    this.setState({
      rating: newRating,
    });
  }

  render() {
    let reviewDetails = this.props.reviewDatas.map((element, key) => {
      console.log(this.props.reviewDatas);
      return (
        <>
           <div className="profil-reviews">
            <div className="student">
              <div className="row m-0">
                <div className="col-12 d-flex">
                  <div className="Avatar-icon">
                    <img src={element.icon} alt={element.AvatarIconAlt} />
                  </div>
                  <div className="student">
                    <div className="d-flex">
                    <h2 className="student__title review-title me-2 ms-2 mt-1">
                      {element.name} </h2>
                      <div className="img">
                      <svg
                    
                        width="15"
                        height="14"
                        viewBox="0 0 12 11"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clipPath="url(#clip0_13_14303)">
                          <path
                            d="M11.0673 4.27371C11.161 4.56607 11.0843 4.881 10.867 5.09558L8.96704 6.97182C8.92975 7.00868 8.91273 7.06175 8.92152 7.11377L9.37004 9.76309C9.42135 10.066 9.30072 10.3665 9.05523 10.5472C8.91655 10.6494 8.75422 10.7013 8.59086 10.7013C8.46508 10.7013 8.33868 10.6705 8.22186 10.6083L5.87338 9.35749C5.82723 9.33291 5.77218 9.33291 5.72605 9.35749L3.37759 10.6083C3.10904 10.7513 2.78964 10.728 2.54415 10.5473C2.29867 10.3666 2.17806 10.0661 2.22934 9.76311L2.67786 7.11379C2.68668 7.06177 2.66964 7.0087 2.63235 6.97185L0.732388 5.09558C0.515075 4.881 0.438341 4.56607 0.532138 4.27371C0.625935 3.98134 0.870692 3.77227 1.17097 3.72808L3.79663 3.34157C3.84819 3.33397 3.89272 3.30118 3.91581 3.25386L5.09005 0.843404C5.22433 0.56773 5.49625 0.396484 5.79969 0.396484C6.10314 0.396484 6.37506 0.56773 6.50933 0.843404L7.68358 3.25383C7.70664 3.30118 7.75119 3.33397 7.80273 3.34154L10.4284 3.72808C10.7287 3.77227 10.9735 3.98134 11.0673 4.27371Z"
                            fill={"#E64A19"}
                            stroke={"#E64A19"}
                            strokeWidth={"1.5"}
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0_13_14303">
                            <rect
                              width="10.8"
                              height="11"
                              fill="white"
                              transform="translate(0.400024)"
                            />
                          </clipPath>
                        </defs>
                      </svg>
                      </div>
                      <div className="rate ms-1">
                      <span className="sub-heading review-rating">
                        {" "}
                        {element.rating}{" "}
                      </span>
                      </div>
                      </div>
                    <p className="student__sub-title">
                      {element.date}
                      <svg
                        className="ms-1 me-1"
                        width="5"
                        height="5"
                        viewBox="0 0 5 5"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <circle cx="2.5" cy="2.5" r="2.5" fill="#5C5C5C" />
                      </svg>
                      {element.course}
                    </p>
                    <p class="student__List ">{element.text}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="network-profile">
          <div className="vertic-line" />
          </div>
        </>
      );
    });
    return (
      <>
        <Modal
          show={this.props.showReview}
          fullscreen={this.props.fullscreen}
          onHide={this.props.onHide}
          centered={this.props.centered}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              <div className="review-popup">
              
              <div className="row">
                <div className=" d-flex">
                  <div className="mt-5 ms-5 cursorshow" onClick={this.props.onHide}>
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M13.3333 8.00065C13.3333 7.63246 13.0348 7.33398 12.6666 7.33398H3.33325C2.96506 7.33398 2.66659 7.63246 2.66659 8.00065C2.66659 8.36884 2.96506 8.66732 3.33325 8.66732H12.6666C13.0348 8.66732 13.3333 8.36884 13.3333 8.00065Z" fill="#1B1C1E"></path><path fillRule="evenodd" clipRule="evenodd" d="M8.47149 2.86128C8.21114 2.60093 7.78903 2.60093 7.52868 2.86128L2.86201 7.52794C2.60166 7.78829 2.60166 8.2104 2.86201 8.47075L7.52868 13.1374C7.78903 13.3978 8.21114 13.3978 8.47149 13.1374C8.73184 12.8771 8.73184 12.455 8.47149 12.1946L4.27622 7.99935L8.47149 3.80409C8.73184 3.54374 8.73184 3.12163 8.47149 2.86128Z" fill="#1B1C1E"></path></svg>
                 </div>
                  <h2 className="ms-5 mt-5  popup-top">More reviews</h2>
                </div>
              </div>
              </div>
              
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
          
              <div className="review-pop-sec">
                <div className="">{reviewDetails}</div>
              </div>
            
          </Modal.Body>
          <Modal.Footer></Modal.Footer>
        </Modal>
      </>
    );
  }
}
export default Index;
