import React, { Component } from "react";
import SelectAdvisors from "../../../../components/OwlCarousel";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleBack(e) {
        e.preventDefault();
        this.props.handleBack(e);
    }

    render() {


        const OwlCarouselData = [
            {
                Description: "",
                AdvisorsImage: "http://via.placeholder.com/70x72",
                Session: "$12/session",
                Rating: "4.7/5.0",
                AdvisorsName: "Nikolay Foster",
                Location: "USA, Cambridge",
                University: "Harvard University",
                tags: ["Economy", "Management", "Marketing", "History", "Maths"],
            },
            {
                Description: "",
                AdvisorsImage: "http://via.placeholder.com/70x72",
                Session: "$11/session",
                Rating: "4.7/5.0",
                AdvisorsName: "Nikolay Foster",
                Location: "USA, Cambridge",
                University: "Harvard University",
                tags: ["Economy", "Management", "Marketing", "History", "Maths"],
            },
            {
                Description: "",
                AdvisorsImage: "http://via.placeholder.com/70x72",
                Session: "$10/session",
                Rating: "4.7/5.0",
                AdvisorsName: "Nikolay Foster",
                Location: "USA, Cambridge",
                University: "Harvard University",
                tags: ["Economy", "Management", "Marketing", "History", "Maths"],
            },
            {
                Description: "",
                AdvisorsImage: "http://via.placeholder.com/70x72",
                Session: "$12/session",
                Rating: "4.5/5.0",
                AdvisorsName: "Nikolay Foster",
                Location: "USA, Cambridge",
                University: "Harvard University",
                tags: ["Economy", "Management", "Marketing", "History", "Maths"],
            },
            {
                Description: "",
                AdvisorsImage: "http://via.placeholder.com/70x72",
                Session: "$12/session",
                Rating: "4.7/5.0",
                AdvisorsName: "Nikolay Foster",
                Location: "USA, Cambridge",
                University: "Harvard University",
                tags: ["Economy", "Management", "Marketing", "History", "Maths"],
            },
            {
                Description: "",
                AdvisorsImage: "http://via.placeholder.com/70x72",
                Session: "$12/session",
                Rating: "4.7/5.0",
                AdvisorsName: "Nikolay Foster",
                Location: "USA, Cambridge",
                University: "Harvard University",
                tags: ["Economy", "Management", "Marketing", "History", "Maths"],
            },
            {
                Description: "",
                AdvisorsImage: "http://via.placeholder.com/70x72",
                Session: "$12/session",
                Rating: "4.7/5.0",
                AdvisorsName: "Nikolay Foster",
                Location: "",
                University: "Harvard University",
                tags: ["Economy", "Management", "Marketing", "History", "Maths"],
            },
        ];
        const InterestData = [
            {
                'id': 1,
                'Description': '',
                'AdvisorsImage': 'http://via.placeholder.com/70x72',
                'Session': '$12/session',
                'Rating': '4.7/5.0',
                'AdvisorsName': 'Nikolay Foster',
                'Location': 'USA, Cambridge',
                'University': 'Harvard University',
                'tags': ['Economy', 'Management', 'Marketing', 'History', 'Maths']
            },
            {
                'id': 2,
                'Description': '',
                'AdvisorsImage': 'http://via.placeholder.com/70x72',
                'Session': '$11/session',
                'Rating': '4.7/5.0',
                'AdvisorsName': 'Nikolay Foster',
                'Location': 'USA, Cambridge',
                'University': 'Harvard University',
                'tags': ['Economy', 'Management', 'Marketing', 'History', 'Maths']
            },
            {
                'Description': '',
                'AdvisorsImage': 'http://via.placeholder.com/70x72',
                'Session': '$10/session',
                'Rating': '4.7/5.0',
                'AdvisorsName': 'Nikolay Foster',
                'Location': 'USA, Cambridge',
                'University': 'Harvard University',
                'tags': ['Economy', 'Management', 'Marketing', 'History', 'Maths']
            },
            {
                'Description': '',
                'AdvisorsImage': 'http://via.placeholder.com/70x72',
                'Session': '$12/session',
                'Rating': '4.5/5.0',
                'AdvisorsName': 'Nikolay Foster',
                'Location': 'USA, Cambridge',
                'University': 'Harvard University',
                'tags': ['Economy', 'Management', 'Marketing', 'History', 'Maths']
            },
            {
                'Description': '',
                'AdvisorsImage': 'http://via.placeholder.com/70x72',
                'Session': '$12/session',
                'Rating': '4.7/5.0',
                'AdvisorsName': 'Nikolay Foster',
                'Location': 'USA, Cambridge',
                'University': 'Harvard University',
                'tags': ['Economy', 'Management', 'Marketing', 'History', 'Maths']
            },
            {
                'Description': '',
                'AdvisorsImage': 'http://via.placeholder.com/70x72',
                'Session': '$12/session',
                'Rating': '4.7/5.0',
                'AdvisorsName': 'Nikolay Foster',
                'Location': 'USA, Cambridge',
                'University': 'Harvard University',
                'tags': ['Economy', 'Management', 'Marketing', 'History', 'Maths']
            },
            {
                'Description': '',
                'AdvisorsImage': 'http://via.placeholder.com/70x72',
                'Session': '$12/session',
                'Rating': '4.7/5.0',
                'AdvisorsName': 'Nikolay Foster',
                'Location': '',
                'University': 'Harvard University',
                'tags': ['Economy', 'Management', 'Marketing', 'History', 'Maths']
            },
        ];
        let { title } = this.props;

        var sameInterestDetails =
            InterestData &&
            InterestData.length > 0 &&
            InterestData.map((element, key) => {
                return (
                    <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3 pb-3">
                        <div className="explore-card">
                            <div className="card__item ">
                                <div className="card__item__lists">
                                    <div className="row ">
                                        <div className="col-sm-12 col-12 col-md-12 mt-3">
                                            <div className="row">
                                                <div className="col-6  col-sm-5 col-md-5">
                                                    <div className="card__item__lists__icon ">
                                                        <img src={element.AdvisorsImage} alt="images" />
                                                    </div>
                                                </div>
                                                <div className="col-6 col-sm-7 col-md-7">
                                                    <div className="float-rights">
                                                        <div className="card__item__lists__session_left">
                                                            <p> {element.Session}</p>
                                                        </div>
                                                        <div className="card__item__lists__rating_left">
                                                            <p>
                                                                <svg
                                                                    width="17"
                                                                    height="16"
                                                                    viewBox="0 0 17 16"
                                                                    fill="none"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                >
                                                                    <path
                                                                        d="M14.2171 5.51641L10.2423 4.93985L8.46551 1.34454C8.41698 1.2461 8.33714 1.16641 8.23852 1.11797C7.99117 0.9961 7.6906 1.09766 7.56692 1.34454L5.79009 4.93985L1.81532 5.51641C1.70574 5.53204 1.60555 5.5836 1.52884 5.66172C1.4361 5.75686 1.385 5.88485 1.38676 6.01757C1.38852 6.15029 1.443 6.27689 1.53823 6.36954L4.41403 9.16797L3.73461 13.1195C3.71868 13.2115 3.72887 13.306 3.76403 13.3924C3.79919 13.4789 3.85791 13.5537 3.93354 13.6086C4.00916 13.6634 4.09866 13.696 4.19189 13.7026C4.28512 13.7092 4.37834 13.6897 4.461 13.6461L8.01622 11.7805L11.5714 13.6461C11.6685 13.6977 11.7812 13.7148 11.8892 13.6961C12.1616 13.6492 12.3448 13.3914 12.2978 13.1195L11.6184 9.16797L14.4942 6.36954C14.5725 6.29297 14.6241 6.19297 14.6398 6.0836C14.6821 5.81016 14.4911 5.55704 14.2171 5.51641ZM10.4083 8.77422L10.9734 12.0602L8.01622 10.5102L5.05901 12.0617L5.62415 8.77579L3.23209 6.44766L6.5384 5.96797L8.01622 2.97891L9.49404 5.96797L12.8003 6.44766L10.4083 8.77422Z"
                                                                        fill="#FA5F1C"
                                                                    />
                                                                </svg>
                                                                <span> {element.Rating}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <h3 className="card__item__lists__titel">
                                                {element.AdvisorsName}
                                            </h3>
                                            <div className="card__item__lists__location">
                                                {element.Location && (
                                                    <p>
                                                        <svg
                                                            width="17"
                                                            height="16"
                                                            viewBox="0 0 17 16"
                                                            fill="none"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                        >
                                                            <path
                                                                d="M12.0748 5.49579C11.8625 5.00796 11.5563 4.56627 11.1737 4.19564C10.7897 3.82167 10.3371 3.52484 9.84048 3.32138C9.3239 3.10788 8.77574 3 8.21067 3C7.64559 3 7.09743 3.10788 6.58086 3.32026C6.0812 3.5259 5.63342 3.82032 5.24768 4.19452C4.86524 4.56535 4.55912 5.00699 4.34649 5.49466C4.12655 6.00034 4.01489 6.53748 4.01489 7.09035C4.01489 7.88369 4.20551 8.67367 4.57997 9.43555C4.88112 10.048 5.30182 10.6447 5.83193 11.2122C6.73764 12.1808 7.68732 12.773 7.95689 12.9315C8.03341 12.9765 8.12068 13.0002 8.20954 13C8.29752 13 8.38436 12.9775 8.46219 12.9315C8.73176 12.773 9.68145 12.1808 10.5871 11.2122C11.1173 10.6458 11.538 10.048 11.8391 9.43555C12.2158 8.67479 12.4064 7.88594 12.4064 7.09147C12.4064 6.5386 12.2948 6.00146 12.0748 5.49579ZM8.21067 12.1381C7.46738 11.6673 4.82698 9.80301 4.82698 7.09147C4.82698 6.21609 5.17775 5.39353 5.81502 4.77323C6.45453 4.15181 7.30497 3.80908 8.21067 3.80908C9.11637 3.80908 9.9668 4.15181 10.6063 4.77436C11.2436 5.39353 11.5944 6.21609 11.5944 7.09147C11.5944 9.80301 8.95395 11.6673 8.21067 12.1381ZM8.21067 5.20249C7.11435 5.20249 6.22557 6.08799 6.22557 7.18024C6.22557 8.2725 7.11435 9.15799 8.21067 9.15799C9.30698 9.15799 10.1958 8.2725 10.1958 7.18024C10.1958 6.08799 9.30698 5.20249 8.21067 5.20249ZM9.10396 8.07023C8.9868 8.1873 8.84756 8.28013 8.69425 8.34338C8.54095 8.40664 8.3766 8.43907 8.21067 8.43881C7.87343 8.43881 7.55649 8.30734 7.31737 8.07023C7.19987 7.9535 7.1067 7.81478 7.04321 7.66204C6.97972 7.5093 6.94717 7.34556 6.94742 7.18024C6.94742 6.84425 7.07939 6.52849 7.31737 6.29026C7.55649 6.05203 7.87343 5.92168 8.21067 5.92168C8.54791 5.92168 8.86485 6.05203 9.10396 6.29026C9.34308 6.52849 9.47391 6.84425 9.47391 7.18024C9.47391 7.51624 9.34308 7.832 9.10396 8.07023Z"
                                                                fill="#212224"
                                                            />
                                                        </svg>
                                                        <span>{element.Location}</span>
                                                    </p>
                                                )}
                                                {element.University && (
                                                    <p>
                                                        <svg
                                                            width="17"
                                                            height="16"
                                                            viewBox="0 0 17 16"
                                                            fill="none"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                        >
                                                            <path
                                                                d="M12.8396 7.64649L8.56168 3.38664L8.27494 3.10095C8.2096 3.03629 8.12125 3 8.02916 3C7.93706 3 7.84871 3.03629 7.78338 3.10095L3.21874 7.64649C3.1518 7.71292 3.09889 7.79205 3.06314 7.8792C3.02739 7.96634 3.00953 8.05974 3.0106 8.15387C3.01503 8.54214 3.33942 8.85208 3.72912 8.85208H4.19965V12.4446H11.8587V8.85208H12.3392C12.5285 8.85208 12.7067 8.77818 12.8407 8.64472C12.9066 8.57921 12.9589 8.50132 12.9944 8.41555C13.03 8.32978 13.0481 8.23784 13.0477 8.14505C13.0477 7.95754 12.9735 7.77995 12.8396 7.64649ZM8.64914 11.6504H7.40917V9.40028H8.64914V11.6504ZM11.0615 8.05791V11.6504H9.35769V9.13556C9.35769 8.89179 9.15952 8.69435 8.91485 8.69435H7.14346C6.89879 8.69435 6.70062 8.89179 6.70062 9.13556V11.6504H4.99677V8.05791H3.93394L8.03026 3.98006L8.28601 4.23485L12.1255 8.05791H11.0615Z"
                                                                fill="#212224"
                                                            />
                                                        </svg>
                                                        <span>{element.University}</span>
                                                    </p>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12 p-0">
                                            <div className="card__item__lists__tags">
                                                {element.tags.map((tags) => (
                                                    <span key={tags.toString() + key}>{tags}</span>
                                                ))}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        return (
            <>

                <div className="tab-heading none-view">
                    <div className="row">
                        <div className="topHeader-text">
                            <h3>My network</h3>
                            {/* <p class="p-0 m-0"><span>It’s good to see you again.</span></p> */}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-sm-6 col-ms-6 d-flex pg-top ">
                        <span onClick={(e) => this.handleBack(e)} className="pt-2 arrow-view cursorshow">
                            <svg
                                width="16"
                                height="16"
                                viewBox="0 0 16 16"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M13.3333 8.00065C13.3333 7.63246 13.0348 7.33398 12.6666 7.33398H3.33325C2.96506 7.33398 2.66659 7.63246 2.66659 8.00065C2.66659 8.36884 2.96506 8.66732 3.33325 8.66732H12.6666C13.0348 8.66732 13.3333 8.36884 13.3333 8.00065Z"
                                    fill="#1B1C1E"
                                />
                                <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M8.47149 2.86128C8.21114 2.60093 7.78903 2.60093 7.52868 2.86128L2.86201 7.52794C2.60166 7.78829 2.60166 8.2104 2.86201 8.47075L7.52868 13.1374C7.78903 13.3978 8.21114 13.3978 8.47149 13.1374C8.73184 12.8771 8.73184 12.455 8.47149 12.1946L4.27622 7.99935L8.47149 3.80409C8.73184 3.54374 8.73184 3.12163 8.47149 2.86128Z"
                                    fill="#1B1C1E"
                                />
                            </svg>
                        </span>
                        <h3 className="network-top-text mb-4 ms-2 pt-2 none-text">

                            Explore {title}
                        </h3>
                    </div>
                   
                </div>

                <div className="advisor-friends">
                    <h5 className="advisor-top ms-4 mb-4">Featured </h5>
                    <div className="mt-0">
                        <div className="explore-list">
                            <div className="SelectAdvisors__list">
                                <SelectAdvisors
                                    OwlCarouselData={OwlCarouselData}
                                    Carouselitems={4}
                                    islooping={true}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="explore-grid">
                        <h5 className="advisor-top ms-4 mb-4">With same interests
                        </h5>
                        <div className="row">{sameInterestDetails}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Index;
