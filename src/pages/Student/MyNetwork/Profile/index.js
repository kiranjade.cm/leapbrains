import React, { Component } from "react";

import ProgressBar from "react-bootstrap/ProgressBar";
import Rating from "../../../../components/Rating";
import Avatar1 from "../../../../assets/images/icons/Avatar1.png";
import BookSessionPopup from "../BookSessionPopup";
import ReviewPopup from "../ReviewPopup";
import FeedBackPopup from "../FeedBackPopup"

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            review: false,
            hover: false,
            isSession: true,
            feedBack: false,
            profileData: [
                {
                    title: "About",
                    bachelor: "Stanford University, CA",
                    major: "Massachusetts Institute of Technology, Cambridge, MA",
                    links: ["Facebook", "Twitter", "Instagram"],
                    tags: [
                        "Artificial intelegent",
                        "Data structures",
                        "Deep learning",
                        "Data science",
                        "Machine learning",
                    ],
                },
            ],
            packageData: [
                {
                    id: 1,
                    title: "Basic Package",
                    bachelor: "One time service",
                    price: "15$",
                    month: "3",
                    service: "In person 1:1 tutoring  ",
                    text: "10 people subscripted currently",
                    text1: "50 people subscripted last year",
                    button: "Purchase",
                },
                {
                    id: 2,
                    title: "Standard Package",
                    bachelor: "One time service",
                    price: "35$",
                    month: "5",
                    service: "In person 1:1 tutoring  ",
                    text: "23 people subscripted",
                    button: "Purchase",
                },
                {
                    id: 3,
                    title: "Premium Package",
                    bachelor: "One time service",
                    price: "55$",
                    month: "6",
                    service: "In person 1:1 tutoring  ",
                    text: "34 people subscripted",
                    button: "Purchase",
                },
            ],
            hoursData: [
                {
                    title: "Working hours",
                    monday: [
                        "8 am - 9 am",
                        "10 am - 11 am",
                        "11 am - 12 am",
                        "1 pm - 2 pm",
                        "4 pm - 5 pm",
                    ],
                    tuesday: ["8 am - 9 am", "10 am - 11 am", "11 am - 12 am"],
                    wednesday: ["8 am - 9 am", "10 am - 11 am"],
                    thursday: ["11 am - 12 am", "1 pm - 2 pm", "4 pm - 5 pm"],
                    friday: ["8 am - 9 am", "10 am - 11 am"],
                    saturday: ["8 am - 9 am"],
                    sunday: ["8 am - 9 am"],
                },
            ],
            reviewData: [
                {
                    title: "What students says ",
                    stars5: "80",
                    stars4: "30",
                    stars3: "10",
                    stars2: "0",
                    stars1: "0",
                },
            ],
            studentsSaysData: [
                {
                    id: 1,

                    icon: Avatar1,
                    name: "Daniel",
                    rating: "5",
                    date: "October 23, 2021",
                    course: "General",
                    text: "I love that Nikolay thoroughly prepares for each class and gives me the material accordingly to my goals. Unlike the many other teachers, Nikolay is interested in my progress.",
                },
                {
                    id: 2,

                    icon: Avatar1,
                    name: "Viktor",
                    rating: "5",
                    date: "September 30, 2021",
                    course: "General",
                    text: "Nikolay is a greate mentor. He established a learning plan according to the mentee’s need and provide guidance and useful tips. I highly recommend him if you want to improve in field",
                },
            ],
            profileImage: [
                {
                    icon: Avatar1,
                    name: "Nikolay Foster",
                    rating: "4",
                    profileImage: Image,
                },
            ],
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleMouseOut = this.handleMouseOut.bind(this);
    }
    handleShow(e) {
        e.preventDefault();
        this.setState({ show: true });
    }
    setShow(isshow) {
        this.setState({ show: isshow });
    }

    showFeedBack(e) {
        e.preventDefault();
        this.setState({ feedBack: true });
    }
    setFeedBack(isfeedBack) {
        this.setState({ feedBack: isfeedBack });
    }
    showReview(e) {
        e.preventDefault();
        this.setState({ review: true });
    }
    setReview(isreview) {
        this.setState({ review: isreview });
    }
    handlePay(e) {
        e.preventDefault();
        this.setState({ package: true });
    }
    setPay(ispackage) {
        this.setState({ package: ispackage });
    }
    closePackagePopup(){
        console.log("payclose")
        this.setState({ package: false });
      
    }
    handleClick() {
        this.setState({ hover: true });
    }

    handleMouseOut() {
        this.setState({ hover: false });
    }

    render() {
        let { show, isSession, review, feedBack } = this.state;
        const SessionPopupData = [
            {
                title: "Book session?",
            },
        ];

        let ProfileList = this.state.profileImage.map((element, key) => {
            return (
                <>
                    <div className="card-profile">
                        <div className="profilepic">
                            <div class="card">
                                {" "}
                                <img
                                    class="card-img-top"
                                    src="https://i.imgur.com/K7A78We.jpg"
                                    alt="Card image cap"
                                />
                                <div class="card-body little-profile">
                                    <div className="row">
                                        <div className="col-sm-7 col-md-7 col-12 ">
                                            <div className="row m-0">
                                                <div className="img-text d-flex">

                                                    <div class="pro-img">
                                                        <img
                                                            src="https://i.imgur.com/8RKXAIV.jpg"
                                                            alt="user"
                                                        />
                                                    </div>
                                                    <div className="">
                                                        <h2 className="card-title">
                                                            {element.name}
                                                        </h2>

                                                        <div className="card-rating">
                                                            <p className="card-rating__rating-title pe-1">Rating : </p>
                                                            <div className="card-rating__star-rate">
                                                                <Rating
                                                                    isRating={true}
                                                                    RatingValue={element.rating}
                                                                />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div className="col-sm-5 col-md-5 col-12">
                                            {isSession === false ? (
                                                <div className="card-button">
                                                    <div className="card-button__msg-button me-2">
                                                        <a className="btn text-black text">
                                                            Message
                                                        </a>
                                                    </div>
                                                    <div className="card-button__book-button">
                                                        <a
                                                            href=""
                                                            onClick={(e) => this.handleShow(e)}
                                                            className="btn cta--rounded cta-primary text-white text "
                                                        >
                                                            Book Session
                                                        </a>
                                                    </div>
                                                </div>
                                            ) : (
                                                <div className="card-button__request-button">
                                                    <a
                                                        href=""
                                                        className="footer__cta d-inline-block  text"
                                                    >
                                                        Request connection
                                                    </a>
                                                </div>
                                            )}
                                            {/* {show && (
                      <BookSessionPopup
                        size="lg"
                        dialogClassName={"BookSessionPopup"}
                        centered={false}
                        show={this.state.show}
                        onHide={() => this.setShow(false)}
                        SessionPopupData={SessionPopupData}
                        onsetShow={(isshow) => this.setShow(isshow)}
                      />
                    )} */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            );
        });

        let ProfileDetails = this.state.profileData.map((element, key) => {
            return (
                <>
                    <div className="mb-4">
                        <div className="row">
                            <div className="col-12">
                                <div className="profile-card">
                                    <div className="card__item">
                                        <div className="card__item__lists__card-height">
                                            <div className="row">
                                                <div className="col-sm-12 col-12 col-md-6">
                                                    <h3 className="card__item__lists__titel ft-range26">
                                                        {element.title}
                                                    </h3>
                                                    <div className="vertic-line py-1" />
                                                </div>
                                                <div className="col-sm-6 empty-div">
                                                    <h3
                                                        className="card__item__lists__titel "
                                                        style={{ visibility: "hidden" }}
                                                    >
                                                        {"."}
                                                    </h3>
                                                    <div className="vertic-line py-1" />
                                                </div>
                                            </div>
                                            <div className="row m-0 mb-4">
                                                <div className="col-sm-12 col-12 col-md-6  col-lg-6  ps-0 pright-zero">
                                                    <div className="row m-0">
                                                        <div className="col-3 p-0">
                                                            <h3 className="card__item__lists__profile-sub ft-range20">
                                                                Bachelor
                                                            </h3>
                                                        </div>
                                                        <div className="col-1">
                                                            <div className="">:</div>
                                                        </div>
                                                        <div className="col-7">
                                                            <h3 className="card__item__lists__profile-sub">
                                                                <span className="sub-heading ft-range18">
                                                                    {" "}
                                                                    {element.bachelor}
                                                                </span>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                    <div className="vertic-line" />
                                                    <div className="row m-0">
                                                        <div className="col-3 p-0 ">
                                                            <h3 className="card__item__lists__profile-sub ft-range20">
                                                                Major
                                                            </h3>
                                                        </div>
                                                        <div className="col-1">
                                                            <div className="">:</div>
                                                        </div>
                                                        <div className="col-7">
                                                            <h3 className="card__item__lists__profile-sub">
                                                                <span className="sub-heading ft-range18">
                                                                    {" "}
                                                                    {element.major}
                                                                </span>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                    <div className="vertic-line" />
                                                    <div className="row m-0">
                                                        <div className="col-3 p-0 ">
                                                            <h3 className="card__item__lists__profile-sub ft-range20">
                                                                Links
                                                            </h3>
                                                        </div>
                                                        <div className="col-1">
                                                            <div className="">:</div>
                                                        </div>
                                                        <div className="col-7 p-0">
                                                            <h3 className="card__item__lists__profile-sub">
                                                                <span className="sub-heading ft-range18">
                                                                    {element.links.map((links) => (
                                                                        <span key={links.toString() + key}>
                                                                            {links}
                                                                        </span>
                                                                    ))}
                                                                </span>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-12 col-12 col-md-6 col-lg-6 pe-0 pleft-zero ">
                                                    <div className="subtitle ">

                                                        <h3 className="card__item__lists__profile-sub ft-range20">
                                                            Core skills :
                                                        </h3>
                                                        <div className="card__item__lists__tags ">
                                                            {element.tags.map((tags) => (
                                                                <span
                                                                    className="tag-text"
                                                                    key={tags.toString() + key}
                                                                >
                                                                    {tags}
                                                                </span>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            );
        });
        {
            /* package  */
        }
        let PackageDetails = this.state.packageData.map((element, key) => {
            return (
                <>
                    <div className="col-lg-4 col-md-6 col-sm-6 col-12 pb-3 ">
                        <div className="profile-card">
                            <div className="card__item">
                                <div className="card__item__lists__card-height ">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <h3 className="card__item__lists__titel ft-range26">
                                                {element.title}
                                            </h3>
                                            <div className="vertic-line" />
                                        </div>
                                    </div>

                                    <div className="row m-0 mb-4">
                                        <div className="col-sm-12 ps-0">
                                            <div className="row m-0">
                                                <div className="col-5 p-0 ">
                                                    <h3 className="card__item__lists__profile-sub ft-range20">
                                                        Bachelor
                                                    </h3>
                                                </div>
                                                <div className="col-1">
                                                    <div className="">:</div>
                                                </div>
                                                <div className="col-5">
                                                    <h3 className="card__item__lists__profile-sub">
                                                        <span className="sub-heading ft-range18">
                                                            {element.bachelor}
                                                        </span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div className="vertic-line" />
                                            <div className="row m-0">
                                                <div className="col-5 p-0 ">
                                                    <h3 className="card__item__lists__profile-sub ft-range20">
                                                        Price
                                                    </h3>
                                                </div>
                                                <div className="col-1">
                                                    <div className="">:</div>
                                                </div>
                                                <div className="col-5">
                                                    <h3 className="card__item__lists__profile-sub">
                                                        <span className="sub-heading ft-range18">
                                                            {element.price}
                                                        </span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div className="vertic-line" />
                                            <div className="row m-0">
                                                <div className="col-5 p-0 ">
                                                    <h3 className="card__item__lists__profile-sub ft-range20">
                                                        Paid session per month
                                                    </h3>
                                                </div>
                                                <div className="col-1">
                                                    <div className="">:</div>
                                                </div>
                                                <div className="col-5">
                                                    <h3 className="card__item__lists__profile-sub">
                                                        <span className="sub-heading ft-range18">
                                                            {" "}
                                                            {element.month}
                                                        </span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div className="vertic-line" />
                                            <div className="row m-0">
                                                <div className="col-5 p-0 ">
                                                    <h3 className="card__item__lists__profile-sub ft-range20">
                                                        Type of service
                                                    </h3>
                                                </div>
                                                <div className="col-1">
                                                    <div className="">:</div>
                                                </div>
                                                <div className="col-5">
                                                    <h3 className="card__item__lists__profile-sub">
                                                        <span className="sub-heading ft-range18">
                                                            {element.service}
                                                        </span>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="profile-footer">
                                        <p
                                            className="package-pargh ft-range18"
                                            style={{ height: "50px" }}
                                        >
                                            {element.text}{" "}
                                            <p className="pt-1 ft-range18"> {element.text1}</p>
                                        </p>
                                        <div className="text-center mb-4">
                                            <div
                                                className="footer__cta d-inline-block w-50"
                                                onClick={(e) => this.handlePay(e)}
                                            >
                                                {element.button}
                                            </div>
                                        </div>
                                        {this.state.package && (
                                            <BookSessionPopup
                                                size="lg"
                                                dialogClassName={"package"}
                                                centered={false}
                                                package={this.state.package}
                                                onHide={() => this.setPay(false)}
                                                // onsetPackage={(ispackage) => this.setPay(ispackage)}
                                                closePackagePopup={() => this.closePackagePopup(false)}
                                            />
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            );
        });
        {
            /* working hours  */
        }
        let hoursDetails = this.state.hoursData.map((element, key) => {
            return (
                <>
                    <div className="row">
                        <div className=" col-12">
                            <div className="profile-card">
                                <div className="card__item">
                                    <div className="card__item__lists__card-height">
                                        <div className="row">
                                            <div className="col-sm-12 col-md-12 ">
                                                <h3 className="card__item__lists__titel mt-4">
                                                    {element.title}
                                                </h3>
                                                <div className="col-sm-8 col-md-8 col-lg-7 col-xl-6 col-xxl-5 col-10">
                                                    <div className="vertic-line" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-12 col-md-12 mb-3">
                                                <div className="row">
                                                    <div className=" col-sm-2 col-md-2 col-lg-1 col-xl-1 col-xxl-1">
                                                        <h3 className="card__item__lists__profile-sub mt-2">
                                                            Monday{" "}
                                                        </h3>
                                                    </div>
                                                    <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
                                                        <div className="card__item__lists__tags tag-space ">
                                                            {element.monday.map((monday) => (
                                                                <span key={monday.toString() + key}>
                                                                    {monday}
                                                                </span>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-8 col-md-8 col-lg-7 col-xl-6 col-xxl-5 col-10">
                                                    <div className="vertic-line" />
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-2 col-md-2 col-lg-1 col-xl-1 col-xxl-1">
                                                        <h3 className="card__item__lists__profile-sub mt-2">
                                                            Tuesday{" "}
                                                        </h3>
                                                    </div>
                                                    <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
                                                        <div className="card__item__lists__tags tag-space">
                                                            {element.tuesday.map((tuesday) => (
                                                                <span key={tuesday.toString() + key}>
                                                                    {tuesday}
                                                                </span>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-8 col-md-8 col-lg-7 col-xl-6 col-xxl-5 col-10">
                                                    <div className="vertic-line" />
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-2 col-md-2 col-lg-1 col-xl-1 col-xxl-1">
                                                        <h3 className="card__item__lists__profile-sub mt-2">
                                                            Wednesday{" "}
                                                        </h3>
                                                    </div>
                                                    <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
                                                        <div className="card__item__lists__tags tag-space">
                                                            {element.wednesday.map((wednesday) => (
                                                                <span key={wednesday.toString() + key}>
                                                                    {wednesday}
                                                                </span>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-8 col-md-8 col-lg-7 col-xl-6 col-xxl-5 col-10">
                                                    <div className="vertic-line" />
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-2 col-md-2 col-lg-1 col-xl-1 col-xxl-1">
                                                        <h3 className="card__item__lists__profile-sub mt-2">
                                                            Thursday{" "}
                                                        </h3>
                                                    </div>
                                                    <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
                                                        <div className="card__item__lists__tags tag-space">
                                                            {element.thursday.map((thursday) => (
                                                                <span key={thursday.toString() + key}>
                                                                    {thursday}
                                                                </span>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-8 col-md-8 col-lg-7 col-xl-6 col-xxl-5 col-10">
                                                    <div className="vertic-line" />
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-2 col-md-2 col-lg-1 col-xl-1 col-xxl-1">
                                                        <h3 className="card__item__lists__profile-sub mt-2">
                                                            Friday
                                                        </h3>
                                                    </div>
                                                    <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
                                                        <div className="card__item__lists__tags tag-space">
                                                            {element.friday.map((friday) => (
                                                                <span key={friday.toString() + key}>
                                                                    {friday}
                                                                </span>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-8 col-md-8 col-lg-7 col-xl-6 col-xxl-5 col-10">
                                                    <div className="vertic-line" />
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-2 col-md-2 col-lg-1 col-xl-1 col-xxl-1">
                                                        <h3 className="card__item__lists__profile-sub mt-2">
                                                            Saturday{" "}
                                                        </h3>
                                                    </div>
                                                    <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
                                                        <div className="card__item__lists__tags tag-space">
                                                            {element.saturday.map((saturday) => (
                                                                <span key={saturday.toString() + key}>
                                                                    {saturday}
                                                                </span>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-sm-8 col-md-8 col-lg-7 col-xl-6 col-xxl-5 col-10">
                                                    <div className="vertic-line" />
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-2 col-md-2 col-lg-1 col-xl-1 col-xxl-1">
                                                        <h3 className="card__item__lists__profile-sub mt-2">
                                                            Sunday{" "}
                                                        </h3>
                                                    </div>
                                                    <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
                                                        <div className="card__item__lists__tags tag-space">
                                                            {element.sunday.map((sunday) => (
                                                                <span key={sunday.toString() + key}>
                                                                    {sunday}
                                                                </span>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            );
        });
        let reviewDetails = this.state.reviewData.map((element, key) => {
            return (
                <>
                    <div className="profile-student-say">
                        <div className="row">
                            <div className="col-sm-12">
                                <h3 className="card__item__lists__titel mt-4">
                                    {element.title}
                                </h3>
                                <div className="vertic-line" />
                            </div>
                        </div>
                        <div className="row m-0 mb-4">
                            <div className=" col-sm-7 col-md-7 col-lg-8 col-xl-8 col-xxl-7 ps-0">
                                <div className="row m-0 mb-3">
                                    <h3 className="card__item__lists__profile-sub">
                                        <div className="bar-size">
                                            <div className="col-5 col-sm-3 col-md-3 col-lg-2 col-xl-2 col-xxl-1 p-0 hours-text ms-4">5 stars</div>

                                            <div className="col-7 col-sm-9 col-md-9 col-lg-10 col-xl-10 col-xxl-10 mt-2">
                                                <ProgressBar now={element.stars5} />
                                            </div>
                                        </div>
                                    </h3>
                                </div>
                                <div className="row m-0 mb-3">
                                    <h3 className="card__item__lists__profile-sub">
                                        <div className="bar-size">
                                            <div className="col-5 col-sm-3 col-md-3 col-lg-2 col-xl-2 col-xxl-1 p-0 hours-text ms-4">4 stars</div>

                                            <div className="col-7 col-sm-9 col-md-9 col-lg-10 col-xl-10 col-xxl-10 mt-2">
                                                <ProgressBar now={element.stars4} />
                                            </div>
                                        </div>
                                    </h3>
                                </div>
                                <div className="row m-0 mb-3">
                                    <h3 className="card__item__lists__profile-sub">
                                        <div className="bar-size">
                                            <div className="col-5 col-sm-3 col-md-3 col-lg-2 col-xl-2 col-xxl-1 p-0 hours-text ms-4">3 stars</div>

                                            <div className="col-7 col-sm-9 col-md-9 col-lg-10 col-xl-10 col-xxl-10 mt-2">
                                                <ProgressBar now={element.stars3} />
                                            </div>
                                        </div>
                                    </h3>
                                </div>
                                <div className="row m-0 mb-3">
                                    <h3 className="card__item__lists__profile-sub">
                                        <div className="bar-size">
                                            <div className="col-5 col-sm-3 col-md-3 col-lg-2 col-xl-2 col-xxl-1 p-0 hours-text ms-4">2 stars</div>

                                            <div className="col-7 col-sm-9 col-md-9 col-lg-10 col-xl-10 col-xxl-10 mt-2">
                                                <ProgressBar now={element.stars2} />
                                            </div>
                                        </div>
                                    </h3>
                                </div>
                                <div className="row m-0 mb-2">
                                    <h3 className="card__item__lists__profile-sub">
                                        <div className="bar-size">
                                            <div className="col-5 col-sm-3 col-md-3 col-lg-2 col-xl-2 col-xxl-1 p-0 hours-text ms-4">1 stars</div>

                                            <div className="col-7 col-sm-9 col-md-9 col-lg-10 col-xl-10 col-xxl-10 mt-2 ">
                                                <ProgressBar now={element.stars1} />
                                            </div>
                                        </div>
                                    </h3>
                                </div>
                            </div>
                            <div className=" col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xxl-1 d-flex">
                                <div className="horizon-line"></div>
                            </div>
                            <div className="col-sm-4 col-md-4 col-lg-3 col-xl-3 col-xxl-4">
                                <div className="Feedbackboxsec">
                                    <div className="feedtoplayer">
                                        <h2 class="card__item__lists__titel review-font">4.0</h2>
                                    </div>
                                    <div className="feedmidlayer">
                                        <div className="rating-side">
                                            <Rating isRating={true} RatingValue={4} />
                                            <div className="ratingsub">
                                            <p className="profile-review">11 review</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="feedbottomlayer">
                                        <div className="feebackbtn">
                                            <a // onClick={this.handleClick}
                                                onMouseOut={this.handleMouseOut}
                                                onClick={(e) => this.showFeedBack(e)}
                                                className="text-black">
                                                Give Feedback
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                
                                {feedBack && (
                                        <FeedBackPopup
                                            size={"lg"}
                                            dialogClassName=""
                                            fullscreen={true}
                                            centered={false}
                                            showFeedBack={this.state.feedBack}
                                            onHide={() => this.setFeedBack(false)}
                                            onsetFeedBack={(isfeedBack) => this.setFeedBack(isfeedBack)}
                                        />

                                    )}
                                    {this.state.hover && (
                                        <div className="feedback-toolip ms-5">
                                            <svg
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M12 1.5C6.20156 1.5 1.5 6.20156 1.5 12C1.5 17.7984 6.20156 22.5 12 22.5C17.7984 22.5 22.5 17.7984 22.5 12C22.5 6.20156 17.7984 1.5 12 1.5ZM12.75 17.0625C12.75 17.1656 12.6656 17.25 12.5625 17.25H11.4375C11.3344 17.25 11.25 17.1656 11.25 17.0625V10.6875C11.25 10.5844 11.3344 10.5 11.4375 10.5H12.5625C12.6656 10.5 12.75 10.5844 12.75 10.6875V17.0625ZM12 9C11.7056 8.99399 11.4253 8.87282 11.2192 8.6625C11.0132 8.45218 10.8977 8.16945 10.8977 7.875C10.8977 7.58055 11.0132 7.29782 11.2192 7.0875C11.4253 6.87718 11.7056 6.75601 12 6.75C12.2944 6.75601 12.5747 6.87718 12.7808 7.0875C12.9868 7.29782 13.1023 7.58055 13.1023 7.875C13.1023 8.16945 12.9868 8.45218 12.7808 8.6625C12.5747 8.87282 12.2944 8.99399 12 9Z"
                                                    fill="white"
                                                />
                                            </svg>{" "}
                                            You already gave feedBack
                                        </div>
                                    )}
                            </div>
                        </div>
                    </div>
                </>
            );
        });
        //student says
        let studentDetails = this.state.studentsSaysData.map((element, key) => {
            return (
                <>
                    <div className="vertic-line" />
                    <div className="profil-reviews">
                        <div className="student">
                            <div className="row m-0">
                                <div className="col-12 d-flex">
                                    <div className="Avatar-icon">
                                        <img src={element.icon} alt={element.AvatarIconAlt} />
                                    </div>
                                    <div className="student">
                                        <div className="d-flex">
                                            <h2 className="student__title review-title me-2 ms-2 mt-1">
                                                {element.name} </h2>
                                            <div className="img">
                                                <svg
                                                    width="15"
                                                    height="14"
                                                    viewBox="0 0 12 11"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <g clipPath="url(#clip0_13_14303)">
                                                        <path
                                                            d="M11.0673 4.27371C11.161 4.56607 11.0843 4.881 10.867 5.09558L8.96704 6.97182C8.92975 7.00868 8.91273 7.06175 8.92152 7.11377L9.37004 9.76309C9.42135 10.066 9.30072 10.3665 9.05523 10.5472C8.91655 10.6494 8.75422 10.7013 8.59086 10.7013C8.46508 10.7013 8.33868 10.6705 8.22186 10.6083L5.87338 9.35749C5.82723 9.33291 5.77218 9.33291 5.72605 9.35749L3.37759 10.6083C3.10904 10.7513 2.78964 10.728 2.54415 10.5473C2.29867 10.3666 2.17806 10.0661 2.22934 9.76311L2.67786 7.11379C2.68668 7.06177 2.66964 7.0087 2.63235 6.97185L0.732388 5.09558C0.515075 4.881 0.438341 4.56607 0.532138 4.27371C0.625935 3.98134 0.870692 3.77227 1.17097 3.72808L3.79663 3.34157C3.84819 3.33397 3.89272 3.30118 3.91581 3.25386L5.09005 0.843404C5.22433 0.56773 5.49625 0.396484 5.79969 0.396484C6.10314 0.396484 6.37506 0.56773 6.50933 0.843404L7.68358 3.25383C7.70664 3.30118 7.75119 3.33397 7.80273 3.34154L10.4284 3.72808C10.7287 3.77227 10.9735 3.98134 11.0673 4.27371Z"
                                                            fill={"#E64A19"}
                                                            stroke={"#E64A19"}
                                                            strokeWidth={"1.5"}
                                                        />
                                                    </g>
                                                    <defs>
                                                        <clipPath id="clip0_13_14303">
                                                            <rect
                                                                width="10.8"
                                                                height="11"
                                                                fill="white"
                                                                transform="translate(0.400024)"
                                                            />
                                                        </clipPath>
                                                    </defs>
                                                </svg>
                                            </div>
                                            <div className="rate ms-1">
                                                <span className="sub-heading review-rating">
                                                    {" "}
                                                    {element.rating}{" "}
                                                </span>
                                            </div>
                                        </div>
                                        <p className="student__sub-title">
                                            {element.date}
                                            <svg
                                            className="ms-1 me-1"
                                                width="5"
                                                height="5"
                                                viewBox="0 0 5 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <circle cx="2.5" cy="2.5" r="2.5" fill="#5C5C5C" />
                                            </svg>
                                            {element.course}
                                        </p>
                                        <p class="student__List ">{element.text}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            );
        });
        return (
            <>
                <div className="network-profile">
                    <div className="profile-top">
                        <div className="">{ProfileList}</div>
                    </div>
                    <div className="wrap-space">
                        <div className="p-2">
                            {this.state.profileData && this.state.profileData.length > 0 && (
                                <div>{ProfileDetails}</div>
                            )}
                            {this.state.packageData && this.state.packageData.length > 0 && (
                                <div className="row">{PackageDetails}</div>
                            )}
                            {this.state.hoursData && this.state.hoursData.length > 0 && (
                                <div>{hoursDetails}</div>
                            )}
                            <div className="row">
                                <div className=" col-12">
                                    <div className="profile-card">
                                        <div className="card__item">
                                            <div className="card__item__lists__card-height">
                                                {reviewDetails}
                                                {this.state.studentsSaysData &&
                                                    this.state.studentsSaysData.length > 0 && (
                                                        <div>{studentDetails}</div>
                                                    )}
                                                <div className="review-bottom">
                                                    <div
                                                        className="footer__cta d-inline-block  mb-5 
 
                             cursorshow"
                                                        onClick={(e) => this.showReview(e)}
                                                    >
                                                        More reviews
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {review && (
                        <ReviewPopup
                            size="lg"
                            dialogClassName={"MoreReview"}
                            fullscreen={true}
                            centered={true}
                            showReview={this.state.review}
                            onHide={() => this.setReview(false)}
                            reviewDatas={this.state.studentsSaysData}
                            onsetReview={(isreview) => this.setReview(isreview)}
                            isDocumentPage={true}
                        />
                    )}
                </div>
            </>
        );
    }
}

export default Index;
