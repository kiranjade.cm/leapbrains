import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import { Link } from 'react-router-dom';
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSuccess: this.props.show,
            isError: this.props.error
        };
    }

    backtoprofile(e) {
        this.props.onsetShow(false)
    }
    backtoProfileFail(e) {
        this.props.onsetPackage(false)
    }

    render() {
        let { popupData, errorData } = this.props;
        let { isSuccess, isError } = this.state;
        return (
            <>
                <div className="success">
                    {isSuccess &&
                        <Modal show={this.props.show} onHide={this.props.onHide}>
                            <Modal.Header closeButton>

                            </Modal.Header>
                            <Modal.Body>
                                <h5 className="cta--center">{popupData[0].bodyTitle} </h5>
                                <div className="cta--center">
                                    <svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0_1713_75552)">
                                            <path d="M77.2125 27.2125C77.5031 26.922 77.7336 26.5771 77.8908 26.1974C78.0481 25.8178 78.129 25.4109 78.129 25C78.129 24.5891 78.0481 24.1823 77.8908 23.8026C77.7336 23.423 77.5031 23.0781 77.2125 22.7875C76.922 22.497 76.5771 22.2665 76.1974 22.1093C75.8178 21.952 75.4109 21.8711 75 21.8711C74.5891 21.8711 74.1823 21.952 73.8026 22.1093C73.423 22.2665 73.0781 22.497 72.7875 22.7875L31.25 64.3313L11.5875 44.6625C11.297 44.372 10.9521 44.1415 10.5724 43.9843C10.1928 43.827 9.78594 43.7461 9.37504 43.7461C8.54519 43.7461 7.74933 44.0757 7.16254 44.6625C6.57575 45.2493 6.24609 46.0452 6.24609 46.875C6.24609 47.7049 6.57575 48.5007 7.16254 49.0875L29.0375 70.9625C29.3278 71.2536 29.6727 71.4845 30.0523 71.642C30.432 71.7995 30.839 71.8806 31.25 71.8806C31.6611 71.8806 32.0681 71.7995 32.4478 71.642C32.8274 71.4845 33.1723 71.2536 33.4625 70.9625L77.2125 27.2125V27.2125ZM50.9125 70.9625L45.3125 65.3563L49.7313 60.9375L53.125 64.3313L94.6625 22.7875C95.2493 22.2007 96.0452 21.8711 96.875 21.8711C97.7049 21.8711 98.5007 22.2007 99.0875 22.7875C99.6743 23.3743 100.004 24.1702 100.004 25C100.004 25.8299 99.6743 26.6258 99.0875 27.2125L55.3375 70.9625C55.0473 71.2536 54.7024 71.4845 54.3228 71.642C53.9431 71.7995 53.5361 71.8806 53.125 71.8806C52.714 71.8806 52.307 71.7995 51.9273 71.642C51.5477 71.4845 51.2028 71.2536 50.9125 70.9625V70.9625Z" fill="#34C38F" />
                                            <path d="M33.4625 44.6625L39.0625 50.2688L34.6438 54.6875L29.0375 49.0875C28.4508 48.5008 28.1211 47.7049 28.1211 46.875C28.1211 46.0452 28.4508 45.2493 29.0375 44.6625C29.6243 44.0758 30.4202 43.7461 31.25 43.7461C32.0799 43.7461 32.8758 44.0758 33.4625 44.6625V44.6625Z" fill="#34C38F" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0_1713_75552">
                                                <rect width="100" height="100" fill="white" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </div>


                            </Modal.Body>
                            <Modal.Footer>
                                <Link to="/student/profile">
                                    <h3 className="GetAdvisors__title" onClick={(e) => this.backtoprofile(e)}>

                                        {popupData[0].footerTitle}</h3>
                                </Link>

                                <Link to="/student/schedule" className="footer__cta d-inline-block ">
                                    {popupData[0].footerButton}</Link>
                            </Modal.Footer>
                        </Modal>
                    }
                </div>
                <div className="error">
                    {isError &&
                        <Modal show={this.props.error} onHide={this.props.onClose}>
                            <Modal.Header closeButton>

                            </Modal.Header>
                            <Modal.Body>
                                <div className="error-popup">
                                <h5 className="text-center error-popup__text  mb-4 mt-5">{errorData[0].bodyTitle} </h5>
                                </div>
                                <div className="text-center mb-5">
                                    <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0_1713_75558)">
                                            <path d="M25 46.875C19.1984 46.875 13.6344 44.5703 9.53204 40.468C5.42968 36.3656 3.125 30.8016 3.125 25C3.125 19.1984 5.42968 13.6344 9.53204 9.53204C13.6344 5.42968 19.1984 3.125 25 3.125C30.8016 3.125 36.3656 5.42968 40.468 9.53204C44.5703 13.6344 46.875 19.1984 46.875 25C46.875 30.8016 44.5703 36.3656 40.468 40.468C36.3656 44.5703 30.8016 46.875 25 46.875ZM25 50C31.6304 50 37.9893 47.3661 42.6777 42.6777C47.3661 37.9893 50 31.6304 50 25C50 18.3696 47.3661 12.0107 42.6777 7.32233C37.9893 2.63392 31.6304 0 25 0C18.3696 0 12.0107 2.63392 7.32233 7.32233C2.63392 12.0107 0 18.3696 0 25C0 31.6304 2.63392 37.9893 7.32233 42.6777C12.0107 47.3661 18.3696 50 25 50Z" fill="#FA5F1C" />
                                            <path d="M21.8809 34.375C21.8809 33.9646 21.9617 33.5583 22.1187 33.1791C22.2758 32.8 22.506 32.4555 22.7962 32.1653C23.0863 31.8751 23.4308 31.6449 23.81 31.4879C24.1891 31.3308 24.5955 31.25 25.0059 31.25C25.4162 31.25 25.8226 31.3308 26.2017 31.4879C26.5809 31.6449 26.9254 31.8751 27.2156 32.1653C27.5058 32.4555 27.7359 32.8 27.893 33.1791C28.05 33.5583 28.1309 33.9646 28.1309 34.375C28.1309 35.2038 27.8016 35.9987 27.2156 36.5847C26.6295 37.1708 25.8347 37.5 25.0059 37.5C24.1771 37.5 23.3822 37.1708 22.7962 36.5847C22.2101 35.9987 21.8809 35.2038 21.8809 34.375ZM22.1871 15.6094C22.1455 15.2151 22.1872 14.8165 22.3096 14.4395C22.432 14.0624 22.6323 13.7153 22.8975 13.4206C23.1627 13.1259 23.4869 12.8903 23.849 12.729C24.2112 12.5677 24.6032 12.4844 24.9996 12.4844C25.396 12.4844 25.7881 12.5677 26.1502 12.729C26.5123 12.8903 26.8365 13.1259 27.1017 13.4206C27.3669 13.7153 27.5672 14.0624 27.6896 14.4395C27.812 14.8165 27.8537 15.2151 27.8121 15.6094L26.7184 26.5687C26.6816 26.9993 26.4846 27.4004 26.1663 27.6926C25.8481 27.9849 25.4317 28.147 24.9996 28.147C24.5675 28.147 24.1511 27.9849 23.8329 27.6926C23.5146 27.4004 23.3176 26.9993 23.2809 26.5687L22.1871 15.6094Z" fill="#FA5F1C" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0_1713_75558">
                                                <rect width="50" height="50" fill="white" />
                                            </clipPath>
                                        </defs>
                                    </svg>

                                </div>
                                <div className="text-center  mb-5">
                                    
                                    
                                    <a className="footer__cta d-inline-block    " onClick={(e) => this.backtoProfileFail(e)}>
                                        {errorData[0].footerButton}</a>
                                </div>

                            </Modal.Body>
                            <Modal.Footer>



                            </Modal.Footer>
                        </Modal>
                    }
                </div>
            </>
        );
    }
}
export default Index;
