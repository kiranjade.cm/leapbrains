import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import StarRatings from 'react-star-ratings';

import SuccessPopup from "../SuccessPopup"
import Select, { components } from "react-select";



class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating: 0,
            show: false,
            textCount:0,
            textCountError:"",
            messageText:"",
            optionSelected:""




        };
    }
    changeRating(newRating) {
        this.setState({
            rating: newRating
        });

    }
    handleChange = selected => {
        this.setState({
          optionSelected: selected
        });
    };

    handleShow(e) {
        e.preventDefault()
        this.setState({ show: true })
        
    }
    setShow(isshow) {

        this.setState({ show: isshow })
    }

    render() {
        const FeedBackData = [
            { value: 'General', label: 'General' },
            { value: 'Plan', label: 'Plan: Going to Cambridge' },
            { value: 'Course', label: 'Course: Algebra' },
            { value: 'Course1', label: 'Course: Computer Science' },
            { value: 'Activity', label: 'Activity: Piano' }
        ];
        const popupData = [
            {
                'bodyTitle': 'Feedback is placed successfully',
                'footerTitle': 'Return to Profile',
                'footerButton': 'Go to Calendar'

            }
        ];
        let { show ,textCount,textCountError,messageText} = this.state;
        const { ValueContainer, Placeholder } = components;
        const CustomValueContainer = ({ children, ...props }) => {
            return (
                <ValueContainer {...props}>
                    <Placeholder {...props} isFocused={props.isFocused}>
                        {props.selectProps.placeholder}
                    </Placeholder>
                    {React.Children.map(children, child =>
                        child && child.type !== Placeholder ? child : null
                    )}
                </ValueContainer>
            );
        };

        const Option = (props) => {
            return (
                <div>
                <components.Option {...props}>
                    <input
                    type="radio"
                    checked={props.isSelected}
                    onChange={() => null}  
                    name={"radioOption"}
                    />
                    <label>{props.label}</label>
                </components.Option>
                </div>
            );
        };

        return (

            <>

                <Modal show={this.props.showFeedBack} fullscreen={this.props.fullscreen} onHide={this.props.onHide}>
                    <Modal.Header closeButton>

                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="login_title text-center">Give feedback about the advisor</h1>
                        <p className="feeback-sub text-center">Tell us about your experience working with this advisor. </p>
                        <div className="feedrating col-sm-12 col-lg-12 col-xl-12 text-center">
                            <StarRatings
                                rating={this.state.rating}
                                starRatedColor="EF6767"
                                changeRating={(newRating) => this.changeRating(newRating)}
                                numberOfStars={5}
                                name='small-rating'
                                size="small"
                            />
                        </div>
                        <div className="row m-0">
                            <div className="col-md-12 col-sm-12 col-12 p-0">
                                <div className="feed-select">
                                <div className="role">
                                    <div className="role_card">
                                        <div className="goals_page">
                                          
                                            <div class="input-floating-label role__btns " >
                                            <Select
                                className="radio__select mb-3"
                                classNamePrefix="mySelect"
                                options={FeedBackData}
                                isClearable={false}
                                
                                components={{
                                    ValueContainer: CustomValueContainer,
                                  
                                    IndicatorSeparator: () => null,
                                    Option
                                }}
                                placeholder=""
                                styles={{
                                    container: (provided, state) => ({
                                        ...provided,
                                        height: '48px',
                                        overflow: "visible"
                                    }),
                                    valueContainer: (provided, state) => ({
                                        ...provided,
                                        overflow: "visible",
                                        height: '100%',
                                        minHeight: '48px',
                                    }),
                                    placeholder: (provided, state) => ({
                                        ...provided,
                                        position: "absolute",
                                        top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                        fontSize: (state.hasValue || state.selectProps.inputValue) && 13,
                                        background: '#fff',
                                        paddingLeft:10,
                                        paddingRight:10
                                    })
                                }}
 
                                closeMenuOnSelect={false}
                                hideSelectedOptions={false}
                                onChange={this.handleChange}
                                value={this.state.optionSelected}
                            />

                                            </div>
                                           
                                            
                                            
                            </div>

                                        
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className="col-md-12 col-sm-12 col-12 p-0">
                                <div className="role">
                                    <div className="role_card">
                                        <div className="goals_page">
                                          
                                        <div class="input-floating-label">
                                <textarea
                                    className= {"textarea--primary textbox--rounded input"}
                                    name="Message"
                                    placeholder="Message"
                                    value={messageText}
                                    style={{"height":"200px"}}
                                    onChange={(e) => {
                                        if (e.target.value.length <= 5000) {
                                            textCount = e.target.value.length;
                                            messageText=e.target.value
                                            this.setState({textCount,messageText,textCountError:""})
                                        } else {
                                            this.setState({textCountError:"Message only allow 5000 Characters"})
                                        }
                                    }}
                                />
                               
                                <label >Message</label>
                                <p><span>{textCount}</span>/5000</p>
                                <p className="error-text">{textCountError}</p>
                            </div> 
                            </div>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="footer__cta cta--rounded" onClick={(e) => this.handleShow(e)}>
                            Send
                        </div>
                    </Modal.Body>
                    <Modal.Footer>

                    </Modal.Footer>
                </Modal>
                {show &&
                    <SuccessPopup
                        size="lg"
                        dialogClassName=""
                        centered={false}
                        show={this.state.show}
                        onHide={() => this.setShow(false)}
                        popupData={popupData}
                        onsetShow={(isshow) => this.setShow(isshow)}
                    />

                }

            </>

        );

    }
}
export default Index;
