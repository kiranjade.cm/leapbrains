import React, { Component } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from 'react-router-dom'
import { Button } from "react-bootstrap";
import Explore from "../Explore"
import AddNewCards from "../AddCards"
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isExplore: false,
            isFriends: true
        }
    }

    handleShow(e) {
        e.preventDefault()
        this.setState({ isExplore: true, isFriends: false })
        this.props.showFriendNetwork(e, false)
    }
    handleBack(e) {
        e.preventDefault();
        this.setState({ isExplore: false, isFriends: true })
        this.props.showFriendNetwork(e, true)
    }
    render() {
        const AddNewData = [

        ];
        let { isExplore, isFriends } = this.state
        return (
            <>
             
                {isFriends &&
                <div className="wrap-space">
                    <div className="advisor-friends">
                        <div className="row">
                            <div className="col-sm-8 col-6">
                             <h5 className="advisor-top">My Friends</h5> 
                            </div>
                            <div className="col-sm-4 col-6 text-rights advisor-button">
                            <Button className="btn cta--rounded cta-primary cursorshow" >
                                        <span>Add new friends</span>
                                    </Button>
                            </div>
                        </div>
                        
                        {AddNewData && AddNewData.length > 0 ?
                            <AddNewCards
                                AddNewData={AddNewData}
                            /> :
                            <div className="tab-content">
                                <div className="box-network">
                                    <div className="boxnetwork">
                                    </div>
                                </div>
                                <h6 className="sub-title">You do not have any Friends yet. Do you want to 
                                    <u>
                                        <span className="cursorshow" onClick={(e) => this.handleShow(e)} >
                                             Explore them
                                        </span>

                                    </u> ?
                                </h6>
                            </div>
                            
                        }

                    </div>
                    </div>
                }
                
                {isExplore &&
                    <div className="e">
                        <Explore
                            title="Friends"
                            handleBack={(e) => this.handleBack(e)}
                        />
                    </div>
                }
            </>
        );
    }
}
export default Index;