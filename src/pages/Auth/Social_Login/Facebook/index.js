import React, { Component } from "react";
import ReactFacebookLogin from "react-facebook-login";

class Index extends Component {
    constructor() {
        super();
        this.state = {
        };
    }

    onResponse = (response) => {
        console.log("FbSucess", response)
    }
    onFailures = (response) => {
        console.log("FbFail", response)
    }

    render() {

        return (
            <>
                <ReactFacebookLogin className="fbbtn" icon="fa-facebook" textButton=""
                    appId={process.env.REACT_APP_FACEBOOK_CLIENT_ID} reauthenticate={true} authLoad={true}
                    autoLoad={false} scope="public_profile,email" fields="name,email,picture"
                    callback={this.onResponse} onFailure={this.onFailures}
                />
            </>
        );
    }
}



export default (Index);