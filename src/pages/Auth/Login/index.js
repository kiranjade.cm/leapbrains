import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../../../assets/css/login.less";
import { isUserAuthenticated } from "../../../utils/AuthUtils";
import { userSignIn, getUserProfile, verifyEmail } from "../../../redux/actions/Auth";
import { connect } from "react-redux";
import { withRouter } from "../../../redux/store/navigate";
import { NotificationContainer, NotificationManager } from 'react-notifications';
import Google from "../Social_Login/Google"
import Facebook from "../Social_Login/Facebook"
import { forgetPassword, verifyPassword } from "../../../redux/actions/Common";
import CustomAlert from "../../../components/Alert";
import Alert from 'react-bootstrap/Alert';
import { CUSTOM_ALERT_CLOSE } from "../../../redux/constants/CommonTypes";
const IDENTIFIER = process.env.REACT_APP_IDENTIFIER;
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            fields: {},
            errors: {},
            isOnboarded: false,
            errorList: [],
            buttonstatus: false,
        };
        this.handleValidation = this.handleValidation.bind(this)
    }
    handleChange(field, e) {
        let { errors } = this.state;
        let fields = this.state.fields;
        fields[field] = e.target.value;
        if (e.target.value.length >= 0) {
            errors[field] = "";
        }
        this.setState({ fields, errors: errors });
    }
    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (typeof fields["email"] !== "undefined") {
            let lastAtPos = fields["email"].lastIndexOf("@");
            let lastDotPos = fields["email"].lastIndexOf(".");
            if (
                !(
                    lastAtPos < lastDotPos &&
                    lastAtPos > 0 &&
                    fields["email"].indexOf("@@") == -1 &&
                    lastDotPos > 2 &&
                    fields["email"].length - lastDotPos > 2
                )
            ) {
                formIsValid = false;
                errors["email"] = "Not a valid email";
            }
        }
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Email cannot be empty";
        }
        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Password cannot be empty";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    handleSubmit(e) {
        e.preventDefault();
        let fields = this.state.fields;
        if (this.handleValidation()) {
            let values = {
                username: fields["email"],
                password: fields["password"],
                identifier: IDENTIFIER
            }
            this.props.userSignIn(values)
        }
    };
    componentDidMount() {
        const leapBrainsWebApp = localStorage.getItem('leapBrainsWebApp')
        if (leapBrainsWebApp != null) {
            localStorage.clear()
            window.location.reload()
        }
        this.setState({ errorList: [] })
        if (this.props.isNewPassword) {
            this.setState({ sentpassword: true })
        }
        if (this.props.isverifyPasswordstatus == false) {
            this.setState({ passwordurlerror: true })
        }
        else if (this.props.isverifyPasswordstatus === true) {
            this.setState({ passwordurlerror: false })
        }
        if(this.props.isEmailConfirm == false ) {
            this.setState({ verifyemailerror: true })
        }
        if(this.props.isEmailVerified == true ) {
            this.setState({ verifyemailsuccess: true })
        }
    }
    componentDidUpdate(e) {
        if (this.props.userProfile.isOnboarded !== undefined && this.props.userProfile.isOnboarded) {
            this.props.navigate(`/${this.props.userProfile.currentRole}/dashboard`);
        }
        else if (this.props.userProfile.isOnboarded !== undefined && !this.props.userProfile.isOnboarded) {
            this.props.navigate('/onboarding/roles');
        }
    }
    componentWillReceiveProps(newProps) {
        this.setState({
            errorList: newProps.errorList,
        });
    }
    forget = () => {
        this.props.forgetPasswordstatus(false)
    }
    render() {
        let { errorList,buttonstatus } = this.state;
        const message = this.props.message;
        return (
            <>
                <div className="login">
                    <CustomAlert  />
                    <div className="login_card">
                        <h1 className="login_title text-center">Log In</h1>
                        <ul className="text-center login_social-icon">
                            <li> <div className="pointer"><div><Google /></div></div> </li>
                            <li> <div className="pointer"> <div><Facebook /></div></div> </li>
                        </ul>
                        <p className="or"><span>or</span></p>
                        <Form onSubmit={this.handleSubmit.bind(this)} className="login_card_form mb-5" autoComplete="off">
                            <div className="input-floating-label">
                                <input refs="email" type="email" name="email" placeholder=" "
                                    className={this.state.errors["email"] ? "textbox--primary textbox--rounded input w-100 error-input" : "textbox--primary textbox--rounded input"}
                                    onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} />
                                <label >Email</label>
                                {errorList && errorList.email && <span className="error-text">{errorList.email}</span>}
                                {this.state.errors["email"] && <span className="error-text">{this.state.errors["email"]}</span>}
                            </div>
                            <div className="input-floating-label">
                                <input refs="password" type="password" name="password" placeholder=" "
                                    className={this.state.errors["password"] ? "textbox--primary textbox--rounded input w-100 error-input" : "textbox--primary textbox--rounded input"}
                                    onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} />
                                <label>Password</label>
                                {errorList && errorList.password && <span className="error-text">{errorList.password}</span>}
                                {this.state.errors["password"] && <span className="error-text">{this.state.errors["password"]}</span>}
                            </div>
                            <p className="rememberlogin">
                                <input className="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" />
                                <label htmlFor="styled-checkbox-2"><span>Keep me logged in</span></label>
                            </p>
                            <Button type="submit" disabled={buttonstatus} className="btn cta--rounded cta-primary w-100"><span>Continue</span></Button>
                        </Form>
                        <p className="cta cta--center"><Link className="login_text" to="/forgotpassword" onClick={() => {this.props.forgetPasswordstatus(false);this.props.customAlertClose()}} >Forgot password?</Link></p>
                        <p className="cta cta--center login_text">Don’t have an account? <Link className="cta--underline" to="/signup" onClick={()=>this.props.customAlertClose()}>Create one</Link></p>
                    </div>
                </div>
                <NotificationContainer />
            </>
        );
    }
}

const mapStateToProps = ({ auth, commonData }) => {
    const { message, errorList, isNewPassword, isverifyPasswordstatus } = commonData
    const { userProfile, isLoginError, isEmailVerified,isEmailConfirm } = auth;
    return { message, errorList, userProfile, isLoginError, isEmailVerified, isNewPassword, isverifyPasswordstatus,isEmailConfirm }
};
const mapDispatchToProps = (dispatch) => {
    return({
        customalertclose: () => {dispatch({type:CUSTOM_ALERT_CLOSE})}
    })
}

export default connect(mapStateToProps, { userSignIn, getUserProfile, verifyEmail, forgetPassword, verifyPassword })(withRouter(Index));