import React, { Component } from "react";
import "../../../../assets/css/profile.less";
import { Link } from "react-router-dom";
import Select, { components } from "react-select";
import { connect } from "react-redux";
import { getGender, getStatus, getCountry } from "../../../../redux/actions/Common";
import { setUserProfile, getUserProfile, setIsGoalSuccess } from "../../../../redux/actions/Auth";
import { NotificationContainer } from 'react-notifications';
import { withRouter } from "../../../../redux/store/navigate";
import { Form } from "react-bootstrap";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            fields: {},
            errors: {},
            selectedGenderOption: null,
            selectedStatusOption: null,
            selectedCountryOption: null,
            studentsCollabCheckBox: false,
            counselingAllowedCheckBox: false,
            subCouncelingServicesCheckBox: false,
            selectGraduationOption: null,
            selectSchoolOption: null,
            graduationyearlist: [
                { label: 2022, value: 2022 },
                { label: 2023, value: 2023 },
                { label: 2024, value: 2024 },
                { label: 2025, value: 2025 },
                { label: 2026, value: 2026 },
                { label: 2027, value: 2027 },
                { label: 2028, value: 2028 },
                { label: 2029, value: 2029 },
                { label: 2030, value: 2030 },
                { label: 2031, value: 2031 },
                { label: 2032, value: 2032 },
            ],
            highschoollist: [
                { label: "Public High Schools", value: "Public High Schools" },
                { label: "Charter High Schools", value: "Charter High Schools" },
                { label: "Magnet High Schools", value: "Magnet High Schools" },
                { label: "Carnegie Vanguard High School", value: "Carnegie Vanguard High School" },
                { label: "Gilbert Classical Academy", value: "Gilbert Classical Academy" },
            ],
        };
    }

    handleChange(field, e) {
        let { errors } = this.state;
        let fields = this.state.fields;
        fields[field] = e.target.value;
        if (e.target.value.length >= 0) {
            errors[field] = "";
        }
        this.setState({ fields, errors: errors });
    }

    handleCheckboxChange(e, buttonName) {
        let { studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox } = this.state;
        if (buttonName === "studentsCollab") {
            studentsCollabCheckBox = e.target.checked
        } else if (buttonName === "counselingAllowed") {
            counselingAllowedCheckBox = e.target.checked
        } else if (buttonName === "subCouncelingServices") {
            subCouncelingServicesCheckBox = e.target.checked
        }
        this.setState({ studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox })
    }

    handleSelectChange(options, name) {
        let { selectedGenderOption, selectedStatusOption, selectedCountryOption, selectGraduationOption, selectSchoolOption, errors } = this.state;
        switch (name) {
            case "gender":
                selectedGenderOption = options;
                errors["gender"] = "";
                break;
            case "status":
                selectedStatusOption = options;
                errors["status"] = "";
                break;
            case "country":
                selectedCountryOption = options;
                errors["country"] = "";
                break;
            case "graduationYear":
                selectGraduationOption = options;
                errors["graduationYear"] = "";
                break;
            case "highSchoolName":
                selectSchoolOption = options;
                errors["highSchoolName"] = "";
                break;
        }
        this.setState({ selectedGenderOption, selectedStatusOption, selectedCountryOption, selectGraduationOption, selectSchoolOption, errors: errors });
    }

    handleValidation() {
        let { selectedGenderOption, selectedStatusOption, selectedCountryOption, selectSchoolOption, selectGraduationOption } = this.state;
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if (!fields["firstName"]) {
            formIsValid = false;
            errors["firstName"] = "First name cannot be empty";
        }
        if (typeof fields["firstName"] !== "undefined") {
            const textcaseRegExp = /^[A-Za-z]+$/;
            const floatcasenumber = textcaseRegExp.test(fields["firstName"]);
            if (!floatcasenumber) {
                formIsValid = false;
                errors["firstName"] = "FirstName should be Text";
            }
        }
        if (!fields["lastName"]) {
            formIsValid = false;
            errors["lastName"] = "Last Name cannot be empty";
        }
        if (typeof fields["lastName"] !== "undefined") {
            const textcaseRegExp = /^[A-Za-z]+$/;
            const floatcasenumber = textcaseRegExp.test(fields["lastName"]);
            if (!floatcasenumber) {
                formIsValid = false;
                errors["lastName"] = "LastName should be Text";
            }
        }
        if (!fields["zipCode"]) {
            formIsValid = false;
            errors["zipCode"] = "Zip code cannot be empty";
        }
        if (typeof fields["zipCode"] !== "undefined") {
            if (this.state.selectedCountryOption != null || undefined || "") {
                if (this.state.selectedCountryOption.value === "US") {
                    const maxnumbercaseRegExp = /^[0-9]{0,5}$/;
                    const minnumbercaseRegexp = /^.{5,}$/;
                    const maxcasenumber = maxnumbercaseRegExp.test(fields["zipCode"]);
                    const mincasenumber = minnumbercaseRegexp.test(fields["zipCode"]);
                    if (!maxcasenumber) {
                        formIsValid = false;
                        errors["zipCode"] = "Zipcode should be 5 digit";
                    }
                    if (!mincasenumber) {
                        formIsValid = false;
                        errors["zipCode"] = "Zipcode should be 5 digit";
                    }
                }
                else if (this.state.selectedCountryOption.value === "IN") {
                    const maxnumbercaseRegExp = /^[0-9]{0,6}$/;
                    const minnumbercaseRegexp = /^.{5,}$/;
                    const maxcasenumber = maxnumbercaseRegExp.test(fields["zipCode"]);
                    const mincasenumber = minnumbercaseRegexp.test(fields["zipCode"]);
                    if (!maxcasenumber) {
                        formIsValid = false;
                        errors["zipCode"] = "Zipcode should be 6 digit";
                    }
                    if (!mincasenumber) {
                        formIsValid = false;
                        errors["zipCode"] = "Zipcode should be 6 digit";
                    }
                }
            }
        }

        if (selectedGenderOption === null) {
            formIsValid = false;
            errors["gender"] = "Gender cannot be empty";
        }

        if (selectedStatusOption === null) {
            formIsValid = false;
            errors["status"] = "Status cannot be empty";
        }

        if (selectGraduationOption === null) {
            formIsValid = false;
            errors["graduationYear"] = "Graduation year cannot be empty";
        }
        if (typeof fields["currentGPA"] !== "undefined") {
            const decimalcaseRegExp = (/^(\d|4)\.\d{2}$/);
            const floatcasenumber = decimalcaseRegExp.test(fields["currentGPA"]);
            if (!floatcasenumber) {
                formIsValid = false;
                errors["currentGPA"] = "GPA should be float ( example : 4.55 )";
            }
        }
        if (!fields["currentGPA"]) {
            formIsValid = false;
            errors["currentGPA"] = "Current GPA cannot be empty";
        }

        if (selectSchoolOption === null) {
            formIsValid = false;
            errors["highSchoolName"] = "High school name cannot be empty";
        }
        if (selectedCountryOption === null) {
            formIsValid = false;
            errors["country"] = "country cannot be empty";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    handleNext = async (e) => {
        e.preventDefault();
        if (this.handleValidation()) {
            let fields = this.state.fields;
            console.log(fields)
            let { studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox, selectedGenderOption, selectedStatusOption, selectedCountryOption, selectGraduationOption, selectSchoolOption } = this.state;
            let values = {
                firstName: fields["firstName"],
                lastName: fields["lastName"],
                graduationYear: selectGraduationOption.label,
                zipCode: fields["zipCode"],
                highSchoolName: selectSchoolOption.label,
                currentGpa: fields["currentGPA"],
                studentsCollab: studentsCollabCheckBox,
                counselingAllowed: counselingAllowedCheckBox,
                subCouncelingServices: subCouncelingServicesCheckBox,
                gender: selectedGenderOption.label,
                status: selectedStatusOption.label,
                country: selectedCountryOption.label,
            }
            console.log(values)
            this.props.setUserProfile(values);
        }
    };

    componentDidMount() {
        this.props.getGender();
        this.props.getStatus();
        this.props.getCountry();
        this.props.getUserProfile();
    }

    componentDidUpdate() {
        let { fields, selectedCountryOption } = this.state;
        if (this.props.isProfileSuccess) {
            this.props.navigate('/onboarding/sendinvites');
        }
        console.log("userProfile", this.props.userProfile)
        console.log("userProfileData", this.props.userProfileData)
        if (this.props.userProfileData && this.props.userProfile != undefined) {
            let userProfile = this.props.userProfile;
            let userProfileData = this.props.userProfileData;
            fields["firstName"] = userProfile.firstName ? userProfile.firstName : "";
            fields["lastName"] = userProfile.lastName ? userProfile.lastName : "";
            fields["zipCode"] = userProfile.zipCode ? userProfile.zipCode : "";
            fields["currentGPA"] = userProfileData.currentGpa ? userProfileData.currentGpa : "";
            let interestedcountry = [userProfile.country];
            console.log(interestedcountry)
            let countryOptions = [];
            interestedcountry !== undefined && interestedcountry.length > 0 && interestedcountry.map((x, key) => {
                console.log( x)
                var temp = { label: x }
                countryOptions.push(temp);
            });
            selectedCountryOption = countryOptions;
        }
    }

    render() {
        const { gender, status, country } = this.props;
        const { graduationyearlist, highschoollist } = this.state;
        let genderOptions = [];
        let statusOptions = [];
        let countryOptions = [];
        let graduationOptions = [];
        let SchoolOptions = [];
        gender !== undefined && gender.length > 0 && gender.map((x, key) => {
            var temp = { label: x, value: key }
            genderOptions.push(temp);
        });
        status !== undefined && status.length > 0 && status.map((x, key) => {
            var temp = { label: x.status, value: x.id }
            statusOptions.push(temp);
        });
        country !== undefined && country.length > 0 && country.map((x, key) => {
            var temp = { label: x.isoCode2Digit, value: x.isoCode2Digit }
            countryOptions.push(temp);
        })
        graduationyearlist !== undefined && graduationyearlist.length > 0 && graduationyearlist.map((x, key) => {
            var temp = { label: x.label, value: x.value }
            graduationOptions.push(temp);
        })
        highschoollist !== undefined && highschoollist.length > 0 && highschoollist.map((x, key) => {
            var temp = { label: x.label, value: x.value }
            SchoolOptions.push(temp);
        })

        const { ValueContainer, Placeholder } = components;
        const CustomValueContainer = ({ children, ...props }) => {
            return (
                <ValueContainer {...props}>
                    <Placeholder {...props} isFocused={props.isFocused}>
                        {props.selectProps.placeholder}
                    </Placeholder>
                    {React.Children.map(children, child => child && child.type !== Placeholder ? child : null)}
                </ValueContainer>
            );
        };
        const styles = { placeholder: (base, state) => ({ ...base, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block', }), }
        return (
            <>
                <div className="profile">
                    <div className="profile__form">
                        <h1 className="profile__form__title text-center">Create Profile</h1>
                        <p className="profile__form__sub-title">General</p>
                        <Form >
                            <div className="row m-0">
                                <div className="col-md-6 col-sm-6 col-12 ps-0 p-mob-0">
                                    <div className="input-floating-label">
                                        <input refs="firstName" type="text" className={"textbox--primary textbox--rounded input"} value={this.state.fields["firstName"]}
                                            name="firstName" placeholder=" " onChange={this.handleChange.bind(this, "firstName")} onKeyPress={(event) => { if (!/[A-Za-z]/.test(event.key)) { event.preventDefault(); } }} />
                                        <label>First Name</label>
                                        {this.state.errors["firstName"] && <span className="error-text">{this.state.errors["firstName"]}</span>}
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6 col-12 pe-0 p-mob-0">
                                    <div className="input-floating-label">
                                        <input refs="lastName" type="text" className={"textbox--primary textbox--rounded input"} name="lastName" placeholder=" " onKeyPress={(event) => { if (!/[A-Za-z]/.test(event.key)) { event.preventDefault(); } }}
                                            onChange={this.handleChange.bind(this, "lastName")} value={this.state.fields["lastName"]}
                                        />
                                        <label>Last Name</label>
                                        {this.state.errors["lastName"] && <span className="error-text">{this.state.errors["lastName"]}</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="row m-0">
                                <div className="col-md-6 col-sm-6 col-12 ps-0 p-mob-0">
                                    <div className="input-floating-label">
                                        <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="country" options={countryOptions} closeMenuOnSelect={true}
                                            isClearable={false} onChange={(value) => this.handleSelectChange(value, 'country')} placeholder="Country"
                                            components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }}
                                            styles={{
                                                container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                                valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                                placeholder: (provided, state) => ({
                                                    ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff',
                                                    paddingLeft: 10, paddingRight: 10, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block',
                                                })
                                            }} />
                                        {this.state.errors["country"] && <span className="error-text">{this.state.errors["country"]}</span>}
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6 col-12 pe-0 p-mob-0">
                                    <div className="input-floating-label">
                                        <input refs="zipCode" type="text" className={"textbox--primary textbox--rounded input"} name="zipCode"
                                            placeholder=" " onChange={this.handleChange.bind(this, "zipCode")} maxLength={9} value={this.state.fields["zipCode"]}
                                            onKeyPress={(event) => { if (!/[0-9-]/.test(event.key)) { event.preventDefault(); } }} />
                                        <label>Zip code</label>
                                        {this.state.errors["zipCode"] && <span className="error-text">{this.state.errors["zipCode"]}</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="row m-0">
                                <div className="col-md-12 col-sm-12 col-12 p-0">
                                    <div className="input-floating-label">
                                        <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="gender" options={genderOptions} closeMenuOnSelect={true}
                                            isClearable={false} onChange={(value) => this.handleSelectChange(value, 'gender')} placeholder="Gender"
                                            components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }}
                                            styles={{
                                                container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                                valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                                placeholder: (provided, state) => ({
                                                    ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff',
                                                    paddingLeft: 10, paddingRight: 10, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block',
                                                })
                                            }} />
                                        {this.state.errors["gender"] && <span className="error-text">{this.state.errors["gender"]}</span>}
                                    </div>

                                </div>
                            </div>
                            <div className="row m-0">
                                <div className="col-md-12 col-sm-12 col-12 p-0">
                                    <div className="input-floating-label">
                                        <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="status" options={statusOptions} closeMenuOnSelect={true}
                                            isClearable={false} onChange={(value) => this.handleSelectChange(value, 'status')}
                                            components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="My status"
                                            styles={{
                                                container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                                valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                                placeholder: (provided, state) => ({
                                                    ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10
                                                })
                                            }} />
                                        {this.state.errors["status"] && <span className="error-text">{this.state.errors["status"]}</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="row m-0">
                                <div className="col-md-6 col-sm-6 col-12 ps-0  p-mob-0">
                                    <div className="input-floating-label">
                                        <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="graduationYear" options={graduationOptions} closeMenuOnSelect={true}
                                            isClearable={false} onChange={(value) => this.handleSelectChange(value, 'graduationYear')}
                                            components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="Graduation Year"
                                            styles={{
                                                container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                                valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                                placeholder: (provided, state) => ({
                                                    ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10
                                                })
                                            }} />
                                        {this.state.errors["graduationYear"] && <span className="error-text">{this.state.errors["graduationYear"]}</span>}
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6 col-12 pe-0  p-mob-0">
                                    <div className="input-floating-label">
                                        <input refs="currentGPA" onKeyPress={(event) => { if (!/[0-9.]/.test(event.key)) { event.preventDefault(); } }} type="text" className={"textbox--primary textbox--rounded input"} name="currentGPA"
                                            placeholder="currentGPA" onChange={this.handleChange.bind(this, "currentGPA")} value={this.state.fields["currentGPA"]} />
                                        <p className="cta--text mb-1">GPA should be float ( example : 4.55 ) and value should not exceed 9.99</p>
                                        <label>Current GPA</label>
                                        {this.state.errors["currentGPA"] && <span className="error-text">{this.state.errors["currentGPA"]}</span>}
                                    </div>
                                </div>
                            </div>
                            <div className="row m-0">
                                <div className="col-md-12 col-sm-12 col-12 p-0">
                                    <div className="input-floating-label">
                                        <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="highSchoolName" options={SchoolOptions} closeMenuOnSelect={true}
                                            isClearable={false} onChange={(value) => this.handleSelectChange(value, 'highSchoolName')}
                                            components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="High School Name"
                                            styles={{
                                                container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                                valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                                placeholder: (provided, state) => ({
                                                    ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10
                                                })
                                            }} />
                                        {this.state.errors["highSchoolName"] && <span className="error-text">{this.state.errors["highSchoolName"]}</span>}
                                    </div>
                                </div>
                            </div>
                            <div style={{ "marginBottom": "100px" }}>
                                <p className="profile__form__sub-title">Interests</p>
                                <p className="">
                                    <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value2" onClick={(e) => this.handleCheckboxChange(e, 'studentsCollab')} />
                                    <label htmlFor="styled-checkbox-1">
                                        <span>Collaborating with students with similar goals</span>
                                    </label>
                                </p>
                                <p className="">
                                    <input className="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" onClick={(e) => this.handleCheckboxChange(e, 'counselingAllowed')} />
                                    <label htmlFor="styled-checkbox-2">
                                        <span>Advice and counseling from senior students</span>
                                    </label>
                                </p>
                                <p className="">
                                    <input className="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value2" onClick={(e) => this.handleCheckboxChange(e, 'subCouncelingServices')} />
                                    <label htmlFor="styled-checkbox-3">
                                        <span>Professional counseling services</span>
                                    </label>
                                </p>
                            </div>
                        </Form>
                    </div>
                </div>
                <div className="footer">
                    <div className="row m-0">
                        <div className="footer__left col-md-4 col-sm-4 col-4 text-center p-0">
                            <p>
                                <Link to="/onboarding/goals" onClick={() => this.props.setIsGoalSuccess(false)} className="footer__left__cta">
                                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" clipRule="evenodd" d="M5.80474 0.195262C5.54439 -0.0650874 5.12228 -0.0650874 4.86193 0.195262L0.195262 4.86193C-0.0650873 5.12228 -0.0650873 5.54439 0.195262 5.80474L4.86193 10.4714C5.12228 10.7318 5.54439 10.7318 5.80474 10.4714C6.06509 10.2111 6.06509 9.78894 5.80474 9.52859L1.60948 5.33333L5.80474 1.13807C6.06509 0.877722 6.06509 0.455612 5.80474 0.195262Z" fill="#1B1C1E" />
                                    </svg>
                                    <span>Go back</span>
                                </Link>
                            </p>
                        </div>
                        <div className="footer__center col-md-4 col-sm-4 col-4 text-center">
                            <p>3 out of 5 steps</p>
                        </div>
                        <div className="footer__right col-md-4 col-sm-4 col-4 text-center">
                            <Link className="footer__cta" to="#" onClick={e => this.handleNext(e)}>Next step</Link>
                        </div>
                    </div>
                </div>
                <NotificationContainer />
            </>
        );
    }
}

const mapStateToProps = ({ auth, commonData }) => {
    const { message, errorList, status, gender, country } = commonData
    const { isProfileSuccess } = auth;
    const { userProfile, userProfileData } = auth;
    return { message, errorList, status, gender, isProfileSuccess, userProfile, country, userProfileData }
};
export default connect(mapStateToProps, { getGender, getStatus, setUserProfile, getUserProfile, setIsGoalSuccess, getCountry })(withRouter(Index));