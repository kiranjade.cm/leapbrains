import React, { Component } from "react";
import "../../../../assets/css/profile.less";
import { Form } from "react-bootstrap";
import Dropzone from "react-dropzone";
import Select, { components } from "react-select";
import { connect } from "react-redux";
import { withRouter } from "../../../../redux/store/navigate";
import { getCountry, getStatus } from "../../../../redux/actions/Common";
import { getUserProfile } from "../../../../redux/actions/Auth";
import { NotificationContainer } from 'react-notifications';
import { Link } from "react-router-dom";
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
            textCount: 0,
            textCountError: "",
            messageText: "",
            isShowPan: false,
            isShowActivity: false,
            isShowPantwo: false,
            isShowActivitytwo: false,
            highschool: true,
            university: false,
            proffesional: false,
            selectedOption: "highschool",
            selectedCountryOption: null,
            selectSchoolOption: null,
            selectedStatusOption: null,
            selectedUniversityOption: null,
            selectGraduationOption: null,
            highschoollist: [
                { label: "Public High Schools", value: "Public High Schools" },
                { label: "Charter High Schools", value: "Charter High Schools" },
                { label: "Magnet High Schools", value: "Magnet High Schools" },
                { label: "Carnegie Vanguard High School", value: "Carnegie Vanguard High School" },
                { label: "Gilbert Classical Academy", value: "Gilbert Classical Academy" },
            ],
            universitylist: [
                { label: "Princeton University", value: "Princeton University" },
                { label: "Harvard University", value: "Harvard University" },
                { label: "Stanford University", value: "Stanford University" },
                { label: "Yale University", value: "Yale Universityl" },
                { label: "Washington University in St. Louis", value: "Washington University" },
            ],
            graduationyearlist: [
                { label: 2022, value: 2022 },
                { label: 2023, value: 2023 },
                { label: 2024, value: 2024 },
                { label: 2025, value: 2025 },
                { label: 2026, value: 2026 },
                { label: 2027, value: 2027 },
                { label: 2028, value: 2028 },
                { label: 2029, value: 2029 },
                { label: 2030, value: 2030 },
                { label: 2031, value: 2031 },
                { label: 2032, value: 2032 },
            ],
        };
    }
    handleChangeradio = (e) => {
        const names = e.currentTarget.value
        if (names === "highschool") { this.setState({ highschool: true, university: false, proffesional: false, selectedOption: "highschool" }) }
        else if (names === "university") { this.setState({ university: true, highschool: false, proffesional: false, selectedOption: "university" }) }
        else { this.setState({ university: false, highschool: false, proffesional: true, selectedOption: "proffesional" }) }
    }
    handleChange(field, e) {
        let { errors } = this.state;
        let fields = this.state.fields;
        fields[field] = e.target.value;
        if (e.target.value.length >= 0) { errors[field] = ""; }
        this.setState({ fields, errors: errors });
    }
    handleShowPlan(e) {
        e.preventDefault()
        if (this.state.isShowPan == false) {
            this.setState({ isShowPan: true })
        }
        else {
            this.setState({ isShowPan: false })
        }
    }
    handleShowPlanActivity(e) {
        e.preventDefault()
        if (this.state.isShowActivity == false) {
            this.setState({ isShowActivity: true })
        }
        else {
            this.setState({ isShowActivity: false })
        }
    }
    handleShowPlantwo(e) {
        e.preventDefault()
        if (this.state.isShowPantwo == false) {
            this.setState({ isShowPantwo: true })
        }
        else {
            this.setState({ isShowPantwo: false })
        }
    }
    handleShowPlanActivitytwo(e) {
        e.preventDefault()
        if (this.state.isShowActivitytwo == false) {
            this.setState({ isShowActivitytwo: true })
        }
        else {
            this.setState({ isShowActivitytwo: false })
        }
    }
    handleSelectChange(options, name) {
        let { selectedCountryOption, selectSchoolOption, selectedStatusOption, selectedUniversityOption, selectGraduationOption, errors } = this.state;
        switch (name) {
            case "country": selectedCountryOption = options;
                errors["country"] = "";
                break;
            case "highSchoolName": selectSchoolOption = options;
                errors["highSchoolName"] = "";
                break;
            case "status": selectedStatusOption = options;
                errors["status"] = "";
                break;
            case "status": selectedUniversityOption = options;
                errors["universityName"] = "";
                break;
            case "graduationYear":
                selectGraduationOption = options;
                errors["graduationYear"] = "";
                break;
        }
        this.setState({ selectedCountryOption, selectSchoolOption, selectedStatusOption, selectedUniversityOption, selectGraduationOption, errors: errors });
    }
    handleBlur(e) {
        if (e.target.value === "") { e.target.type = "text" }
        else if (e.target.value !== "") { e.target.type = "time" }
    }
    handleClickAddTimeMon(e) {
        e.preventDefault();
        var count = document.getElementsByClassName("initial_plan_parent_Mon").length;
        let menu = document.querySelector('#initial_plan_parent_Mon');
        let clonedMenu = menu.cloneNode(true);
        clonedMenu.id = 'menu-mobile';
        var child = clonedMenu.firstChild;
        document.getElementById("initial_plan_clone_Mon").appendChild(clonedMenu);
    }
    handleClickAddTimeTue(e) {
        e.preventDefault();
        var count = document.getElementsByClassName("initial_plan_parent_Tue").length;
        let menu = document.querySelector('#initial_plan_parent_Tue');
        let clonedMenu = menu.cloneNode(true);
        clonedMenu.id = 'menu-mobile';
        var child = clonedMenu.firstChild;
        document.getElementById("initial_plan_clone_Tue").appendChild(clonedMenu);
    }
    handleClickAddTimeWed(e) {
        e.preventDefault();
        var count = document.getElementsByClassName("initial_plan_parent_Wed").length;
        let menu = document.querySelector('#initial_plan_parent_Wed');
        let clonedMenu = menu.cloneNode(true);
        clonedMenu.id = 'menu-mobile';
        var child = clonedMenu.firstChild;
        document.getElementById("initial_plan_clone_Wed").appendChild(clonedMenu);
    }
    handleClickAddTimeThu(e) {
        e.preventDefault();
        var count = document.getElementsByClassName("initial_plan_parent_Thu").length;
        let menu = document.querySelector('#initial_plan_parent_Thu');
        let clonedMenu = menu.cloneNode(true);
        clonedMenu.id = 'menu-mobile';
        var child = clonedMenu.firstChild;
        document.getElementById("initial_plan_clone_Thu").appendChild(clonedMenu);
    }
    handleClickAddTimeFri(e) {
        e.preventDefault();
        var count = document.getElementsByClassName("initial_plan_parent_Fri").length;
        let menu = document.querySelector('#initial_plan_parent_Fri');
        let clonedMenu = menu.cloneNode(true);
        clonedMenu.id = 'menu-mobile';
        var child = clonedMenu.firstChild;
        document.getElementById("initial_plan_clone_Fri").appendChild(clonedMenu);
    }
    handleClickAddTimeSat(e) {
        e.preventDefault();
        var count = document.getElementsByClassName("initial_plan_parent_Sat").length;
        let menu = document.querySelector('#initial_plan_parent_Sat');
        let clonedMenu = menu.cloneNode(true);
        clonedMenu.id = 'menu-mobile';
        var child = clonedMenu.firstChild;
        document.getElementById("initial_plan_clone_Sat").appendChild(clonedMenu);
    }
    handleClickAddTimeSun(e) {
        e.preventDefault();
        var count = document.getElementsByClassName("initial_plan_parent_Sun").length;
        let menu = document.querySelector('#initial_plan_parent_Sun');
        let clonedMenu = menu.cloneNode(true);
        clonedMenu.id = 'menu-mobile';
        var child = clonedMenu.firstChild;
        document.getElementById("initial_plan_clone_Sun").appendChild(clonedMenu);
    }

    handleValidation() {
        let { selectedCountryOption, selectSchoolOption, selectedStatusOption,selectedUniversityOption,selectGraduationOption } = this.state;
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if (!fields["firstName"]) { formIsValid = false; errors["firstName"] = "First name cannot be empty"; }
        if (!fields["lastName"]) { formIsValid = false; errors["lastName"] = "Last Name cannot be empty"; }
        if (selectedCountryOption === null) { formIsValid = false; errors["country"] = "country cannot be empty"; }
        if (!fields["zipCode"]) { formIsValid = false; errors["zipCode"] = "zipCode cannot be empty"; }
        if (typeof fields["zipCode"] !== "undefined") {
            if (this.state.selectedCountryOption.value === "US") {
                const maxnumbercaseRegExp = /^[0-9]{0,5}$/;
                const minnumbercaseRegexp = /^.{5,}$/;
                const maxcasenumber = maxnumbercaseRegExp.test(fields["zipCode"]);
                const mincasenumber = minnumbercaseRegexp.test(fields["zipCode"]);
                if (!maxcasenumber) { formIsValid = false; errors["zipCode"] = "Zipcode should be 5 digit"; }
                if (!mincasenumber) { formIsValid = false; errors["zipCode"] = "Zipcode should be 5 digit"; }
            }
            else if (this.state.selectedCountryOption.value === "IN") {
                const maxnumbercaseRegExp = /^[0-9]{0,6}$/;
                const minnumbercaseRegexp = /^.{5,}$/;
                const maxcasenumber = maxnumbercaseRegExp.test(fields["zipCode"]);
                const mincasenumber = minnumbercaseRegexp.test(fields["zipCode"]);
                if (!maxcasenumber) { formIsValid = false; errors["zipCode"] = "Zipcode should be 6 digit"; }
                if (!mincasenumber) { formIsValid = false; errors["zipCode"] = "Zipcode should be 6 digit"; }
            }
        }
        if (selectSchoolOption === null) { formIsValid = false; errors["highSchoolName"] = "High school Name cannot be empty"; }
        if (selectedStatusOption === null) { formIsValid = false; errors["status"] = "Status cannot be empty"; }
        if (!fields["currentGPA"]) { formIsValid = false; errors["currentGPA"] = "Current GPA cannot be empty";}
        if (selectedUniversityOption === null) { formIsValid = false; errors["university"] = "University Name cannot be empty"; }
        if (selectGraduationOption === null) { formIsValid = false; errors["graduationYear"] = "Graduation year cannot be empty";}
        if (!fields["UniversityGPA"]) { formIsValid = false; errors["UniversityGPA"] = "University GPA cannot be empty";}
        if (!fields["highschoolGPA"]) { formIsValid = false; errors["highschoolGPA"] = "High school GPA cannot be empty";}
        this.setState({ errors: errors });
        return formIsValid;
    }
    componentDidMount() {
        this.props.getCountry();
        this.props.getStatus();
    }
    render() {
        let { textCount, textCountError, messageText, isShowPan, isShowActivity, isShowPantwo, isShowActivitytwo } = this.state;
        let { highschoollist, universitylist, graduationyearlist, highschool, university, proffesional } = this.state;
        const { country, status, } = this.props;
        let countryOptions = [];
        let SchoolOptions = [];
        let statusOptions = [];
        let UniversityOptions = [];
        let graduationOptions = [];
        const options = [
            { value: '2021 (1-st semestre)', label: '2021 (1-st semestre)' },
            { value: '2021 (2-st semestre)', label: '2021 (2-st semestre)' },
            { value: '2022 (1-st semestre)', label: '2022 (1-st semestre)' },
            { value: '2022 (2-nd semestre)', label: '2022 (2-nd semestre)' },
            { value: '2023 (1-st semestre)', label: '2023 (1-st semestre)' },
            { value: '2023 (2-nd semestre)', label: '2023 (2-nd semestre)' }
        ]
        const levels = [
            { value: 'Level-1', label: 'Level-1' },
            { value: 'Level-2', label: 'Level-2' },
            { value: 'Level-3', label: 'Level-3' },
            { value: 'Level-4', label: 'Level-4' },
            { value: 'Level-5', label: 'Level-5' },
        ]
        const options1 = [
            { value: 'Computer Science and IT', label: 'Computer Science and IT' },
            { value: 'Business', label: 'Business' },
            { value: 'English', label: 'English' },
            { value: 'Foreign Language', label: 'Foreign Language' },
            { value: 'Math', label: 'Math' },
            { value: 'Performing Arts', label: 'Performing Arts' }
        ]
        const options2 = [
            { value: 'Animation', label: 'Animation' },
            { value: 'App development', label: 'App development' },
            { value: 'Audio production', label: 'Audio production' },
            { value: 'Computer programming', label: 'Computer programming' },
            { value: 'Computer repair', label: 'Computer repair' }
        ]
        const activity = [
            { value: 'Arts', label: 'Arts' },
            { value: 'Music Instrumental', label: 'Music Instrumental' },
            { value: 'Sports', label: 'Sports' },
            { value: 'Speech and Debate', label: 'Speech and Debate' },
            { value: 'Computer / Technology', label: 'Computer / Technology' }
        ]
        country !== undefined && country.length > 0 && country.map((x, key) => {
            var temp = { label: x.isoCode2Digit, value: x.isoCode2Digit }
            countryOptions.push(temp);
        })
        highschoollist !== undefined && highschoollist.length > 0 && highschoollist.map((x, key) => {
            var temp = { label: x.label, value: x.value }
            SchoolOptions.push(temp);
        })
        status !== undefined && status.length > 0 && status.map((x, key) => {
            var temp = { label: x.status, value: x.id }
            statusOptions.push(temp);
        });
        universitylist !== undefined && universitylist.length > 0 && universitylist.map((x, key) => {
            var temp = { label: x.label, value: x.value }
            UniversityOptions.push(temp);
        })
        graduationyearlist !== undefined && graduationyearlist.length > 0 && graduationyearlist.map((x, key) => {
            var temp = { label: x.label, value: x.value }
            graduationOptions.push(temp);
        })
        const style1 = { control: (base, state) => ({ ...base, border: "0 !important", boxShadow: "0 !important", "&:hover": { border: "0 !important" } }) };
        const { ValueContainer, Placeholder } = components;
        const CustomValueContainer = ({ children, ...props }) => { return (<ValueContainer {...props}> <Placeholder {...props} isFocused={props.isFocused}>{props.selectProps.placeholder} </Placeholder> {React.Children.map(children, child => child && child.type !== Placeholder ? child : null)}</ValueContainer>); };
        const styles = { placeholder: (base, state) => ({ ...base, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block', }), }
        return (
            <>
                <div className="profile mb-4">
                    <div className="profile__form">
                        <h1 className="profile__form__title text-center">Create Profile</h1>
                        <Form >
                            <div>
                                <p className="profile__form__sub-title">Avatar</p>
                                <div className="dragdropcnt">
                                    <Dropzone onDrop={this.handleDrop} accept="image/jpeg" maxSize={1}>
                                        {({ getRootProps, getInputProps, isDragActive, isDragReject }) => (
                                            <div {...getRootProps({ className: "dropzone" })}>
                                                <input {...getInputProps()} />
                                                <div className="dropzoneinside">
                                                    <div className="dropzoneinsidetop">
                                                        <span><svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M16.6602 20H5.26016C3.68615 20 2.41016 18.724 2.41016 17.15V3.85C2.41016 2.27599 3.68614 1 5.26016 1H14.7602L14.7703 1.01017C15.438 1.06674 16.0674 1.35729 16.5449 1.83475L18.6754 3.96526C19.1529 4.44271 19.4434 5.07213 19.5 5.73983L19.5102 5.75V17.15C19.5102 18.724 18.2342 20 16.6602 20ZM5.26016 18.1C4.73549 18.1 4.31016 17.6747 4.31016 17.15V3.85C4.31016 3.32533 4.73549 2.9 5.26016 2.9H13.8102V4.8C13.8102 5.84934 14.6608 6.7 15.7102 6.7H17.6102V17.15C17.6102 17.6747 17.1848 18.1 16.6602 18.1H5.26016Z" fill="#1B1C1E" /></svg></span>
                                                    </div>
                                                    <div className="dropzoneinsidemid">
                                                        {!isDragActive && 'Drag & Drop the document  here'}
                                                        {isDragActive && !isDragReject && "Drop it like it's hot!"}
                                                        {isDragReject && "File type not accepted, sorry!"}
                                                        {isDragReject && "File type not accepted, sorry!"}
                                                    </div>
                                                    <div className="dropzoneinsidebtm">
                                                        <span>Or</span><p>Choose</p>
                                                    </div>
                                                </div>
                                            </div>
                                        )}
                                    </Dropzone>
                                    <p className="dropzonebtm">Accepted file type: .jpg and .png only</p>
                                </div>
                            </div>
                            <div>
                                <p className="profile__form__sub-title">General</p>
                                <div className="generalcnt">
                                    <div className="row m-0">
                                        <div className="col-md-6 col-sm-12 col-12 ps-0 p-mob-0">
                                            <div className="input-floating-label">
                                                <input refs="firstName" type="text" className={"textbox--primary textbox--rounded input"} value={this.state.fields["firstName"]} name="firstName" placeholder=" " onChange={this.handleChange.bind(this, "firstName")} onKeyPress={(event) => { if (!/[A-Za-z]/.test(event.key)) { event.preventDefault(); } }} />
                                                <label>First Name</label>
                                                {this.state.errors["firstName"] && <span className="error-text">{this.state.errors["firstName"]}</span>}
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-sm-12 col-12 pe-0 p-mob-0">
                                            <div className="input-floating-label">
                                                <input refs="lastName" type="text" className={"textbox--primary textbox--rounded input"} name="lastName" placeholder=" " onChange={this.handleChange.bind(this, "lastName")} onKeyPress={(event) => { if (!/[A-Za-z]/.test(event.key)) { event.preventDefault(); } }} />
                                                <label>Last Name</label>
                                                {this.state.errors["lastName"] && <span className="error-text">{this.state.errors["lastName"]}</span>}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row m-0">
                                        <div className="col-md-6 col-sm-6 col-12 ps-0 p-mob-0">
                                            <div className="input-floating-label">
                                                <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="country" options={countryOptions} closeMenuOnSelect={true}
                                                    isClearable={false} onChange={(value) => this.handleSelectChange(value, 'country')} placeholder="Country" components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }}
                                                    styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block', }) }} />
                                                {this.state.errors["country"] && <span className="error-text">{this.state.errors["country"]}</span>}
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 col-12 pe-0 p-mob-0">
                                            <div className="input-floating-label">
                                                <input refs="zipCode" type="text" className={"textbox--primary textbox--rounded input"} name="zipCode" placeholder=" " onChange={this.handleChange.bind(this, "zipCode")} maxLength={9} onKeyPress={(event) => { if (!/[0-9-]/.test(event.key)) { event.preventDefault(); } }} />
                                                <label>Zip code</label>
                                                {this.state.errors["zipCode"] && <span className="error-text">{this.state.errors["zipCode"]}</span>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p className="profile__form__sub-title">Current Status</p>
                                <div className="statuscnt">
                                    <p className="profile__form__smalltext">I am currently...</p>
                                </div>
                                <div className="profile__form__checkboxcnt mb-2">
                                    <div className="styled-radio-flex">
                                        <div className="styled-radio">
                                            <input id="radio-1" name="radio" type="radio" value="highschool" checked={this.state.selectedOption === "highschool"} onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-1" className="radio-label">High School</label>
                                        </div>
                                        <div className="styled-radio">
                                            <input id="radio-2" name="radio" type="radio" value="university" checked={this.state.selectedOption === "university"} onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-2" className="radio-label">University</label>
                                        </div>
                                        <div className="styled-radio">
                                            <input id="radio-3" name="radio" type="radio" value="proffesional " checked={this.state.selectedOption === "proffesional"} onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-3" className="radio-label">Professional</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    {highschool &&
                                        <div className="highschoolcnt mt-4">
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="highSchoolName" options={SchoolOptions} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="High School Name"
                                                        onChange={(value) => this.handleSelectChange(value, 'highSchoolName')} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                    {this.state.errors["highSchoolName"] && <span className="error-text">{this.state.errors["highSchoolName"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="status" options={statusOptions} closeMenuOnSelect={true} isClearable={false}
                                                        onChange={(value) => this.handleSelectChange(value, 'status')} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="My status" styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                    {this.state.errors["status"] && <span className="error-text">{this.state.errors["status"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="Current GPA" type="text" className="textbox--primary textbox--rounded input" name="Current GPA" placeholder=" " />
                                                    <label>Current GPA</label> <p className="cta--text" style={{ "fontSize": "12px", "marginLeft": "20px" }}>*Optional</p>
                                                    {/* {this.state.errors["currentGPA"] && <span className="error-text">{this.state.errors["currentGPA"]}</span>} */}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="SAT score" type="text" className="textbox--primary textbox--rounded input" name="SAT score" placeholder=" " />
                                                    <label>SAT score</label>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="ACT score" type="text" className="textbox--primary textbox--rounded input" name="ACT score" placeholder=" " />
                                                    <label>ACT score</label>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <textarea className={"textarea--primary textbox--rounded input"} name="Other accomplishment" placeholder=" " value={messageText} style={{ "height": "80px" }} onChange={(e) => {
                                                        if (e.target.value.length <= 75) {textCount = e.target.value.length; messageText = e.target.value; this.setState({ textCount, messageText, textCountError: "" }) } else { this.setState({ textCountError: "Message only allow 75 Characters" }) }}} />
                                                    <label>Other accomplishment</label> <p><span>{textCount}</span>/75</p> <p className="error-text">{textCountError}</p> </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                                <div>
                                    {university &&
                                        <div className="highschoolcnt mt-4">
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="highSchoolName" options={SchoolOptions} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="High School Name"
                                                        onChange={(value) => this.handleSelectChange(value, 'highSchoolName')} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                    {this.state.errors["highSchoolName"] && <span className="error-text">{this.state.errors["highSchoolName"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="university" options={UniversityOptions} closeMenuOnSelect={true} isClearable={false}
                                                        onChange={(value) => this.handleSelectChange(value, 'university')} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="University Name" styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                    {this.state.errors["university"] && <span className="error-text">{this.state.errors["university"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="graduationYear" options={graduationOptions} closeMenuOnSelect={true} isClearable={false} onChange={(value) => this.handleSelectChange(value, 'graduationYear')}
                                                        components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="University Graduation Year" styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                    {this.state.errors["graduationYear"] && <span className="error-text">{this.state.errors["graduationYear"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="University GPA" type="text" className="textbox--primary textbox--rounded input" name="University GPA" placeholder=" " />
                                                    <label>University GPA</label>
                                                    <p className="cta--text" style={{ "fontSize": "12px", "marginLeft": "20px" }}>*Optional</p>
                                                    {this.state.errors["UniversityGPA"] && <span className="error-text">{this.state.errors["UniversityGPA"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="SAT score" type="text" className="textbox--primary textbox--rounded input" name="SAT score" placeholder=" " />
                                                    <label>SAT score</label>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="ACT score" type="text" className="textbox--primary textbox--rounded input" name="ACT score" placeholder=" " />
                                                    <label>ACT score</label>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <textarea className={"textarea--primary textbox--rounded input"} name="Other accomplishment" placeholder=" " value={messageText} style={{ "height": "80px" }} onChange={(e) => {
                                                        if (e.target.value.length <= 75) {
                                                            textCount = e.target.value.length; messageText = e.target.value
                                                            this.setState({ textCount, messageText, textCountError: "" })
                                                        } else { this.setState({ textCountError: "Message only allow 75 Characters" }) }
                                                    }} />
                                                    <label>Other accomplishment</label> <p><span>{textCount}</span>/75</p> <p className="error-text">{textCountError}</p> </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                                <div>
                                    {proffesional &&
                                        <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                            <div className="input-floating-label">
                                                <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="highSchoolName" options={SchoolOptions} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="High School Name"
                                                    onChange={(value) => this.handleSelectChange(value, 'highSchoolName')} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                {this.state.errors["highSchoolName"] && <span className="error-text">{this.state.errors["highSchoolName"]}</span>}
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="Current GPA" type="text" className="textbox--primary textbox--rounded input" name="Current GPA" placeholder=" " />
                                                    <label>High School GPA</label>
                                                    {this.state.errors["highschoolGPA"] && <span className="error-text">{this.state.errors["highschoolGPA"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="university" options={UniversityOptions} closeMenuOnSelect={true} isClearable={false}
                                                        onChange={(value) => this.handleSelectChange(value, 'university')} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="University Name" styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                    {this.state.errors["university"] && <span className="error-text">{this.state.errors["university"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="University GPA" type="text" className="textbox--primary textbox--rounded input" name="University GPA" placeholder=" " />
                                                    <label>University GPA</label>
                                                    {this.state.errors["UniversityGPA"] && <span className="error-text">{this.state.errors["UniversityGPA"]}</span>}
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="SAT score" type="text" className="textbox--primary textbox--rounded input" name="SAT score" placeholder=" " />
                                                    <label>SAT score</label>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="ACT score" type="text" className="textbox--primary textbox--rounded input" name="ACT score" placeholder=" " />
                                                    <label>ACT score</label>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <textarea className={"textarea--primary textbox--rounded input"} name="Other accomplishment" placeholder=" " value={messageText} style={{ "height": "80px" }} onChange={(e) => {
                                                        if (e.target.value.length <= 75) {
                                                            textCount = e.target.value.length; messageText = e.target.value
                                                            this.setState({ textCount, messageText, textCountError: "" })
                                                        } else { this.setState({ textCountError: "Message only allow 75 Characters" }) }
                                                    }} />
                                                    <label>Other accomplishment</label> <p><span>{textCount}</span>/75</p> <p className="error-text">{textCountError}</p> </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                            <div>
                                <p className="profile__form__sub-title">Advisory Courses</p>
                                <div>
                                    <p className="profile__form__smalltext">Ongoing courses</p>
                                    <div className="cols-m-12">
                                        <div>
                                            <div className="mb-3">
                                                {isShowPan &&
                                                    <div className="row m-0"  >
                                                        <div className="col-12 p-0 mb-4">
                                                            <Select className="goals__form__select mb-3" placeholder="Course name" classNamePrefix="mySelect" options={options1} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                        </div>
                                                        <div className="col-12 p-0 mb-4">
                                                            <Select placeholder="Course type" className="goals__form__select mb-3" classNamePrefix="mySelect" options={options2} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                        </div>
                                                    </div>
                                                }
                                                {isShowActivity &&
                                                    <div className="row m-0"  >
                                                        <div className="col-12 p-0 mb-4">
                                                            <Select placeholder="Level" className="goals__form__select mb-3" classNamePrefix="mySelect" options={levels} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                        </div>
                                                        <div className="col-12 p-0 mb-4">
                                                            <Select placeholder="Activity Name " className="goals__form__select mb-3" classNamePrefix="mySelect" options={activity} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                        </div>
                                                    </div>
                                                }
                                                <div className="row m-0">
                                                    <div className="col-6 p-0">
                                                        <a href="" className="cta cta--rounded cta--plan m-0 w-100 d-block text-center p-2 cta-primary text-white" onClick={(e) => this.handleShowPlan(e)} ><svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M9.16658 2.33301C9.16658 2.06779 9.06123 1.81344 8.87369 1.6259C8.68615 1.43836 8.4318 1.33301 8.16658 1.33301C7.90137 1.33301 7.64701 1.43836 7.45948 1.6259C7.27194 1.81344 7.16658 2.06779 7.16658 2.33301V6.66634H2.83325C2.56804 6.66634 2.31368 6.7717 2.12615 6.95923C1.93861 7.14677 1.83325 7.40112 1.83325 7.66634C1.83325 7.93156 1.93861 8.18591 2.12615 8.37345C2.31368 8.56098 2.56804 8.66634 2.83325 8.66634H7.16658V12.9997C7.16658 13.2649 7.27194 13.5192 7.45948 13.7068C7.64701 13.8943 7.90137 13.9997 8.16658 13.9997C8.4318 13.9997 8.68615 13.8943 8.87369 13.7068C9.06123 13.5192 9.16658 13.2649 9.16658 12.9997V8.66634H13.4999C13.7651 8.66634 14.0195 8.56098 14.207 8.37345C14.3946 8.18591 14.4999 7.93156 14.4999 7.66634C14.4999 7.40112 14.3946 7.14677 14.207 6.95923C14.0195 6.7717 13.7651 6.66634 13.4999 6.66634H9.16658V2.33301Z" fill="#fff" /></svg>
                                                            Add Course</a>
                                                    </div>
                                                    <div className="col-6 pe-0">
                                                        <a href="" className="cta cta--rounded cta--plan m-0 w-100 d-block text-center p-2 cta-primary text-white" onClick={(e) => this.handleShowPlanActivity(e)} > <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M9.16658 2.33301C9.16658 2.06779 9.06123 1.81344 8.87369 1.6259C8.68615 1.43836 8.4318 1.33301 8.16658 1.33301C7.90137 1.33301 7.64701 1.43836 7.45948 1.6259C7.27194 1.81344 7.16658 2.06779 7.16658 2.33301V6.66634H2.83325C2.56804 6.66634 2.31368 6.7717 2.12615 6.95923C1.93861 7.14677 1.83325 7.40112 1.83325 7.66634C1.83325 7.93156 1.93861 8.18591 2.12615 8.37345C2.31368 8.56098 2.56804 8.66634 2.83325 8.66634H7.16658V12.9997C7.16658 13.2649 7.27194 13.5192 7.45948 13.7068C7.64701 13.8943 7.90137 13.9997 8.16658 13.9997C8.4318 13.9997 8.68615 13.8943 8.87369 13.7068C9.06123 13.5192 9.16658 13.2649 9.16658 12.9997V8.66634H13.4999C13.7651 8.66634 14.0195 8.56098 14.207 8.37345C14.3946 8.18591 14.4999 7.93156 14.4999 7.66634C14.4999 7.40112 14.3946 7.14677 14.207 6.95923C14.0195 6.7717 13.7651 6.66634 13.4999 6.66634H9.16658V2.33301Z" fill="#fff" /></svg>
                                                            Add Activity
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <p className="profile__form__smalltext">Taken courses</p>
                                    <div className="cols-m-12">
                                        <div>
                                            <div className="mb-3">
                                                {isShowPantwo &&
                                                    <div className="row m-0"  >
                                                        <div className="col-12 p-0 mb-4">
                                                            <Select className="goals__form__select mb-3" placeholder="Course name" classNamePrefix="mySelect" options={options1} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                        </div>
                                                        <div className="col-12 p-0 mb-4">
                                                            <Select placeholder="Course type" className="goals__form__select mb-3" classNamePrefix="mySelect" options={options2} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                        </div>
                                                    </div>
                                                }
                                                {isShowActivitytwo &&
                                                    <div className="row m-0"  >
                                                        <div className="col-12 p-0 mb-4">
                                                            <Select placeholder="Level" className="goals__form__select mb-3" classNamePrefix="mySelect" options={options1} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                        </div>
                                                        <div className="col-12 p-0 mb-4">
                                                            <Select placeholder="Activity Name " className="goals__form__select mb-3" classNamePrefix="mySelect" options={options2} closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                                        </div>
                                                    </div>
                                                }
                                                <div className="row m-0">
                                                    <div className="col-6 p-0">
                                                        <a href="" className="cta cta--rounded cta--plan m-0 w-100 d-block text-center p-2 cta-primary text-white" onClick={(e) => this.handleShowPlantwo(e)} ><svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M9.16658 2.33301C9.16658 2.06779 9.06123 1.81344 8.87369 1.6259C8.68615 1.43836 8.4318 1.33301 8.16658 1.33301C7.90137 1.33301 7.64701 1.43836 7.45948 1.6259C7.27194 1.81344 7.16658 2.06779 7.16658 2.33301V6.66634H2.83325C2.56804 6.66634 2.31368 6.7717 2.12615 6.95923C1.93861 7.14677 1.83325 7.40112 1.83325 7.66634C1.83325 7.93156 1.93861 8.18591 2.12615 8.37345C2.31368 8.56098 2.56804 8.66634 2.83325 8.66634H7.16658V12.9997C7.16658 13.2649 7.27194 13.5192 7.45948 13.7068C7.64701 13.8943 7.90137 13.9997 8.16658 13.9997C8.4318 13.9997 8.68615 13.8943 8.87369 13.7068C9.06123 13.5192 9.16658 13.2649 9.16658 12.9997V8.66634H13.4999C13.7651 8.66634 14.0195 8.56098 14.207 8.37345C14.3946 8.18591 14.4999 7.93156 14.4999 7.66634C14.4999 7.40112 14.3946 7.14677 14.207 6.95923C14.0195 6.7717 13.7651 6.66634 13.4999 6.66634H9.16658V2.33301Z" fill="#fff" /></svg>
                                                            Add Course</a>
                                                    </div>
                                                    <div className="col-6 pe-0">
                                                        <a href="" className="cta cta--rounded cta--plan m-0 w-100 d-block text-center p-2 cta-primary text-white" onClick={(e) => this.handleShowPlanActivitytwo(e)} > <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M9.16658 2.33301C9.16658 2.06779 9.06123 1.81344 8.87369 1.6259C8.68615 1.43836 8.4318 1.33301 8.16658 1.33301C7.90137 1.33301 7.64701 1.43836 7.45948 1.6259C7.27194 1.81344 7.16658 2.06779 7.16658 2.33301V6.66634H2.83325C2.56804 6.66634 2.31368 6.7717 2.12615 6.95923C1.93861 7.14677 1.83325 7.40112 1.83325 7.66634C1.83325 7.93156 1.93861 8.18591 2.12615 8.37345C2.31368 8.56098 2.56804 8.66634 2.83325 8.66634H7.16658V12.9997C7.16658 13.2649 7.27194 13.5192 7.45948 13.7068C7.64701 13.8943 7.90137 13.9997 8.16658 13.9997C8.4318 13.9997 8.68615 13.8943 8.87369 13.7068C9.06123 13.5192 9.16658 13.2649 9.16658 12.9997V8.66634H13.4999C13.7651 8.66634 14.0195 8.56098 14.207 8.37345C14.3946 8.18591 14.4999 7.93156 14.4999 7.66634C14.4999 7.40112 14.3946 7.14677 14.207 6.95923C14.0195 6.7717 13.7651 6.66634 13.4999 6.66634H9.16658V2.33301Z" fill="#fff" /></svg>
                                                            Add Activity </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p className="profile__form__sub-title mt-4">Office hours</p>
                                <div>
                                    <div className="timeinputcnt" id="initial_plan_clone_Mon">
                                        <p className="profile__form__smalltext">Mon</p>
                                        <div className="row m-0" id="initial_plan_parent_Mon">
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div onClick={(e) => this.handleClickAddTimeMon(e)} style={{ "display": "flex", "justifyContent": "end" }} className="col-md-2 col-sm-2 col-2 ps-0 p-mob-0"><svg style={{ marginTop: "15px" }} width="20" height="20" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50 25C50 31.6304 47.3661 37.9893 42.6777 42.6777C37.9893 47.3661 31.6304 50 25 50C18.3696 50 12.0107 47.3661 7.32233 42.6777C2.63392 37.9893 0 31.6304 0 25C0 18.3696 2.63392 12.0107 7.32233 7.32233C12.0107 2.63392 18.3696 0 25 0C31.6304 0 37.9893 2.63392 42.6777 7.32233C47.3661 12.0107 50 18.3696 50 25V25ZM26.5625 14.0625C26.5625 13.6481 26.3979 13.2507 26.1049 12.9576C25.8118 12.6646 25.4144 12.5 25 12.5C24.5856 12.5 24.1882 12.6646 23.8951 12.9576C23.6021 13.2507 23.4375 13.6481 23.4375 14.0625V23.4375H14.0625C13.6481 23.4375 13.2507 23.6021 12.9576 23.8951C12.6646 24.1882 12.5 24.5856 12.5 25C12.5 25.4144 12.6646 25.8118 12.9576 26.1049C13.2507 26.3979 13.6481 26.5625 14.0625 26.5625H23.4375V35.9375C23.4375 36.3519 23.6021 36.7493 23.8951 37.0424C24.1882 37.3354 24.5856 37.5 25 37.5C25.4144 37.5 25.8118 37.3354 26.1049 37.0424C26.3979 36.7493 26.5625 36.3519 26.5625 35.9375V26.5625H35.9375C36.3519 26.5625 36.7493 26.3979 37.0424 26.1049C37.3354 25.8118 37.5 25.4144 37.5 25C37.5 24.5856 37.3354 24.1882 37.0424 23.8951C36.7493 23.6021 36.3519 23.4375 35.9375 23.4375H26.5625V14.0625Z" fill="#1C84EE" /></svg></div>
                                        </div>
                                    </div>
                                    <div className="timeinputcnt" id="initial_plan_clone_Tue">
                                        <p className="profile__form__smalltext">Tue</p>
                                        <div className="row m-0" id="initial_plan_parent_Tue">
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-6 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div onClick={(e) => this.handleClickAddTimeTue(e)} style={{ "display": "flex", "justifyContent": "end" }} className="col-md-2 col-sm-2 col-1 ps-0 p-mob-0"><svg style={{ marginTop: "15px" }} width="20" height="20" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50 25C50 31.6304 47.3661 37.9893 42.6777 42.6777C37.9893 47.3661 31.6304 50 25 50C18.3696 50 12.0107 47.3661 7.32233 42.6777C2.63392 37.9893 0 31.6304 0 25C0 18.3696 2.63392 12.0107 7.32233 7.32233C12.0107 2.63392 18.3696 0 25 0C31.6304 0 37.9893 2.63392 42.6777 7.32233C47.3661 12.0107 50 18.3696 50 25V25ZM26.5625 14.0625C26.5625 13.6481 26.3979 13.2507 26.1049 12.9576C25.8118 12.6646 25.4144 12.5 25 12.5C24.5856 12.5 24.1882 12.6646 23.8951 12.9576C23.6021 13.2507 23.4375 13.6481 23.4375 14.0625V23.4375H14.0625C13.6481 23.4375 13.2507 23.6021 12.9576 23.8951C12.6646 24.1882 12.5 24.5856 12.5 25C12.5 25.4144 12.6646 25.8118 12.9576 26.1049C13.2507 26.3979 13.6481 26.5625 14.0625 26.5625H23.4375V35.9375C23.4375 36.3519 23.6021 36.7493 23.8951 37.0424C24.1882 37.3354 24.5856 37.5 25 37.5C25.4144 37.5 25.8118 37.3354 26.1049 37.0424C26.3979 36.7493 26.5625 36.3519 26.5625 35.9375V26.5625H35.9375C36.3519 26.5625 36.7493 26.3979 37.0424 26.1049C37.3354 25.8118 37.5 25.4144 37.5 25C37.5 24.5856 37.3354 24.1882 37.0424 23.8951C36.7493 23.6021 36.3519 23.4375 35.9375 23.4375H26.5625V14.0625Z" fill="#1C84EE" /></svg></div>
                                        </div>
                                    </div>
                                    <div className="timeinputcnt" id="initial_plan_clone_Wed">
                                        <p className="profile__form__smalltext">Wed</p>
                                        <div className="row m-0" id="initial_plan_parent_Wed">
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div onClick={(e) => this.handleClickAddTimeWed(e)} style={{ "display": "flex", "justifyContent": "end" }} className="col-md-2 col-sm-2 col-2 ps-0 p-mob-0"><svg style={{ marginTop: "15px" }} width="20" height="20" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50 25C50 31.6304 47.3661 37.9893 42.6777 42.6777C37.9893 47.3661 31.6304 50 25 50C18.3696 50 12.0107 47.3661 7.32233 42.6777C2.63392 37.9893 0 31.6304 0 25C0 18.3696 2.63392 12.0107 7.32233 7.32233C12.0107 2.63392 18.3696 0 25 0C31.6304 0 37.9893 2.63392 42.6777 7.32233C47.3661 12.0107 50 18.3696 50 25V25ZM26.5625 14.0625C26.5625 13.6481 26.3979 13.2507 26.1049 12.9576C25.8118 12.6646 25.4144 12.5 25 12.5C24.5856 12.5 24.1882 12.6646 23.8951 12.9576C23.6021 13.2507 23.4375 13.6481 23.4375 14.0625V23.4375H14.0625C13.6481 23.4375 13.2507 23.6021 12.9576 23.8951C12.6646 24.1882 12.5 24.5856 12.5 25C12.5 25.4144 12.6646 25.8118 12.9576 26.1049C13.2507 26.3979 13.6481 26.5625 14.0625 26.5625H23.4375V35.9375C23.4375 36.3519 23.6021 36.7493 23.8951 37.0424C24.1882 37.3354 24.5856 37.5 25 37.5C25.4144 37.5 25.8118 37.3354 26.1049 37.0424C26.3979 36.7493 26.5625 36.3519 26.5625 35.9375V26.5625H35.9375C36.3519 26.5625 36.7493 26.3979 37.0424 26.1049C37.3354 25.8118 37.5 25.4144 37.5 25C37.5 24.5856 37.3354 24.1882 37.0424 23.8951C36.7493 23.6021 36.3519 23.4375 35.9375 23.4375H26.5625V14.0625Z" fill="#1C84EE" /></svg></div>
                                        </div>
                                    </div>
                                    <div className="timeinputcnt" id="initial_plan_clone_Thu">
                                        <p className="profile__form__smalltext">Thu</p>
                                        <div className="row m-0" id="initial_plan_parent_Thu">
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div onClick={(e) => this.handleClickAddTimeThu(e)} style={{ "display": "flex", "justifyContent": "end" }} className="col-md-2 col-sm-2 col-2 ps-0 p-mob-0"><svg style={{ marginTop: "15px" }} width="20" height="20" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50 25C50 31.6304 47.3661 37.9893 42.6777 42.6777C37.9893 47.3661 31.6304 50 25 50C18.3696 50 12.0107 47.3661 7.32233 42.6777C2.63392 37.9893 0 31.6304 0 25C0 18.3696 2.63392 12.0107 7.32233 7.32233C12.0107 2.63392 18.3696 0 25 0C31.6304 0 37.9893 2.63392 42.6777 7.32233C47.3661 12.0107 50 18.3696 50 25V25ZM26.5625 14.0625C26.5625 13.6481 26.3979 13.2507 26.1049 12.9576C25.8118 12.6646 25.4144 12.5 25 12.5C24.5856 12.5 24.1882 12.6646 23.8951 12.9576C23.6021 13.2507 23.4375 13.6481 23.4375 14.0625V23.4375H14.0625C13.6481 23.4375 13.2507 23.6021 12.9576 23.8951C12.6646 24.1882 12.5 24.5856 12.5 25C12.5 25.4144 12.6646 25.8118 12.9576 26.1049C13.2507 26.3979 13.6481 26.5625 14.0625 26.5625H23.4375V35.9375C23.4375 36.3519 23.6021 36.7493 23.8951 37.0424C24.1882 37.3354 24.5856 37.5 25 37.5C25.4144 37.5 25.8118 37.3354 26.1049 37.0424C26.3979 36.7493 26.5625 36.3519 26.5625 35.9375V26.5625H35.9375C36.3519 26.5625 36.7493 26.3979 37.0424 26.1049C37.3354 25.8118 37.5 25.4144 37.5 25C37.5 24.5856 37.3354 24.1882 37.0424 23.8951C36.7493 23.6021 36.3519 23.4375 35.9375 23.4375H26.5625V14.0625Z" fill="#1C84EE" /></svg></div>
                                        </div>
                                    </div>
                                    <div className="timeinputcnt" id="initial_plan_clone_Fri">
                                        <p className="profile__form__smalltext">Fri</p>
                                        <div className="row m-0" id="initial_plan_parent_Fri">
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div onClick={(e) => this.handleClickAddTimeFri(e)} style={{ "display": "flex", "justifyContent": "end" }} className="col-md-2 col-sm-2 col-2 ps-0 p-mob-0"><svg style={{ marginTop: "15px" }} width="20" height="20" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50 25C50 31.6304 47.3661 37.9893 42.6777 42.6777C37.9893 47.3661 31.6304 50 25 50C18.3696 50 12.0107 47.3661 7.32233 42.6777C2.63392 37.9893 0 31.6304 0 25C0 18.3696 2.63392 12.0107 7.32233 7.32233C12.0107 2.63392 18.3696 0 25 0C31.6304 0 37.9893 2.63392 42.6777 7.32233C47.3661 12.0107 50 18.3696 50 25V25ZM26.5625 14.0625C26.5625 13.6481 26.3979 13.2507 26.1049 12.9576C25.8118 12.6646 25.4144 12.5 25 12.5C24.5856 12.5 24.1882 12.6646 23.8951 12.9576C23.6021 13.2507 23.4375 13.6481 23.4375 14.0625V23.4375H14.0625C13.6481 23.4375 13.2507 23.6021 12.9576 23.8951C12.6646 24.1882 12.5 24.5856 12.5 25C12.5 25.4144 12.6646 25.8118 12.9576 26.1049C13.2507 26.3979 13.6481 26.5625 14.0625 26.5625H23.4375V35.9375C23.4375 36.3519 23.6021 36.7493 23.8951 37.0424C24.1882 37.3354 24.5856 37.5 25 37.5C25.4144 37.5 25.8118 37.3354 26.1049 37.0424C26.3979 36.7493 26.5625 36.3519 26.5625 35.9375V26.5625H35.9375C36.3519 26.5625 36.7493 26.3979 37.0424 26.1049C37.3354 25.8118 37.5 25.4144 37.5 25C37.5 24.5856 37.3354 24.1882 37.0424 23.8951C36.7493 23.6021 36.3519 23.4375 35.9375 23.4375H26.5625V14.0625Z" fill="#1C84EE" /></svg></div>
                                        </div>
                                    </div>
                                    <div className="timeinputcnt" id="initial_plan_clone_Sat">
                                        <p className="profile__form__smalltext">Sat</p>
                                        <div className="row m-0" id="initial_plan_parent_Sat">
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div onClick={(e) => this.handleClickAddTimeSat(e)} style={{ "display": "flex", "justifyContent": "end" }} className="col-md-2 col-sm-2 col-2 ps-0 p-mob-0"><svg style={{ marginTop: "15px" }} width="20" height="20" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50 25C50 31.6304 47.3661 37.9893 42.6777 42.6777C37.9893 47.3661 31.6304 50 25 50C18.3696 50 12.0107 47.3661 7.32233 42.6777C2.63392 37.9893 0 31.6304 0 25C0 18.3696 2.63392 12.0107 7.32233 7.32233C12.0107 2.63392 18.3696 0 25 0C31.6304 0 37.9893 2.63392 42.6777 7.32233C47.3661 12.0107 50 18.3696 50 25V25ZM26.5625 14.0625C26.5625 13.6481 26.3979 13.2507 26.1049 12.9576C25.8118 12.6646 25.4144 12.5 25 12.5C24.5856 12.5 24.1882 12.6646 23.8951 12.9576C23.6021 13.2507 23.4375 13.6481 23.4375 14.0625V23.4375H14.0625C13.6481 23.4375 13.2507 23.6021 12.9576 23.8951C12.6646 24.1882 12.5 24.5856 12.5 25C12.5 25.4144 12.6646 25.8118 12.9576 26.1049C13.2507 26.3979 13.6481 26.5625 14.0625 26.5625H23.4375V35.9375C23.4375 36.3519 23.6021 36.7493 23.8951 37.0424C24.1882 37.3354 24.5856 37.5 25 37.5C25.4144 37.5 25.8118 37.3354 26.1049 37.0424C26.3979 36.7493 26.5625 36.3519 26.5625 35.9375V26.5625H35.9375C36.3519 26.5625 36.7493 26.3979 37.0424 26.1049C37.3354 25.8118 37.5 25.4144 37.5 25C37.5 24.5856 37.3354 24.1882 37.0424 23.8951C36.7493 23.6021 36.3519 23.4375 35.9375 23.4375H26.5625V14.0625Z" fill="#1C84EE" /></svg></div>
                                        </div>
                                    </div>
                                    <div className="timeinputcnt" id="initial_plan_clone_Sun">
                                        <p className="profile__form__smalltext">Sun</p>
                                        <div className="row m-0" id="initial_plan_parent_Sun">
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-5 ps-0 p-mob-0">
                                                <div className="input-floating-label">
                                                    <input refs="" type="text" className="textbox--primary textbox--rounded input" name="Time" placeholder=" " onChange={this.timeChanges} onFocus={(e) => (e.target.type = "time")} onBlur={(e) => (this.handleBlur(e))} />
                                                    <label >Select time</label>
                                                </div>
                                            </div>
                                            <div onClick={(e) => this.handleClickAddTimeSun(e)} style={{ "display": "flex", "justifyContent": "end" }} className="col-md-2 col-sm-2 col-2 ps-0 p-mob-0"><svg style={{ marginTop: "15px" }} width="20" height="20" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50 25C50 31.6304 47.3661 37.9893 42.6777 42.6777C37.9893 47.3661 31.6304 50 25 50C18.3696 50 12.0107 47.3661 7.32233 42.6777C2.63392 37.9893 0 31.6304 0 25C0 18.3696 2.63392 12.0107 7.32233 7.32233C12.0107 2.63392 18.3696 0 25 0C31.6304 0 37.9893 2.63392 42.6777 7.32233C47.3661 12.0107 50 18.3696 50 25V25ZM26.5625 14.0625C26.5625 13.6481 26.3979 13.2507 26.1049 12.9576C25.8118 12.6646 25.4144 12.5 25 12.5C24.5856 12.5 24.1882 12.6646 23.8951 12.9576C23.6021 13.2507 23.4375 13.6481 23.4375 14.0625V23.4375H14.0625C13.6481 23.4375 13.2507 23.6021 12.9576 23.8951C12.6646 24.1882 12.5 24.5856 12.5 25C12.5 25.4144 12.6646 25.8118 12.9576 26.1049C13.2507 26.3979 13.6481 26.5625 14.0625 26.5625H23.4375V35.9375C23.4375 36.3519 23.6021 36.7493 23.8951 37.0424C24.1882 37.3354 24.5856 37.5 25 37.5C25.4144 37.5 25.8118 37.3354 26.1049 37.0424C26.3979 36.7493 26.5625 36.3519 26.5625 35.9375V26.5625H35.9375C36.3519 26.5625 36.7493 26.3979 37.0424 26.1049C37.3354 25.8118 37.5 25.4144 37.5 25C37.5 24.5856 37.3354 24.1882 37.0424 23.8951C36.7493 23.6021 36.3519 23.4375 35.9375 23.4375H26.5625V14.0625Z" fill="#1C84EE" /></svg></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p className="profile__form__sub-title">Already working with students ?</p>
                                <div className="col-12 p-0 mb-4">
                                    <Select placeholder="Email" className="goals__form__select mb-3" classNamePrefix="mySelect" options={options2} isMulti closeMenuOnSelect={true} isClearable={false} components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} styles={{ container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }), placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 }) }} />
                                </div>
                            </div>
                        </Form>
                    </div>
                </div>
                <div className="footer">
                    <div className="row m-0">
                        <div className="footer__left col-md-4 col-sm-4 col-4 text-center p-0">
                            <Link to="/onboarding/roles">
                            <p className="footer__left__cta"><svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M5.80474 0.195262C5.54439 -0.0650874 5.12228 -0.0650874 4.86193 0.195262L0.195262 4.86193C-0.0650873 5.12228 -0.0650873 5.54439 0.195262 5.80474L4.86193 10.4714C5.12228 10.7318 5.54439 10.7318 5.80474 10.4714C6.06509 10.2111 6.06509 9.78894 5.80474 9.52859L1.60948 5.33333L5.80474 1.13807C6.06509 0.877722 6.06509 0.455612 5.80474 0.195262Z" fill="#1B1C1E" /></svg>
                                <span>Go back</span>
                            </p>
                            </Link>
                        </div>
                        <div className="footer__center col-md-4 col-sm-4 col-4 text-center"><p>1 out of 3 steps</p></div>
                        <div className="footer__right col-md-4 col-sm-4 col-4 text-center">
                            <Link className="footer__cta" to="#" onClick={(e)=>this.handleValidation()} >Next step</Link>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
const mapStateToProps = ({ auth, commonData }) => {
    const { message, errorList, country, status } = commonData
    const { userProfile, userProfileData } = auth;
    return { message, errorList, country, status, userProfile, userProfileData }
};
export default connect(mapStateToProps, { getUserProfile, getCountry, getStatus })(withRouter(Index));