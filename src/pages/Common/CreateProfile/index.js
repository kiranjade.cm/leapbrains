import React, { Component } from "react";
import "../../../assets/css/profile.less";
import AdvisorProfile from "./AdvisorProfile"
import StudentProfile from "./StudentProfile"
import ParentProfile from "./ParentProfile";
import { getUserProfile } from "../../../redux/actions/Auth"
import { connect } from "react-redux";
import { withRouter } from "../../../redux/store/navigate";
import { getUserRoles } from "../../../redux/actions/Common";
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            student: false,
            advisor: false,
            parent: false,
            characters: ""
        };
    }
    componentDidMount() {
        this.props.getUserRoles();
        this.props.getUserProfile();
        console.log(this.props.userProfile);
        let Character = this.props.userProfile.currentRole
        this.setState({ characters: this.props.userProfile.currentRole })
        console.log(Character)
        if (Character === "student") {
            this.setState({
                student: true,
                advisor: false,
                parent: false,
            });
        }
        else if (Character === "advisor") {
            this.setState({
                student: false,
                advisor: true,
                parent: false,
            });
        }
        else if (Character === "parent") {
            this.setState({
                student: false,
                advisor: false,
                parent: true,
            });
        }
        else if (Character === "" || undefined) {
            window.history.go(-1)
        }
    }
    componentDidUpdate() {
        if (this.state.characters == null || "" || undefined) {
            window.history.go(-1)
        }
    }
    render() {
        let { roles, student, advisor,parent } = this.state;
        return (
            <>
                {student &&
                    <StudentProfile />
                }
                {advisor &&

                    <AdvisorProfile />
                }
                {parent &&
                    <ParentProfile />
                }
                
            </>
        );
    }
}

const mapStateToProps = ({ auth, commonData }) => {
    const { message, errorList } = commonData
    const { userProfile } = auth;
    return { message, errorList, userProfile }
};

export default connect(mapStateToProps, { getUserRoles, getUserProfile })(withRouter(Index));
