import React, { Component } from "react";
import "../../../../../assets/css/profile.less";
import { Link } from "react-router-dom";
import Select, { components } from "react-select";
import { connect } from "react-redux";
import { getGender, getStatus, getCountry } from "../../../../../redux/actions/Common";
import { setUserProfile, getUserProfile } from "../../../../../redux/actions/Auth";
import { NotificationContainer } from 'react-notifications';
import { withRouter } from "../../../../../redux/store/navigate";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            fields: {},
            errors: {},
            selectedGenderOption: null,
            selectedStatusOption: null,
            selectedCountryOption: null,
            studentsCollabCheckBox: false,
            counselingAllowedCheckBox: false,
            subCouncelingServicesCheckBox: false,
        };
    }

    handleChange(field, e) {
        let { errors } = this.state;
        let fields = this.state.fields;
        fields[field] = e.target.value;
        if (e.target.value.length >= 0) {
            errors[field] = "";
        }
        this.setState({ fields, errors: errors });
    }

    handleCheckboxChange(e, buttonName) {
        let { studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox } = this.state;
        if (buttonName === "studentsCollab") {
            studentsCollabCheckBox = e.target.checked
        } else if (buttonName === "counselingAllowed") {
            counselingAllowedCheckBox = e.target.checked
        } else if (buttonName === "subCouncelingServices") {
            subCouncelingServicesCheckBox = e.target.checked
        }
        this.setState({ studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox })
    }

    handleSelectChange(options, name) {
        let { selectedGenderOption, selectedStatusOption, selectedCountryOption, errors } = this.state;
        switch (name) {
            case "gender":
                selectedGenderOption = options;
                errors["gender"] = "";
                break;
                case "status":
                    selectedStatusOption = options;
                    errors["status"] = "";
                    break;
            case "country":
                selectedCountryOption = options;
                errors["country"] = "";
                break;
        }
        this.setState({ selectedGenderOption, selectedStatusOption, selectedCountryOption, errors: errors });
    }

    handleValidation() {
        let { selectedGenderOption, selectedStatusOption, selectedCountryOption, } = this.state;
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        // console.log(fields)
        if (!fields["firstName"]) {
            formIsValid = false;
            errors["firstName"] = "First name cannot be empty";
        }

        if (!fields["lastName"]) {
            formIsValid = false;
            errors["lastName"] = "Last Name cannot be empty";
        }

        if (!fields["zipCode"]) {
            formIsValid = false;
            errors["zipCode"] = "Zip code cannot be empty";
        }

        if (selectedStatusOption === null) {
            formIsValid = false;
            errors["status"] = "Status cannot be empty";
        }

        if (!fields["graduationYear"]) {
            formIsValid = false;
            errors["graduationYear"] = "Graduation year cannot be empty";
        }

        if (!fields["currentGPA"]) {
            formIsValid = false;
            errors["currentGPA"] = "Current GPA cannot be empty";
        }

        if (!fields["highSchoolName"]) {
            formIsValid = false;
            errors["highSchoolName"] = "High school name cannot be empty";
        }
        if (!fields["intereset"]) {
            formIsValid = false;
            errors["intereset"] = "Intereset cannot be empty";

        }
        if (selectedCountryOption === null) {
            formIsValid = false;
            errors["country"] = "country cannot be empty";
        }
        // console.log(errors)
        this.setState({ errors: errors });
        return formIsValid;
    }

    handleNext(e) {
        e.preventDefault();
        if (this.handleValidation()) {
            console.log(fields)
            let fields = this.state.fields;
            console.log(fields)
            let { studentsCollabCheckBox, counselingAllowedCheckBox, subCouncelingServicesCheckBox, selectedGenderOption, selectedStatusOption,selectedCountryOption } = this.state;
            let values = {
                firstName: fields["firstName"],
                lastName: fields["lastName"],
                graduationYear: fields["graduationYear"],
                zipCode: fields["zipCode"],
                country: selectedCountryOption.label,
                Interestedin: fields["Interestedin"],
                status: selectedStatusOption.label            
            }
        }
    };

    componentDidMount() {
        this.props.getStatus();
        this.props.getCountry();
    }

    componentDidUpdate() {

    }

    render() {
        const { status, gender, country } = this.props;
        let statusOptions = [];
        let countryOptions = [];
        let graduationOptions = [
            { label: 2022, value: 2022 },
            { label: 2023, value: 2023 },
            { label: 2024, value: 2024 },
            { label: 2025, value: 2025 },
            { label: 2026, value: 2026 },
            { label: 2027, value: 2027 },
            { label: 2028, value: 2028 },
            { label: 2029, value: 2029 },
            { label: 2030, value: 2030 },
            { label: 2031, value: 2031 },
            { label: 2032, value: 2032 },
        ];
        let HighschoolOptions = [
            { label: "Public High Schools", value: "Public High Schools" },
            { label: "Charter High Schools", value: "Charter High Schools" },
            { label: "Magnet High Schools", value: "Magnet High Schools" },
            { label: "Carnegie Vanguard High School", value: "Carnegie Vanguard High School" },
            { label: "Gilbert Classical Academy", value: "Gilbert Classical Academy" },
        ];
        let interesetOptions = [
            { label: "Proffesion", value: "Proffesion" },
            { label: "Engineering", value: "Engineering" },
            { label: "Developer", value: "Developer" },
        ];
        country !== undefined && country.length > 0 && country.map((x, key) => {
            var temp = { label: x.countryName, value: x.isoCode2Digit }
            countryOptions.push(temp);
        })
        status !== undefined && status.length > 0 && status.map((x, key) => {
            var temp = { label: x.status, value: x.id }
            statusOptions.push(temp);
        });
        const { ValueContainer, Placeholder } = components;
        const CustomValueContainer = ({ children, ...props }) => {
            return (
                <ValueContainer {...props}>
                    <Placeholder {...props} isFocused={props.isFocused}>
                        {props.selectProps.placeholder}
                    </Placeholder>
                    {React.Children.map(children, child => child && child.type !== Placeholder ? child : null)}
                </ValueContainer>
            );
        };
        const styles = { placeholder: (base, state) => ({ ...base, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block', }), }
        return (
            <>
                <div className="profile">
                    <h1 className="profile__form__title text-center mt-4">Create Profile for your child</h1>
                    <div className="childprofile__form">
                        <p className="profile__form__sub-title">General</p>
                        <div className="row m-0">
                            <div className="col-md-6 col-sm-6 col-12 ps-0 p-mob-0">
                                <div className="input-floating-label">
                                    <input refs="firstName" type="text" className={"textbox--primary textbox--rounded input"}
                                        name="firstName" placeholder="First Name" onChange={this.handleChange.bind(this, "firstName")}
                                    />
                                    <label>First Name</label>
                                    {this.state.errors["firstName"] && <span className="error-text">{this.state.errors["firstName"]}</span>}
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-6 col-12 pe-0 p-mob-0">
                                <div className="input-floating-label">
                                    <input refs="lastName" type="text" className={"textbox--primary textbox--rounded input"} name="lastName" placeholder="Last Name"
                                        onChange={this.handleChange.bind(this, "lastName")}
                                    />
                                    <label>Last Name</label>
                                    {this.state.errors["lastName"] && <span className="error-text">{this.state.errors["lastName"]}</span>}
                                </div>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className="col-md-6 col-sm-6 col-12 ps-0 p-mob-0">
                                <div className="input-floating-label">
                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="country" options={countryOptions} closeMenuOnSelect={true}
                                        isClearable={false} onChange={(value) => this.handleSelectChange(value, 'country')} placeholder="Country"
                                        components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }}
                                        styles={{
                                            container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                            valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                            placeholder: (provided, state) => ({
                                                ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff',
                                                paddingLeft: 10, paddingRight: 10, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block',
                                            })
                                        }} />
                                    {this.state.errors["country"] && <span className="error-text">{this.state.errors["country"]}</span>}
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-6 col-12 pe-0 p-mob-0">
                                <div className="input-floating-label">
                                    <input refs="zipCode" type="text" className={"textbox--primary textbox--rounded input"} name="zipCode"
                                        placeholder="Zip code" onChange={this.handleChange.bind(this, "zipCode")} maxLength={9}
                                        onKeyPress={(event) => { if (!/[0-9-]/.test(event.key)) { event.preventDefault(); } }} />
                                    <label>Zip code</label>
                                    {this.state.errors["zipCode"] && <span className="error-text">{this.state.errors["zipCode"]}</span>}
                                </div>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className="col-md-12 col-sm-12 col-12 p-0">
                                <div className="input-floating-label">
                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="status" options={statusOptions} closeMenuOnSelect={true}
                                        isClearable={false} onChange={(value) => this.handleSelectChange(value, 'status')} placeholder="My child’s status"
                                        components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }}
                                        styles={{
                                            container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                            valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                            placeholder: (provided, state) => ({
                                                ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff',
                                                paddingLeft: 10, paddingRight: 10, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block',
                                            })
                                        }} />
                                </div>
                                {this.state.errors["status"] && <span className="error-text">{this.state.errors["status"]}</span>}
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className="col-md-12 col-sm-12 col-12 p-0">
                                <div className="input-floating-label">
                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="graduationYear" options={graduationOptions} closeMenuOnSelect={true}
                                        isClearable={false} onChange={(value) => this.handleSelectChange(value, 'graduationYear')}
                                        components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="Graduation Year"
                                        styles={{
                                            container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                            valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                            placeholder: (provided, state) => ({
                                                ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10
                                            })
                                        }} />
                                    {this.state.errors["graduationYear"] && <span className="error-text">{this.state.errors["graduationYear"]}</span>}
                                </div>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className="col-md-12 col-sm-12 col-12 p-0">
                                <div className="input-floating-label">
                                    <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="highSchoolName" options={HighschoolOptions} closeMenuOnSelect={true}
                                        isClearable={false} onChange={(value) => this.handleSelectChange(value, 'highSchoolName')}
                                        components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }} placeholder="High School Name"
                                        styles={{
                                            container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                            valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                            placeholder: (provided, state) => ({
                                                ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10
                                            })
                                        }} />
                                    {this.state.errors["highSchoolName"] && <span className="error-text">{this.state.errors["highSchoolName"]}</span>}
                                </div>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className="col-md-12 col-sm-12 col-12 p-0">
                                <div className="input-floating-label">
                                    <input refs="currentGPA" type="text" className={"textbox--primary textbox--rounded input"} name="currentGPA"
                                        placeholder="Current GPA" maxLength={5} onChange={this.handleChange.bind(this, "currentGPA")}
                                        onKeyPress={(event) => { if (!/[0-9.]/.test(event.key)) { event.preventDefault(); } }} />
                                    <label>Current GPA</label>
                                    {this.state.errors["currentGPA"] && <span className="error-text">{this.state.errors["currentGPA"]}</span>}
                                </div>
                            </div>
                        </div>
                        <div style={{ "marginBottom": "100px" }}>
                            <p className="profile__form__sub-title">Interests</p>
                            <div className="row m-0">
                                <div className="col-md-12 col-sm-12 col-12 p-0">
                                    <div className="input-floating-label">
                                        <Select className="goals__form__select mb-3" classNamePrefix="mySelect" name="intereset" options={interesetOptions} closeMenuOnSelect={true}
                                            isClearable={false} onChange={(value) => this.handleSelectChange(value, 'intereset')} placeholder="Interested in"
                                            components={{ ValueContainer: CustomValueContainer, IndicatorSeparator: () => null }}
                                            styles={{
                                                container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                                valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                                placeholder: (provided, state) => ({
                                                    ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff',
                                                    paddingLeft: 10, paddingRight: 10, display: state.isFocused || state.isSelected || state.selectProps.inputValue ? 'none' : 'block',
                                                })
                                            }} />
                                    </div>
                                    {this.state.errors["intereset"] && <span className="error-text">{this.state.errors["intereset"]}</span>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <div className="row m-0">
                        <div className="footer__left col-md-4 col-sm-4 col-4 text-center p-0">
                            <p>
                                <Link to="/onboarding/spouseinvite" className="footer__left__cta">
                                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" clipRule="evenodd" d="M5.80474 0.195262C5.54439 -0.0650874 5.12228 -0.0650874 4.86193 0.195262L0.195262 4.86193C-0.0650873 5.12228 -0.0650873 5.54439 0.195262 5.80474L4.86193 10.4714C5.12228 10.7318 5.54439 10.7318 5.80474 10.4714C6.06509 10.2111 6.06509 9.78894 5.80474 9.52859L1.60948 5.33333L5.80474 1.13807C6.06509 0.877722 6.06509 0.455612 5.80474 0.195262Z" fill="#1B1C1E" />
                                    </svg>
                                    <span>Go back</span>
                                </Link>
                            </p>
                        </div>
                        <div className="footer__center col-md-4 col-sm-4 col-4 text-center">
                            <p>5 out of 9 steps</p>
                        </div>
                        <div className="footer__right col-md-4 col-sm-4 col-4 text-center">
                            <Link className="footer__cta" to="/onboarding/invite/child"> Next step</Link>
                        </div>
                    </div>
                </div>
                {/* <NotificationContainer /> */}
            </>
        );
    }
}

const mapStateToProps = ({ auth, commonData }) => {
    const { message, errorList, status, gender, country } = commonData
    const { isProfileSuccess } = auth;
    const { userProfile } = auth;
    return { message, errorList, status, gender, isProfileSuccess, userProfile, country }
};

export default connect(mapStateToProps, { getGender, getStatus, setUserProfile, getUserProfile, getCountry })(withRouter(Index));