import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../../../assets/css/role.less";
import { connect } from "react-redux";
import { getUserRoles } from "../../../redux/actions/Common";
import { setUserRole, getUserProfile } from "../../../redux/actions/Auth";
import { NotificationContainer } from 'react-notifications';
import { withRouter } from "../../../redux/store/navigate";

const IDENTIFIER = process.env.REACT_APP_IDENTIFIER;

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userRoles: [],
      selected: "",
      fields: {},
      errors: {},
      student: false,
      advisor: false,
      parent: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.props.getUserRoles();
    this.props.getUserProfile();
    if (this.props.isRoleSuccess && this.props.userProfile) {
      this.setState({ selected: this.props.userProfile.currentRole ? this.props.userProfile.currentRole : "" })
    }
    if (this.props.userProfile != undefined) {
      let currentRoles =  this.props.userProfile.currentRole
      this.setState({ selected: currentRoles ? currentRoles : "" })
      if (currentRoles === "student") {
        this.setState({ student: true, advisor: false, parent: false });
      }
      else if (currentRoles === "advisor") {
        this.setState({ advisor: true, parent: false, student: false });
      }
      else if (currentRoles === "parent") {
        this.setState({ parent: true, student: false, advisor: false });
      }
      let values = {
        currentRole: this.props.userProfile.currentRole
      }
      this.props.setUserRole(values)
    }
  }

  handleChange(event) {
    let selectedOption = event.target.value;
    console.log(selectedOption);
    if (selectedOption === "student") {
      this.setState({ student: true, advisor: false, parent: false });
    }
    else if (selectedOption === "advisor") {
      this.setState({ advisor: true, parent: false, student: false });
    }
    else if (selectedOption === "parent") {
      this.setState({ parent: true, student: false, advisor: false });
    }
    this.setState({
      selected: selectedOption
    });
    let values = {
      currentRole: selectedOption
    }
    this.props.setUserRole(values)
  }

  handleValidation() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!this.state.selected) {
      formIsValid = false;
      errors["role"] = "Please select any role";
    }
    console.log(errors)
    this.setState({ errors: errors });
    return formIsValid;
  }

  handleNext(e) {
    if (this.handleValidation()) {
      console.log(this.state.selected);
      if (this.props.isRoleSuccess && this.state.selected == "student") {
        this.props.navigate('/onboarding/goals');
        e.preventDefault();
      }
      else if (this.props.isRoleSuccess && this.state.selected == "advisor") {
        this.props.navigate('/onboarding/profile');
        e.preventDefault();
      }
      else if (this.props.isRoleSuccess && this.state.selected == "parent") {
        this.props.navigate('/onboarding/goals/child');
        e.preventDefault();
      }
    }
  }
  render() {
    let { selected, student, advisor, parent } = this.state;
    const roles = this.props.userRoles;

    const roleselected = selected.charAt(0).toUpperCase() + selected.slice(1);
    let listItem = roles !== undefined && roles.length > 0 && roles.map((x, key) => {
      const rolesName = x.charAt(0).toUpperCase() + x.slice(1);
      return (
        <div className="mb-2 role__btns ">
          <input type="radio" className="btn-check" key={rolesName} id={rolesName} autoComplete="off" name="options" value={x}
            onChange={e => this.handleChange(e)} checked={roleselected === rolesName ? true : false} />
          <label className="btn cta--role--btn w-mob-100" htmlFor={rolesName}>I’m a {rolesName}</label>
        </div>
      );
    });

    return (
      <>
        <div className="role">
          <div className="role_card">
            <h1 className="role_title text-center">Select your role</h1>
            <p className="role_desc text-center bluetext">Choose one option as you want</p>
            <div className="roles_page">
              {listItem}
            </div>
          </div>
          {this.state.errors["role"] && <span className="error-text text-center" style={{ display: "table", margin: "0 auto" }}>{this.state.errors["role"]}</span>}
        </div>
        <div className="footer">
          <div className="row m-0">
            <div className="footer__left col-md-4 col-sm-4 col-4 text-center p-0">
              <p>
                <Link to="/onboarding/roles" className="footer__left__cta">
                  <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd" d="M5.80474 0.195262C5.54439 -0.0650874 5.12228 -0.0650874 4.86193 0.195262L0.195262 4.86193C-0.0650873 5.12228 -0.0650873 5.54439 0.195262 5.80474L4.86193 10.4714C5.12228 10.7318 5.54439 10.7318 5.80474 10.4714C6.06509 10.2111 6.06509 9.78894 5.80474 9.52859L1.60948 5.33333L5.80474 1.13807C6.06509 0.877722 6.06509 0.455612 5.80474 0.195262Z" fill="#1B1C1E" />
                  </svg>
                  <span>Go back</span>
                </Link>
              </p>
            </div>
            <div className="footer__center col-md-4 col-sm-4 col-4 text-center">
              <p>2 out of 5 steps</p>
            </div>
            <div className="footer__right col-md-4 col-sm-4 col-4 text-center">
              <Link className="footer__cta" to="#" onClick={e => this.handleNext(e)}>Next step</Link>
            </div>
          </div>
        </div>
        <div className="footer">
          <div className="row m-0">
            <div className="footer__left col-md-4 col-sm-4 d-sm-block d-none p-0">
            </div>
            <div className="footer__center col-md-4 col-sm-4 col-6 text-center p-0">
              {student &&
                <p>1 out of 5 steps</p>
              }
              {advisor &&
                <p>1 out of 3 steps</p>
              }
              {parent &&
                <p>1 out of 9 steps</p>
              }
            </div>
            <div className="footer__right col-md-4 col-sm-4 col-6 text-center p-0">
              <Link className="footer__cta" to="#" onClick={e => this.handleNext(e)}>Next step</Link>
            </div>
          </div>
        </div>
        {/* <NotificationContainer /> */}
      </>
    );
  }
}

const mapStateToProps = ({ auth, commonData }) => {
  const { message, errorList, userRoles } = commonData
  const { isRoleSuccess } = auth;
  const { userProfile } = auth;
  return { message, errorList, userRoles, isRoleSuccess, userProfile }
};

export default connect(mapStateToProps, { getUserRoles, setUserRole, getUserProfile })(withRouter(Index));