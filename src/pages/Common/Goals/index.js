import React, { Component } from "react";
import { Link } from "react-router-dom";
import Select, { components } from "react-select";
import "../../../assets/css/common.less";
import { connect } from "react-redux";
import { getUniversities, getProfessions } from "../../../redux/actions/Common";
import { setUserGoals, getUserProfile, setIsGoalSuccess } from "../../../redux/actions/Auth";
import { Form } from "react-bootstrap";
import { NotificationContainer } from 'react-notifications';
import { withRouter } from "../../../redux/store/navigate";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            universityCheckBox: false,
            professionsCheckBox: false,
            gPACheckBox: false,
            fields: {},
            errors: {},
            selectedUniversityOption: null,
            selectedProfessionOption: null
        };
    }

    handleCheckboxChange(e, buttonName) {
        let { universityCheckBox, professionsCheckBox, gPACheckBox } = this.state;
        console.log('value of checkbox : ' + buttonName, e.target.checked);
        if (buttonName === "universities") {
            universityCheckBox = e.target.checked
        } else if (buttonName === "profession") {
            professionsCheckBox = e.target.checked
        } else if (buttonName === "GPA") {
            gPACheckBox = e.target.checked
        }
        this.setState({ universityCheckBox, professionsCheckBox, gPACheckBox })
    }

    handleChange(field, e) {
        let { errors } = this.state;
        let SelectedFields = this.state.fields;
        SelectedFields[field] = e.target.value;
        if (e.target.value.length >= 0) {
            errors[field] = "";
        }
        this.setState({ SelectedFields, errors: errors });
    }

    handleSelectChange(options, name) {
        let { selectedUniversityOption, selectedProfessionOption } = this.state;
        switch (name) {
            case "university":
                selectedUniversityOption = options;
                break;
            case "professions":
                selectedProfessionOption = options;
                break;
        }
        this.setState({ selectedUniversityOption, selectedProfessionOption });
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        let { selectedUniversityOption, selectedProfessionOption } = this.state;

        if (!this.state.universityCheckBox && !this.state.professionsCheckBox && !this.state.gPACheckBox) {
            formIsValid = false;
            errors["goal"] = "Please select any goal";
        }
        if (this.state.universityCheckBox && selectedUniversityOption === null) {
            formIsValid = false;
            errors["university"] = "Please select any university";
        }
        if (this.state.professionsCheckBox && selectedProfessionOption === null) {
            formIsValid = false;
            errors["profession"] = "Please select any profession";
        }
        if (this.state.gPACheckBox && fields["GPA"] === undefined) {
            formIsValid = false;
            errors["GPA"] = "Please enter GPA";
        }
        if (this.state.gPACheckBox &&  typeof fields["GPA"] !== "undefined") {
            const decimalcaseRegExp = (/^(\d|4)\.\d{2}$/);
            const floatcasenumber = decimalcaseRegExp.test(fields["GPA"]);
            if (!floatcasenumber) {
                formIsValid = false;
                errors["GPA"] = "GPA should be float ( example : 8.55 )";
            }
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    handleNext = async (e) => {
        this.setState({ loading: true })
        if (this.handleValidation()) {
            let goals = [];
            let fields = this.state.fields;
            let { selectedUniversityOption, selectedProfessionOption } = this.state;
            let gpaValue = fields["GPA"];
            if (this.state.universityCheckBox)
                goals.push("university");
            else
                selectedUniversityOption = [];

            if (this.state.professionsCheckBox)
                goals.push("profession");
            else
                selectedProfessionOption = [];

            if (this.state.gPACheckBox)
                goals.push("GPA");
            else
                gpaValue = "0.00";
            let selectedUniversity = [];
            selectedUniversityOption.forEach(element => {
                selectedUniversity.push(element.label);
            });
            let selectedProfession = [];
            selectedProfessionOption.forEach(element => {
                selectedProfession.push(element.label);
            });
            let values = {
                improveGpa: this.state.gPACheckBox,
                specificProfession: this.state.professionsCheckBox,
                specificUniversity: this.state.universityCheckBox,
                goalsSelected: goals,
                desiredGpa: gpaValue,
                interestedUniversities: selectedUniversity,
                interestedProfession: selectedProfession
            }
            console.log("goalsvalues",values);
            this.props.setUserGoals(values)
        }
    }

    componentDidMount() {
        let { universityCheckBox, professionsCheckBox, gPACheckBox, fields, selectedUniversityOption, selectedProfessionOption } = this.state
        this.props.getUniversities();
        this.props.getProfessions();
        console.log(this.props.getUserProfile())
        console.log(this.props.userProfileData)
        console.log(this.props.userProfileData)
        if (this.props.userProfileData != undefined) {
            let goalesData = this.props.userProfileData.goals
            if (goalesData && goalesData.length > 0) {
                goalesData && goalesData.map((data, key) => {
                    if (data == "university") {
                        universityCheckBox = true;
                        this.refs.universityCheckBox.checked = true;
                    } else if (data == "gpa") {
                        gPACheckBox = true;
                        this.refs.gPACheckBox.checked = true;
                    } else if (data == "profession") {
                        professionsCheckBox = true;
                        this.refs.professionsCheckBox.checked = true;
                    }
                })
            }
        }

        if (this.props.userProfileData != undefined) {
            console.log("userProfileData",this.props.userProfileData)
            // DATA VALUE MAPPING
            // GPA DATA
            let userProfileData = this.props.userProfileData;
            fields["GPA"] = userProfileData.desiredGpa ? userProfileData.desiredGpa : "";
            // University DATA
            let interestedUniversities = userProfileData.universities;
            let universityOptionstions = [];
            console.log("interestedUniversities",interestedUniversities)
            interestedUniversities !== undefined && interestedUniversities.length > 0 && interestedUniversities.map((university, key) => {
                var temp = { label: university, value: 1  }
                universityOptionstions.push(temp);
            });
            selectedUniversityOption = universityOptionstions;
            // Profession DATA
            let interestedProfession = userProfileData.professions;
            let professionOptionstions = [];
            interestedProfession !== undefined && interestedProfession.length > 0 && interestedProfession.map((profession, key) => {
                var temp = { label: profession,  value: 1  }
                professionOptionstions.push(temp);
                console.log(professionOptionstions)
            });
            selectedProfessionOption = professionOptionstions;
            // DATA VALUE MAPPING
        }

        this.setState({ universityCheckBox, professionsCheckBox, gPACheckBox, selectedUniversityOption, selectedProfessionOption });
    }

    componentDidUpdate(e) {
        if (this.props.isGoalSuccess) {
            this.props.navigate('/onboarding/profile');
        }
    }

    render() {
        const { universities, professions } = this.props;
        let { universityCheckBox, professionsCheckBox, gPACheckBox, selectedUniversityOption, selectedProfessionOption } = this.state;
        let universityOptionstions = [];
        let professionOptionstions = [];
        universities !== undefined && universities.length > 0 && universities.map((university, key) => {
            var temp = { label: university.university, value: university.id }
            universityOptionstions.push(temp);
        });
        professions !== undefined && professions.length > 0 && professions.map((profession, key) => {
            var temp = { label: profession.profession, value: profession.id }
            professionOptionstions.push(temp);
        });

        const { ValueContainer, Placeholder } = components;
        const CustomValueContainer = ({ children, ...props }) => {
            return (
                <ValueContainer {...props}>
                    <Placeholder {...props} isFocused={props.isFocused}>{props.selectProps.placeholder}</Placeholder>
                    {React.Children.map(children, child => child && child.type !== Placeholder ? child : null)}
                </ValueContainer>
            );
        };
        return (
            <>
                <div className="role">
                    <div className="role_card">
                        <h1 className="role_title text-center">What do you hope to gain from LeapBrains?</h1>
                        <p className="role_desc text-center bluetext">Choose as many options as you want.</p>
                        <div className="goals_page">
                            <div className="mb-2 role__btns">
                                <input type="checkbox" ref='universityCheckBox' defaultChecked={universityCheckBox} onClick={(e) => this.handleCheckboxChange(e, 'universities')} className="btn-check" name="options1" id="btncheck1" autoComplete="off" />
                                <label className="btn cta--role--btn w-mob-100" htmlFor="btncheck1">Get into specific universities</label>
                            </div>
                            <div className=" mb-2 role__btns">
                                <input type="checkbox" ref='professionsCheckBox' defaultChecked={professionsCheckBox} onClick={(e) => this.handleCheckboxChange(e, 'profession')} className="btn-check" name="options2" id="btncheck2" autoComplete="off" />
                                <label className="btn cta--role--btn w-mob-100" htmlFor="btncheck2">Get into a specific profession</label>
                            </div>
                            <div className=" mb-2 role__btns">
                                <input type="checkbox" ref='gPACheckBox' defaultChecked={gPACheckBox} onClick={(e) => this.handleCheckboxChange(e, 'GPA')} className="btn-check" name="options3" id="btncheck3" autoComplete="off" />
                                <label className="btn cta--role--btn w-mob-100" htmlFor="btncheck3">Improve my GPA</label>
                            </div>
                        </div>
                        {this.state.errors["goal"] && <span className="error-text text-center" style={{ display: "table", margin: "0 auto" }}>{this.state.errors["goal"]}</span>}
                        <Form className="login_card_form mb-5" autoComplete="off">
                            <div className="goals__form">
                                {universityCheckBox &&
                                    <div className="row">
                                        <div className="col-md-12 mb-2 role__btns">
                                            <Select className="goals__form__select mb-3" classNamePrefix="mySelect" options={universityOptionstions}
                                                closeMenuOnSelect={true} isClearable={false} isMulti components={{ ValueContainer: CustomValueContainer, DropdownIndicator: () => null, IndicatorSeparator: () => null }}
                                                placeholder="University" value={selectedUniversityOption} onChange={(value) => this.handleSelectChange(value, 'university')}
                                                styles={{
                                                    container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }),
                                                    valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                                    placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 })
                                                }} />
                                            {this.state.errors["university"] && <span className="error-text">{this.state.errors["university"]}</span>}
                                        </div>
                                    </div>
                                }
                                {professionsCheckBox &&
                                    <div className="row">
                                        <div className="col-md-12 mb-2 role__btns">
                                            <Select className="goals__form__select mb-3" classNamePrefix="mySelect" options={professionOptionstions}
                                                closeMenuOnSelect={true} isClearable={false} isMulti components={{ ValueContainer: CustomValueContainer, DropdownIndicator: () => null, IndicatorSeparator: () => null }}
                                                placeholder="Professions" value={selectedProfessionOption} onChange={(value) => this.handleSelectChange(value, 'professions')}
                                                styles={{
                                                    container: (provided, state) => ({ ...provided, height: '48px', overflow: "visible" }), valueContainer: (provided, state) => ({ ...provided, overflow: "visible", height: '100%', minHeight: '48px', }),
                                                    placeholder: (provided, state) => ({ ...provided, position: "absolute", top: state.hasValue || state.selectProps.inputValue ? -13 : "30%", fontSize: (state.hasValue || state.selectProps.inputValue) && 13, background: '#fff', paddingLeft: 10, paddingRight: 10 })
                                                }} />
                                            {this.state.errors["profession"] && <span className="error-text">{this.state.errors["profession"]}</span>}
                                        </div>
                                    </div>
                                }
                                {gPACheckBox &&
                                    <div className="row">
                                        <div className="col-md-12 mb-2">
                                            <div className="input-floating-label">
                                                <input refs="GPA" type="text" className={"textbox--primary textbox--rounded input"} name="GPA"
                                                    placeholder="GPA" onKeyPress={(event) => { if (!/[0-9.]/.test(event.key)) { event.preventDefault(); } }} onChange={this.handleChange.bind(this, "GPA")} value={this.state.fields["GPA"]} />
                                                <p className="cta--text mb-1">GPA should be float ( example : 4.55 ) and value should not exceed 9.99</p>
                                                <label>GPA</label>
                                                {this.state.errors["GPA"] && <span className="error-text">{this.state.errors["GPA"]}</span>}
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </Form>
                    </div>
                </div>
                <div className="footer">
                    <div className="row m-0">
                        <div className="footer__left col-md-4 col-sm-4 col-4 text-center p-0">
                            <p>
                                <Link to="/onboarding/roles" className="footer__left__cta">
                                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" clipRule="evenodd" d="M5.80474 0.195262C5.54439 -0.0650874 5.12228 -0.0650874 4.86193 0.195262L0.195262 4.86193C-0.0650873 5.12228 -0.0650873 5.54439 0.195262 5.80474L4.86193 10.4714C5.12228 10.7318 5.54439 10.7318 5.80474 10.4714C6.06509 10.2111 6.06509 9.78894 5.80474 9.52859L1.60948 5.33333L5.80474 1.13807C6.06509 0.877722 6.06509 0.455612 5.80474 0.195262Z" fill="#1B1C1E" />
                                    </svg>
                                    <span>Go back</span>
                                </Link>
                            </p>
                        </div>
                        <div className="footer__center col-md-4 col-sm-4 col-4 text-center">
                            <p>2 out of 5 steps</p>
                        </div>
                        <div className="footer__right col-md-4 col-sm-4 col-4 text-center">
                            <Link className="footer__cta" to="#" onClick={e => this.handleNext(e)}>Next step</Link>
                        </div>
                    </div>
                </div>
                <NotificationContainer />
            </>
        );
    }
}

const mapStateToProps = ({ auth, commonData }) => {
    const { message, errorList, universities, professions } = commonData
    const { isGoalSuccess } = auth;
    const { userProfile, userProfileData } = auth;
    return { message, errorList, universities, professions, isGoalSuccess, userProfile, userProfileData }
};

export default connect(mapStateToProps, { getUniversities, getProfessions, setUserGoals, getUserProfile, setIsGoalSuccess })(withRouter(Index));