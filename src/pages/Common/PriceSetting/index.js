import React, { Component } from "react";
import "../../../assets/css/profile.less";
import { Link } from "react-router-dom";
import Select, { components } from "react-select";
import { connect } from "react-redux";
import { NotificationContainer } from 'react-notifications';
import Dropzone from "react-dropzone";
import { Button, Form } from "react-bootstrap";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null,
            isShowPan: false,
            isShowActivity: false,
            names: "",
            highschool: true,
            university: false,
            proffesional: false,
            selectedOption: "highschool",
        };
    }
    handleShowPlan(e) {
        e.preventDefault()
        if (this.state.isShowPan == false) {
            this.setState({ isShowPan: true })
        }
        else {
            this.setState({ isShowPan: false })
        }
    }
    handleShowPlanActivity(e) {
        e.preventDefault()
        if (this.state.isShowActivity == false) {
            this.setState({ isShowActivity: true })
        }
        else {
            this.setState({ isShowActivity: false })
        }
    }
    handleDrop = acceptedFiles => {
        this.setState({ fileNames: acceptedFiles.map(file => file.name) })
    }

    handleChangeradio = (e) => {
        const names = e.currentTarget.value
        console.log(names)
        if (names === "highschool") {
            this.setState({ highschool: true, university: false, proffesional: false, selectedOption: "highschool" })
        }
        else if (names === "university") {
            this.setState({ university: true, highschool: false, proffesional: false, selectedOption: "university" })
        }
        else {
            this.setState({ university: false, highschool: false, proffesional: true, selectedOption: "proffesional" })
        }
    }

    render() {
        const { ValueContainer, Placeholder } = components;
        const CustomValueContainer = ({ children, ...props }) => {
            return (
                <ValueContainer {...props}>
                    <Placeholder {...props} isFocused={props.isFocused}>
                        {props.selectProps.placeholder}
                    </Placeholder>
                    {React.Children.map(children, child =>
                        child && child.type !== Placeholder ? child : null
                    )}
                </ValueContainer>
            );
        };
        let { isShowPan, isShowActivity, highschool, university, proffesional } = this.state;
        const options = [
            { value: '2021 (1-st semestre)', label: '2021 (1-st semestre)' },
            { value: '2021 (2-st semestre)', label: '2021 (2-st semestre)' },
            { value: '2022 (1-st semestre)', label: '2022 (1-st semestre)' },
            { value: '2022 (2-nd semestre)', label: '2022 (2-nd semestre)' },
            { value: '2023 (1-st semestre)', label: '2023 (1-st semestre)' },
            { value: '2023 (2-nd semestre)', label: '2023 (2-nd semestre)' }
        ]
        const options1 = [
            { value: '2021 (1-st semestre)', label: '2021 (1-st semestre)' },
            { value: '2021 (2-st semestre)', label: '2021 (2-st semestre)' },
            { value: '2022 (1-st semestre)', label: '2022 (1-st semestre)' },
            { value: '2022 (2-nd semestre)', label: '2022 (2-nd semestre)' },
            { value: '2023 (1-st semestre)', label: '2023 (1-st semestre)' },
            { value: '2023 (2-nd semestre)', label: '2023 (2-nd semestre)' }
        ]
        const options2 = [
            { value: 'Academic Regular', label: 'Academic Regular' },
            { value: 'Academic Accelerated', label: 'Academic Accelerated' },
            { value: 'Academic Honors', label: 'Academic Honors' },
            { value: 'Academic AP/College level', label: 'Academic AP/College level' },
            { value: 'Desired Grade', label: 'Desired Grade' }
        ]

        return (
            <>
                <div className="profile">
                    <div className="profile__form">
                        <h1 className="profile__form__title text-center">Pricing Settings</h1>
                        <p className="profile__form__sub-title">Create package</p>
                        <div className="generalcnt">
                            <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                <div class="input-floating-label">
                                    <input
                                        refs="Package Name"
                                        type="text"
                                        className="textbox--primary textbox--rounded input"
                                        name="Package Name"
                                        placeholder="Package Name"
                                    />
                                    <label>Package Name</label>
                                </div>
                            </div>
                            <div className="statuscnt">
                                <p className="profile__form__smalltext">Services</p>
                                <div className="profile__form__checkboxcnt mb-2">
                                    <div class="styled-radio-flex">
                                        <div class="styled-radio">
                                            <input id="serviceradio-1" name="radio" type="radio" value=""   />
                                            <label htmlFor="serviceradio-1" class="radio-label">One time service</label>
                                        </div>
                                        <div class="styled-radio">
                                            <input id="serviceradio-2" name="radio" type="radio" value="" />
                                            <label htmlFor="serviceradio-2" class="radio-label">Monthly</label>
                                        </div>
                                    </div>
                                </div>
                                <p className="profile__form__sub-title">Price</p>
                                <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                    <div class="input-floating-label">
                                        <input
                                            refs="$"
                                            type="text"
                                            className="textbox--primary textbox--rounded input"
                                            name="$"
                                            placeholder="$"
                                        />
                                        <label>$</label>
                                    </div>
                                </div>
                                <p className="profile__form__smalltext">Number of free consultation sessions <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" clipRule="evenodd" d="M7.80173 1.52344C4.33648 1.52344 1.52734 4.33258 1.52734 7.79783C1.52734 11.2631 4.33648 14.0722 7.80173 14.0722C11.267 14.0722 14.0761 11.2631 14.0761 7.79783C14.0761 4.33258 11.267 1.52344 7.80173 1.52344ZM0.0273438 7.79783C0.0273438 3.50415 3.50806 0.0234375 7.80173 0.0234375C12.0954 0.0234375 15.5761 3.50415 15.5761 7.79783C15.5761 12.0915 12.0954 15.5722 7.80173 15.5722C3.50806 15.5722 0.0273438 12.0915 0.0273438 7.79783ZM7.80078 7.04785C8.21499 7.04785 8.55078 7.38364 8.55078 7.79785V10.9198C8.55078 11.334 8.21499 11.6698 7.80078 11.6698C7.38657 11.6698 7.05078 11.334 7.05078 10.9198V7.79785C7.05078 7.38364 7.38657 7.04785 7.80078 7.04785ZM7.80078 3.92578C7.38657 3.92578 7.05078 4.26157 7.05078 4.67578C7.05078 5.08999 7.38657 5.42578 7.80078 5.42578H7.80928C8.22349 5.42578 8.55928 5.08999 8.55928 4.67578C8.55928 4.26157 8.22349 3.92578 7.80928 3.92578H7.80078Z" fill="#1B1C1E" fillOpacity="0.5" />
                                </svg>
                                </p>
                                <div className="profile__form__checkboxcnt mb-2">
                                    <div class="styled-radio-flex">
                                        <div class="styled-radio">
                                            <input id="radio-12" name="radio1" type="radio" value="1" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-12" class="radio-label">1</label>
                                        </div>

                                        <div class="styled-radio">
                                            <input id="radio-21" name="radio1" type="radio" value="21" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-21" class="radio-label">2</label>
                                        </div>

                                        <div class="styled-radio">
                                            <input id="radio-31" name="radio1" type="radio" value="31" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-31" class="radio-label">3</label>
                                        </div>
                                    </div>
                                </div>
                                <p className="profile__form__smalltext">Number of free paid sessions included in monthly fee </p>
                                <div className="profile__form__checkboxcnt mb-2">
                                    <div class="styled-radio-flex">
                                        <div class="styled-radio">
                                            <input id="radio-11" name="radio2" type="radio" value="11" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-11" class="radio-label">1</label>
                                        </div>

                                        <div class="styled-radio">
                                            <input id="radio-22" name="radio2" type="radio" value="22" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-22" class="radio-label">2</label>
                                        </div>

                                        <div class="styled-radio">
                                            <input id="radio-33" name="radio2" type="radio" value="33" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-33" class="radio-label">3</label>
                                        </div>
                                        <div class="styled-radio">
                                            <input id="radio-44" name="radio2" type="radio" value="44" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-44" class="radio-label">4</label>
                                        </div>
                                        <div class="styled-radio">
                                            <input id="radio-55" name="radio2" type="radio" value="55" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-55" class="radio-label">5</label>
                                        </div>
                                        <div class="styled-radio">
                                            <input id="radio-66" name="radio2" type="radio" value="66" onChange={this.handleChangeradio} />
                                            <label htmlFor="radio-66" class="radio-label">6</label>
                                        </div>
                                    </div>
                                </div>
                                <p className="profile__form__sub-title">Type of service included in monthly fee or one time fee</p>
                                <p className="">
                                    <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" onClick={(e) => this.handleCheckboxChange(e, 'studentsCollab')} />
                                    <label htmlFor="styled-checkbox-1">
                                        <span>In person 1:1 tutoring</span>
                                    </label>
                                </p>
                                <p className="">
                                    <input className="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" onClick={(e) => this.handleCheckboxChange(e, 'counselingAllowed')} />
                                    <label htmlFor="styled-checkbox-2">
                                        <span>Virtual 1:1 tutoring</span>
                                    </label>
                                </p>
                                <p className="">
                                    <input className="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value3" onClick={(e) => this.handleCheckboxChange(e, 'subCouncelingServices')} />
                                    <label htmlFor="styled-checkbox-3">
                                        <span>Course Milestone progress monitoring and feedback</span>
                                    </label>
                                </p>
                                <p className="">
                                    <input className="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value4" onClick={(e) => this.handleCheckboxChange(e, 'subCouncelingServices')} />
                                    <label htmlFor="styled-checkbox-4">
                                        <span>Initial course plan review and counceling</span>
                                    </label>
                                </p>
                                <p className="">
                                    <input className="styled-checkbox" id="styled-checkbox-5" type="checkbox" value="value5"  onClick={(e) => this.handleCheckboxChange(e, 'subCouncelingServices')} />
                                    <label htmlFor="styled-checkbox-5">
                                        <span>Other</span>
                                    </label>
                                </p>
                                <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                    <div class="input-floating-label">
                                        <input
                                            refs="Describe another type"
                                            type="text"
                                            className="textbox--primary textbox--rounded input"
                                            name="Describe another type"
                                            placeholder="Describe another type"
                                        />
                                        <label>Describe another type</label>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-12 ps-0 p-mob-0">
                                    <div className="input-floating-label">
                                        <Select
                                            className="goals__form__select mb-3"
                                            classNamePrefix="mySelect"
                                            name="status"
                                            options="green"
                                            closeMenuOnSelect={true}
                                            isClearable={false}
                                            onChange={(value) => this.handleSelectChange(value, 'status')}
                                            components={{
                                                ValueContainer: CustomValueContainer, IndicatorSeparator: () => null
                                            }}
                                            placeholder="Limits for number of messages exchanged"
                                            styles={{
                                                container: (provided, state) => ({
                                                    ...provided,
                                                    height: '48px',
                                                    overflow: "visible"
                                                }),
                                                valueContainer: (provided, state) => ({
                                                    ...provided,
                                                    overflow: "visible",
                                                    height: '100%',
                                                    minHeight: '48px',
                                                }),
                                                placeholder: (provided, state) => ({
                                                    ...provided,
                                                    position: "absolute",
                                                    top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13,
                                                    background: '#fff',
                                                    paddingLeft: 10,
                                                    paddingRight: 10
                                                })
                                            }}
                                        />
                                    </div>
                                </div>
                                <hr />
                                <div className="packagebtncnt">
                                    <div className="packagebtn">
                                        <span>+ Create new package</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <div className="row m-0">
                        <div className="footer__left col-md-4 col-sm-4 col-4 text-center p-0">
                            <p>
                                <Link to="/onboarding/profile" className="footer__left__cta">
                                    <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" clipRule="evenodd" d="M5.80474 0.195262C5.54439 -0.0650874 5.12228 -0.0650874 4.86193 0.195262L0.195262 4.86193C-0.0650873 5.12228 -0.0650873 5.54439 0.195262 5.80474L4.86193 10.4714C5.12228 10.7318 5.54439 10.7318 5.80474 10.4714C6.06509 10.2111 6.06509 9.78894 5.80474 9.52859L1.60948 5.33333L5.80474 1.13807C6.06509 0.877722 6.06509 0.455612 5.80474 0.195262Z" fill="#1B1C1E" />
                                    </svg>
                                    <span>Go back</span>
                                </Link>
                            </p>
                        </div>
                        <div className="footer__center col-md-4 col-sm-4 col-4 text-center">
                            <p>2 out of 3 steps</p>
                        </div>
                        <div className="footer__right col-md-4 col-sm-4 col-4 text-center">
                            <Link className="footer__cta" to="/onboarding/selectstudent" >Next step</Link>
                        </div>
                    </div>
                </div>
                <NotificationContainer />
            </>
        );
    }
}

export default Index;