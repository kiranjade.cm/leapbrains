import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Routes, Route, } from "react-router-dom";

//layout
import LoginLayout from './layouts/Login';
import DashboardLayout from './layouts/Dashboard';
import DefaultLayout from './layouts/Default';

// Auth pages
import Login from "./pages/Auth/Login";
import SignUp from "./pages/Auth/SignUp";

// common pages
import Roles from "./pages/Common/Roles";
import Goals from "./pages/Common/Goals";
import CreateProfile from "./pages/Common/CreateProfile";
import SendInvites from "./pages/Common/SendInvites";
import Plans from "./pages/Common/Plans";
import SelectAdvisor from "./pages/Common/SelectAdvisor";
import SelectFriend from "./pages/Common/SelectFriend";

// Student Pages
import Dashboard from "./pages/Student/Dashboard"
import MyWork from "./pages/Student/MyWork"

// import Signup from "./components/Pages/Common/Signup"
// import Roles from "./components/Pages/Common/Roles"
// import Verify from "./components/Pages/Common/Verify"
// import Notavailable from "./components/Pages/Common/404"
// import Newpassword from "./components/Pages/Common/Newpassword"
// import Resetpassword from "./components/Pages/Common/Resetpassword"
// Student Pages
// import Dashboard from "./components/Pages/StudentPages/Dashboard"
// import MyWork from "./components/Pages/StudentPages/MyWork"
// import Goals from "./components/Pages/StudentPages/Goals"
// import Newprofile from "./components/Pages/StudentPages/Createprofile"
// import Sendinvites from "./components/Pages/StudentPages/SendInvites"
// import Plans from "./components/Pages/StudentPages/Plans"
// import MyNetwork from "./components/Pages/StudentPages/MyNetwork"
// import SelectAdvisor from "./components/Pages/StudentPages/AdvisorPages/SelectAdvisor"
// import Schedule from "./components/Pages/StudentPages/Schedule"
// import SelectFriend from "./components/Pages/StudentPages/SelectFriend"
// import StudentNewplans from "./components/Pages/StudentPages/Plans/StudentNewplans"
// import FeedbackAdvisor from "./components/Pages/StudentPages/FeedbackAdvisor"
// Advisor pages
// import NewPlan from "./components/Pages/AdvisorPages/Plan/NewPlan"
// import PhaseOne from "./components/Pages/AdvisorPages/Plan/PhaseOne"
// import AdvisorDashboard from "./components/Pages/AdvisorPages/Dashboard"
class App extends Component {
    render() {
        return (
            <>
                <BrowserRouter>
                    <Routes>
                      <Route exact path= "/" element={<LoginLayout/>} >
                        <Route exact path="/" element={<Login/>} />
                        <Route exact path="/login" element={<Login/>} />
                        <Route exact path="/signup" element={<SignUp/>} />
                        <Route exact path="/roles" element={<Roles/>} />
                        <Route exact path="/goals" element={<Goals/>} />
                        <Route exact path="/student/newprofile" element={<CreateProfile/>} />
                        <Route exact path="/student/sendinvites" element={<SendInvites/>} />
                        <Route exact path="/student/plans" element={<Plans/>} />
                        <Route exact path="/student/selectadvisor" element={<SelectAdvisor/>} />
                        <Route exact path="/student/selectfriend" element={<SelectFriend/>} />
                      </Route>

                      <Route exact path= "/student" element={<DashboardLayout/>} >
                        <Route exact path="/student/dashboard" element={<Dashboard/>} />
                      </Route>

                      <Route exact path= "/" element={<DefaultLayout/>} >
                        <Route exact path="/student/mywork" element={<MyWork/>} />
                      </Route>
                      
                    </Routes>
                </BrowserRouter>
            </>
        );
    }
}

export default App;