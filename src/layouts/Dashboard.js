import React from "react";
import {Outlet} from "react-router-dom";
import "../assets/css/dashboard.less";
import Sidebar from "../layouts/Block/Sidebar";
import ChatBox from "../components/ChatBox";
import Calendar from "../components/Calendar";

class DashboardLayout extends React.Component {
    render(){
        return (
            <div>
                <div id="dashboard-layout">
                    <Sidebar/>
                    <div className="center-wrap">
                        <Outlet/>
                    </div>
                    <Calendar />
                    <ChatBox />
                </div>
            </div>
        )
    }
}
export default DashboardLayout;
