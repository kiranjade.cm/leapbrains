import React from "react";
import {Outlet} from "react-router-dom";
import Sidebar from "../layouts/Block/Sidebar";
import ChatBox from "../components/ChatBox";

class DashboardLayout extends React.Component {
    render(){
        return (
            <div>
                <div id="layoutSidenav">
                    <Sidebar/>
                    <div className="center-wrap-default">
                        <Outlet/>
                    </div>
                    <ChatBox />
                </div>
            </div>
        )
    }
}
export default DashboardLayout;
