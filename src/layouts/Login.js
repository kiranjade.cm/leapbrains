import React from "react"
import {Outlet} from "react-router-dom";
class LoginLayout extends React.Component {
    render(){
        return (
            <>
                <div className="grandParentContaniner">
                    <div className="parentContainer">
                        <Outlet/>
                    </div>
                </div>
            </>
        )
    }
}
export default LoginLayout;