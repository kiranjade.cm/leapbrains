import React from "react";
import { BrowserRouter, Routes, Route, Navigate, useLocation } from "react-router-dom";

import DashboardLayout from '../layouts/Dashboard';
import DefaultLayout from '../layouts/Default';

/*import Student pages */
import Dashboard from "../pages/Student/Dashboard";
import MyWork from "../pages/Student/MyWork";
import MyNetwork from "../pages/Student/MyNetwork";
import Library from "../pages/Student/Library";
import Schedule from "../pages/Student/Schedule";
import Notification from "../pages/Student/Notification";
import Profile from "../pages/Student/Profile"
import AddProfile from "../pages/Student/MyNetwork/Profile"
import Messenger from "../pages/Student/Messenger"


import { isUserAuthenticated } from "../utils/AuthUtils"

const RequireAuth = ({ children }) => {
  let location = useLocation();
  let user = isUserAuthenticated();
  if (!user) {
    return <Navigate to="/" state={{ from: location }} replace />;
  }
  return children;
}

const StudentRoutes = () => (
  <Routes>
    {/* STUDENT DASHBOARD */}
    <Route exact path="student" element={<DashboardLayout />} >
      <Route path="dashboard" element={<RequireAuth><Dashboard /></RequireAuth>} />
    </Route>
    {/* Student otherpages */}
    <Route exact path="student" element={<DefaultLayout />} >
      <Route path="mywork" element={<RequireAuth><MyWork /></RequireAuth>} />
      <Route path="mynetwork" element={<RequireAuth><MyNetwork /></RequireAuth>} />
      <Route path="library" element={<RequireAuth><Library /></RequireAuth>} />
      <Route path="schedule" element={<RequireAuth><Schedule /></RequireAuth>} />
      <Route path="notification" element={<RequireAuth><Notification /></RequireAuth>} />
      <Route path="myprofile" element={<RequireAuth><Profile /></RequireAuth>} />
      <Route path="profile" element={<RequireAuth><AddProfile /></RequireAuth>} />
      <Route path="messenger" element={<RequireAuth><Messenger /></RequireAuth>} />
    </Route>
  </Routes>
);

export default StudentRoutes;