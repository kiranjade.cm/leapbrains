import React from "react";
import { BrowserRouter, Routes, Route, Navigate, useLocation } from "react-router-dom";
import LoginLayout from '../layouts/Login';

// Auth pages
import Login from "../pages/Auth/Login";
import Signup from "../pages/Auth/SignUp";

import Notavailable from "../pages/Common/404"
import Newpassword from "../pages/Common/NewPassword";
import Resetpassword from "../pages/Common/ResetPassword";

import { RequireAuth } from "../utils/AuthUtils"

// const RequireAuth = (  {children} ) => {
//   let location = useLocation();
//   let user = isUserAuthenticated();
//   if (!user) {
//     return <Navigate to="/" state={{ from: location }} replace />;
//   }
//   else {
//     axios.defaults.headers.common['Authorization'] = "Bearer " + getUserData('accessToken');
//   }
//   return children;
// }

const CommonRoutes = () => (
  <Routes>
    {/* Common Routes */}
    <Route exact path="/" element={<LoginLayout />} >
      <Route path="/" element={<Login />} />
      <Route path="signup" element={<Signup />} />
      <Route path="notavailable" element={<Notavailable />} />
      <Route path="forgotpassword" element={<Newpassword />} />
      <Route path="/forgotpassword/verify/:key" element={<Resetpassword />} />
    </Route>
  </Routes>
);
export default CommonRoutes;