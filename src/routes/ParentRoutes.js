import React from "react";
import { BrowserRouter, Routes, Route, Navigate, useLocation } from "react-router-dom";
import { isUserAuthenticated } from "../utils/AuthUtils"
import DashboardLayout from '../layouts/Dashboard';
import DefaultLayout from '../layouts/Default';

/*import Student pages */
import Dashboard from "../pages/Parent/Dashboard";



const RequireAuth = ({ children }) => {
  let location = useLocation();
  let user = isUserAuthenticated();
  if (!user) {
    return <Navigate to="/" state={{ from: location }} replace />;
  }
  return children;
}

const ParentRoutes = () => (
  <Routes>
    {/* STUDENT DASHBOARD */}
    <Route exact path="parent" element={<DashboardLayout />} >
      <Route path="dashboard" element={<RequireAuth><Dashboard /></RequireAuth>} />
    </Route>
    {/* Student otherpages */}
    <Route exact path="parent" element={<DefaultLayout />} >
    </Route>
  </Routes>
);

export default ParentRoutes;