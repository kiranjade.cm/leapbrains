import React from "react";
import { BrowserRouter, Routes, Route, Navigate, useLocation } from "react-router-dom";
import Dashboard from "../pages/Advisor/Dashboard";
import DashboardLayout from "../layouts/Dashboard";
import DefaultLayout from "../layouts/Default"
import { RequireAuth } from "../utils/AuthUtils";

const AdvisorRoutes = () => (
  <Routes>
    {/* Advisor DASHBOARD */}
    <Route exact path="advisor" element={<DashboardLayout />} >
      <Route path="dashboard" element={<RequireAuth><Dashboard /></RequireAuth>} />
    </Route>
    {/* Advisor otherpages */}
    <Route exact path="advisor" element={<DefaultLayout />} >
      {/* <Route path="mywork" element={<RequireAuth><MyWork /></RequireAuth>} />
      <Route path="mynetwork" element={<RequireAuth><MyNetwork /></RequireAuth>} />
      <Route path="library" element={<RequireAuth><Library /></RequireAuth>} />
      <Route path="schedule" element={<RequireAuth><Schedule /></RequireAuth>} />
      <Route path="notification" element={<RequireAuth><Notification /></RequireAuth>} />
      <Route path="myprofile" element={<RequireAuth><Profile /></RequireAuth>} /> */}
    </Route>
  </Routes>

);

export default AdvisorRoutes;