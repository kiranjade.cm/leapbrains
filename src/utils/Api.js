import axios from 'axios';
import { refreshToken } from 'util/AuthUtils'

const BASE_URL = process.env.REACT_APP_BASE_URL;

const AxiosAPI = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  }
});

AxiosAPI.interceptors.response.use((response) => {
  // console.log(`response ${response}`);
  if(response.status === 401) {
    refreshToken();
  }
  return response;
}, (error) => {
  console.log(`Error: ${error}`);
  if(error.response && error.response.status===401) {
    refreshToken()
  }
  return Promise.reject(error);
});

export default AxiosAPI;