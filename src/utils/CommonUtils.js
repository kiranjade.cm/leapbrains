import moment from 'moment';
import { getUserData } from './AuthUtils';

const getYesterdayObj = () => {
    let date = new Date();
    return new Date(date.setDate(date.getDate() - 1));
}

const getTodayObj = () => {
    return new Date();
}

const formatDatetime = (date) => {
    return date ? moment(date).format('YYYY-MM-DD HH:mm:ss') : null;
}

const sortArrayAlphabetically = (data, key) => {
    let result = data.sort((a, b)=>{
        let sa = a[key].toLowerCase(),
            sb = b[key].toLowerCase();
        if (sa < sb) {
            return -1;
        }
        if (sa > sb) {
            return 1;
        }
        return 0;
    });
    return result;
}

const findArrayElement = (array, key, value) => {
    return array.find(obj => obj[key] === value);
}

const isPositiveNumber = (value) => {
    if(isNaN(value)) return false;
    if(Number(value) <= 0) return false;
    if(Number.isInteger(Number(value)) <= 0) return false;
    return true;
}

const arrayRemove = (arr, value) => { 
    return arr.filter((ele) => ele!==value);
}

const getKeyByValue = (obj, value) => Object.keys(obj).find(key => obj[key] === value);

const formatDate = (dateObj) => {
    if(!dateObj)
        return null
    return moment(dateObj).format('MM-DD-YYYY')
}

const getRouterPrefix = () => process.env.REACT_APP_ROUTER_PREFIX;

const isEmpty = (obj) => {
    let hasOwnProperty = Object.prototype.hasOwnProperty;
    if (obj == null) return true;
    if (obj === undefined) return true;
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;
    if (typeof obj !== "object") return true;
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }
    return true;
}

const range = (min, max) => [...Array(max - min + 1).keys()].map(i => i + min);

const isRecordEditable = isAdmin => {
    let userType = getUserData('userType')
    switch(userType) {
        case 'DS':
            return false;
        case 'DA':
            return isAdmin ? true : false;
        case 'DU':
            return isAdmin ? false : true;
        default:
            return false;
    }
}

const isMobileNumberValid = (value) => {
    if (value.match(/^\+(?:[0-9] ?){6,14}[0-9]$/) || value.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)) {
        return true;
    } else {
        return false;
    }
}

const getName = (array,char) => array.join(char)

const disableFutureDates = current => current>getYesterdayObj()

export { getTodayObj, getYesterdayObj, sortArrayAlphabetically, arrayRemove, findArrayElement, isPositiveNumber, getKeyByValue, formatDate, getRouterPrefix, isEmpty, range, isRecordEditable, isMobileNumberValid, disableFutureDates, getName };