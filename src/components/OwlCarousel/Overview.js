import React, { Component } from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
        };
    }
    componentWillMount() {

    }
    render() {
        const options = {
            items: this.props.Carouselitems ? this.props.Carouselitems : 4,
            nav: false,
            autoplay: false,
            slideBy: 1,
            dots: false,
            dotsEach: false,
            dotData: true,
            loop: this.props.islooping ? this.props.islooping : false,
            responsive: {
                0: {
                    items: 1
                },
                450: {
                    items: 2
                },
                768: {
                    items: 3
                },
                1000: {
                    items: this.props.Carouselitems ? this.props.Carouselitems : 4
                }
            }
        };
        let { OwlCarouselData } = this.props;

        var OwlCarouselList = OwlCarouselData && OwlCarouselData.length > 0 && OwlCarouselData.map((element, key) => {
            return (
                <div>
                    <div className='owl__item'>
                        <div className="owl__item__lists" style={{ minHeight: '60px', marginLeft: '20px', marginRight: '20px' }}>
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="owl__item__lists__icon">
                                        <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="24" cy="24" r="24" fill="#FA5F1C" />
                                            <path d="M24 34C29.5228 34 34 29.5228 34 24C34 18.4772 29.5228 14 24 14C18.4772 14 14 18.4772 14 24C14 29.5228 18.4772 34 24 34Z" stroke="white" strokeWidth="2.13004" strokeLinecap="square" />
                                            <path d="M24.0002 28C26.2094 28 28.0002 26.2091 28.0002 24C28.0002 21.7909 26.2094 20 24.0002 20C21.7911 20 20.0002 21.7909 20.0002 24C20.0002 26.2091 21.7911 28 24.0002 28Z" stroke="white" strokeWidth="2.13004" strokeLinecap="square" />
                                            <path d="M26.8298 26.8301L30.7326 30.6225" stroke="white" strokeWidth="2.13004" strokeLinecap="square" />
                                            <path d="M17.3633 30.8798L21.17 27.0508" stroke="white" strokeWidth="2.13004" strokeLinecap="square" />
                                            <path d="M26.8303 21.1696L30.3603 17.6396" stroke="white" strokeWidth="2.13004" strokeLinecap="square" />
                                            <path d="M17.3633 17.1367L21.1701 21.1706" stroke="white" strokeWidth="2.13004" strokeLinecap="square" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="owl__item__lists__location">
                                        {element.Description && <p>
                                            <span>{element.Description}</span>
                                        </p>}
                                        <h1 className="" style={{
                                            fontStyle: "normal",
                                            fontWeight: "700",
                                            fontSize: "49px",
                                            lineHeight: "50px"
                                        }}>{element.Count}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        });

        return (
            <>
                {OwlCarouselData && OwlCarouselData.length > 0 &&
                    <OwlCarousel className='owl owl-OwlCarousel owl-theme' ref="gallery" {...options}>
                        {OwlCarouselList}
                    </OwlCarousel>
                }

            </>
        )
    }
}
export default Index;