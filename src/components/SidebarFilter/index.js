import React, { Component } from "react";
import Accordion from "react-bootstrap/Accordion";
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {}

  render() {
    
    return (
      <>
      <div className="accordion-top">
        <div className="side-filter">
          <div className="row">
                <div className="col-6">
                  <div className="filter-name">Filter</div>
                </div>
                <div className="col-6 text-rights">
                    <a href="" className="cta--underline filter-underline"> Clear Filter</a>
                </div>
            </div>
        </div>
        <Accordion alwaysOpen>
          <Accordion.Item eventKey="0">
            <Accordion.Header>phase</Accordion.Header>
            <Accordion.Body>
              <div className="accordion-list">
                 <ul className="d-grid ps-0">
                        <li>
                            <input className="styled-checkbox" id="1" type="checkbox"value="value1" />
                            <label htmlFor="1">
                                <span>Stevan Sanders</span>
                            </label>
                        </li>
                        <li>
                            <input className="styled-checkbox" id="2" type="checkbox"value="value1" />
                            <label htmlFor="2">
                                <span>Stevan Sanders</span>
                            </label>
                        </li>
                        <li>
                            <input className="styled-checkbox" id="3" type="checkbox"value="value1" />
                            <label htmlFor="3">
                                <span>Stevan Sanders</span>
                            </label>
                        </li>
                    </ul>
                </div>
                
              
            </Accordion.Body>
          </Accordion.Item>
        <Accordion.Item eventKey="1">
            <Accordion.Header>Plan</Accordion.Header>
            <Accordion.Body>
                <div className="accordion-list">
                 <ul className="d-grid ps-0">
                        <li>
                            <input className="styled-checkbox" id="4" type="checkbox"value="value1" />
                            <label htmlFor="4">
                                <span>Algebra</span>
                            </label>
                        </li>
                        <li>
                            <input className="styled-checkbox" id="5" type="checkbox"value="value1" />
                            <label htmlFor="5">
                                <span>Admission to Cambridge</span>
                            </label>
                        </li>
                       
                    </ul>
                </div>
            </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="2">
            <Accordion.Header>Course</Accordion.Header>
            <Accordion.Body>
                <div className="accordion-list">
                 <ul className="d-grid ps-0">
                        <li>
                            <input className="styled-checkbox" id="6" type="checkbox"value="value1" />
                            <label htmlFor="6">
                                <span>Stevan Sanders</span>
                            </label>
                        </li>
                       
                    </ul>
                </div>
            </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="3">
            <Accordion.Header>Status</Accordion.Header>
            <Accordion.Body>
                <div className="accordion-list">
                 <ul className="d-grid ps-0">
                        <li>
                            <input className="styled-checkbox" id="7" type="checkbox"value="value1" />
                            <label htmlFor="7">
                                <span>Ongoing</span>
                            </label>
                        </li>
                        <li>
                            <input className="styled-checkbox" id="8" type="checkbox"value="value1" />
                            <label htmlFor="8">
                                <span>Completed</span>
                            </label>
                        </li>
                        <li>
                            <input className="styled-checkbox" id="9" type="checkbox"value="value1" />
                            <label htmlFor="9">
                                <span>Not started</span>
                            </label>
                        </li>
                    </ul>
                </div>
            </Accordion.Body>
        </Accordion.Item>
      
        </Accordion>
        </div>
      </>
    );
  }
}
export default Index;
