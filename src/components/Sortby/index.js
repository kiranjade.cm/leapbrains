import React, { Component } from "react";
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {}

  render() {
    return (
      <>
        <div className="sortby">
          <span className="sortIcon">
             <svg width="24" height="24" viewBox="0 0 24 24" fill="none"  xmlns="http://www.w3.org/2000/svg" >
                <path d="M22.0007 2.66797H2.00065C1.82384 2.66797 1.65427 2.73821 1.52925 2.86323C1.40422 2.98826 1.33398 3.15782 1.33398 3.33464V4.44797C1.33411 4.60543 1.36539 4.7613 1.42604 4.90661C1.48668 5.05193 1.57548 5.1838 1.68732 5.29464L9.33398 13.0546V19.8546L10.6673 20.3613V12.668C10.6678 12.5802 10.651 12.4933 10.6178 12.412C10.5847 12.3308 10.5358 12.2569 10.474 12.1946L2.66732 4.39464V4.0013H21.334V4.40797L13.554 12.1946C13.4874 12.2547 13.4335 12.3275 13.3957 12.4089C13.3579 12.4902 13.3369 12.5783 13.334 12.668V21.4746L14.6673 22.0013V13.0013L22.314 5.33464C22.4276 5.22099 22.5174 5.08578 22.5781 4.93695C22.6388 4.78813 22.6691 4.62868 22.6673 4.46797V3.33464C22.6673 3.15782 22.5971 2.98826 22.4721 2.86323C22.347 2.73821 22.1775 2.66797 22.0007 2.66797Z" fill="black"/>
                </svg>
            </span>
          <input type="text" className="sortTerm " placeholder={this.props.placeholder ? this.props.placeholder : ""}/>
        </div>
      </>
    );
  }
}
export default Index;
