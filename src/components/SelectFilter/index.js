import React, { Component } from "react";
import Select, { components } from "react-select";
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentWillMount() { }

    render() {
        const { ValueContainer, Placeholder } = components;
        const FilterData = [
            { value: 'A-Z', label: 'A-Z' },
            { value: 'Z-A', label: 'Z-A' },

        ];
        const CustomValueContainer = ({ children, ...props }) => {
            return (
                <ValueContainer {...props}>
                    <Placeholder {...props} isFocused={props.isFocused}>
                        {props.selectProps.placeholder}
                    </Placeholder>
                    {React.Children.map(children, child =>
                        child && child.type !== Placeholder ? child : null
                    )}
                </ValueContainer>
            );
        };

        return (
            <>
                <div className="select-filter">
                    <span className="selectIcon">
                        <Select
                            className="goals__form__select mb-3"
                            classNamePrefix="mySelect"
                            options={FilterData}
                            closeMenuOnSelect={false}
                            isClearable={false}
                            defaultValue={{ value: 'A-Z', label: 'A-Z'}}

                            components={{
                                ValueContainer: CustomValueContainer, IndicatorSeparator: () => null
                            }}
                            placeholder=""
                            styles={{
                                container: (provided, state) => ({
                                    ...provided,
                                    height: '40px',
                                    overflow: "visible"
                                }),
                                valueContainer: (provided, state) => ({
                                    ...provided,
                                    overflow: "visible",
                                    height: '100%',
                                    minHeight: '40px',
                                }),
                                placeholder: (provided, state) => ({
                                    ...provided,
                                    position: "absolute",
                                    top: state.hasValue || state.selectProps.inputValue ? -13 : "30%",
                                    fontSize: (state.hasValue || state.selectProps.inputValue) && 13,
                                    background: '#fff',
                                    paddingLeft: 10,
                                    paddingRight: 10
                                })
                            }}
                        />
                    </span>
                </div>
            </>
        );
    }
}
export default Index;
